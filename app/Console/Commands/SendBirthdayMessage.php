<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Sms_template;
use App\Http\Controllers\CartController;

class SendBirthdayMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendBirthdayMessage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to send birthday wishes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = new User;
        $sms_template = new Sms_template;

        $message_data = $sms_template::where('template_title','birthday')->first();

        if($message_data != null && $message_data != '')
        {
            $message = $message_data->template_description;
        }
        else
        {
            $message = 'Dear user, wish you a very happy birthday';
        }

        $birthday_list = $user::whereDay('dob', '=', date('d'))->whereMonth('dob', '=', date('m'))->get();
        // $this->info($birthday_list);

        $mask = 'daduss';
        $unicode=0;
        foreach ($birthday_list as $user) {

            $mobile = $user->mobile;
            $sendmessagearray=array('mask'=>$mask,'mobile'=> $mobile ,"message"=>preg_replace("|\&|","^*^*^",$message),'from'=>3,'unicode'=>$unicode);

            $sendmessagejson = json_encode($sendmessagearray);
            $sendmessagedata = 'sendmessage='.$sendmessagejson;

            app('App\Http\Controllers\CartController')->sendsms($sendmessagejson,$sendmessagedata);
        }
    }
}
