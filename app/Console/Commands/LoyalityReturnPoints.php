<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\LoyaltyReadmePointRecord;
use App\Models\Order;

class LoyalityReturnPoints extends Command
{

    protected $signature = 'LoyalityReturnPoints:LoyalityReturnPoints';
    protected $description = 'Loyality Return Points';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $yesterday = date('Y-m-d', strtotime("-1 days"));
        $loyalty = LoyaltyReadmePointRecord::where('created_at', 'LIKE', "$yesterday%")->where('returnreadmepoint', '!=', 1)->whereNull('uniqueno')->pluck('order_id')->toArray();
        if (isset($loyalty) && !empty($loyalty)) {
            $orders = Order::whereIn('id', $loyalty)->where('payment_status', 0)->get()->toArray();
            if (isset($orders) && !empty($orders)) {
                foreach ($orders as $ok => $ov) {
                    $readmereverse = $this->REVERSE_POINTS($ov['id']);
                    if($readmereverse != false) {
                        $bdata = json_decode($readmereverse, true);
                        if ($bdata['REVERSE_POINTSResult']['Success'] == true) {
                            $reversePoint = $bdata['REVERSE_POINTSResult']['output']['balance_points'];
                            $loyali = LoyaltyReadmePointRecord::where('order_id', $ov['id'])->first();
                            $loyali->balance_points = (int)$reversePoint;
                            $loyali->returnreadmepoint = 1;
                            $loyali->save();
                        }
                    }

                }
            }

        }
    }

    public function curl_post($endpoint, $data)
    {
        try {
            $payload = json_encode($data);
            $ch = curl_init($endpoint);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'userid:mob_usr',
                    'pwd:@pa$$w0rd')
            );
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function REVERSE_POINTS($orderId)
    {
        $loyalityPoint = LoyaltyReadmePointRecord::where('order_id', $orderId)->first();
        if ($loyalityPoint) {
            $user = User::find($loyalityPoint->user_id);
            $endpoint = 'http://mqst.mloyalpos.com/Service.svc/REVERSE_POINTS';
            $data = [
                "objClass" => [
                    "store_code" => "ONLINE",
                    "redeem_points" => $loyalityPoint->redeem_points,
                    "redeem_date" => date('Y-m-d', strtotime($loyalityPoint->created_at)),
                    "customer_mobile" => $user->mobile,
                    "ref_bill_no" => $orderId,
                ]
            ];
            $result = $this->curl_post($endpoint, $data);
        } else {
            $result = false;
        }
        return $result;

    }

}
