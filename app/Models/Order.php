<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     protected $table = 'orders';

    /**
    * The database primary key value.
    *
    * @var string
    */

    public function user(){
      return $this->belongsTo('App\Models\User');
    }
    //
    // public function payment_method(){
    //   return $this->hasOne('App\Models\PaymentMethod');
    // }




    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','cart','name','mobile','contact_name','contact_number','address','state','city','pincode','order_status','created_at','updated_at',
        'delivery_charge','cart_total','grand_total','tax','discount','payment_method'


    ];

    public function status() {
        return $this->hasOne('App\Models\Status','id', 'order_status');
    }


}
