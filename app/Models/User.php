<?php
namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable
{

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $primaryKey = 'id';

    protected $fillable = [
        'name','username','email','mobile','dob','otp','otp_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','remember_token'
    ];

    public function orders() {
        return $this->hasMany('App\Models\Order');
    }

    public function customeraddresses() {
        return $this->hasMany('App\Models\Customeraddress');
    }
}
