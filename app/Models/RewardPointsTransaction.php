<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RewardPointsTransaction extends Model
{

    protected $table = 'reward_points_transaction';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'mobile','points_used','transaction_amount','bill_no','mode','created_at','updated_at'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

}
