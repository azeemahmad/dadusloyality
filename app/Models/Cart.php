<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
     protected $table = 'cart';

     use SoftDeletes;

    /**
    * The database primary key value.
    *
    * @var string
    */

    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'cart_data','user_id','cart_token','created_at','updated_at','deleted_at'
    ];

}
