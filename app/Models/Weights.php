<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Weights extends Model
{
     protected $table = 'weights';

    /**
    * The database primary key value.
    *
    * @var string
    */

    public function product(){
      return $this->belongsTo('App\Models\Products');
    }
    //
    // public function payment_method(){
    //   return $this->hasOne('App\Models\PaymentMethod');
    // }




    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'products_id','weight','price','status','created_at','updated_at','deleted_at'


    ];

}
