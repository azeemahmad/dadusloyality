<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Categories extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $primaryKey = 'id';

    protected $fillable = [
        'title', 'category_slug', 'parent_id','category_banner','category_mobile_banner','sequence','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function product()
    {
        return $this->hasMany('App\Models\Products');
    }

    // public function parent(){
    //     return $this->belongsTo('App\Models\Categories','parent_id');
    // }

    public function children()
    {
        return $this->hasMany('App\Models\Categories', 'parent_id', 'id');
    }
    public function parent()
    {
        return $this->belongsTo('App\Models\Categories', 'parent_id');
    }


    public function get_categories_tree()
    {

        // return $cat_tree = $this::where('status',1)->with('children')->where('parent_id',0)->get();
        // foreach ($cat_tree as $cat)
        // {
        //     print_r($cat->title);
        //     print_r('<br>');
        //     if($cat->children != null)
        //     {
        //         foreach ($cat->children as $subcat) {
        //             print_r('&nbsp');
        //             print_r('----');
        //             print_r($subcat->title);
        //             print_r('<br>');
        //         }
        //     }
        //
        // }
        // dd();

      try {

        return $cat_tree = $this::where('status',1)->with('children')->where('parent_id',0)->get();
        
        // $result = DB::select("select cat.id as 'id', cat.title as 'category', cat.category_slug as 'category_slug'  , group_concat(subcat.title) as 'subcategory',
        // group_concat(subcat.id) as 'subcat_id', group_concat(subcat.category_slug) as 'subcategory_slug'
        // from `categories` cat
        // left join `categories` subcat on subcat.parent_id = cat.id and subcat.status = 1
        // where cat.status = 1 and cat.parent_id = 0
        // group by cat.title");

        // return $result;

      }catch (\Exception $e) {
          return false;
      }
    }

    public function get_categories()
    {
      try {

        $result = DB::select("select cat.* , group_concat(subcat.title) as 'subcategory',
        group_concat(subcat.id) as 'subcat_id'
        from `categories` cat
        left join `categories` subcat on subcat.parent_id = cat.id and subcat.status = 1
        where cat.status = 1
        group by cat.title");

        return $result;

        }catch (\Exception $e) {
          return false;
      }
    }

    public function delete_category($pid)
    {
      try {

          if(!empty($pid))
            return DB::table('categories')->where('id', '=', $pid)->delete();
          else
            return false;

      } catch (\Exception $e) {
          return false;
      }
    }

    // public function product()
    // {
    //     return $this->hasMany('App\Models\Products','category_id');
    // }
    //


}
