<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Products extends Model
{
     protected $table = 'products';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prod_title',  'prod_type' ,'prod_desc' ,'prod_qty' ,'prod_price' ,'category_id' ,'prod_active' ,'prod_sequence' ,'prod_image' ,'prod_image_small' ,'prod_image_large' , 'status' , 'product_slug'
    ];


    /**
    * get product by id
    * @param productid
    */
    // public function product_detail($id)
    // {
    //   try {
    //      return $this::select('products.*')->where('products.id',$id)->get();
    //   } catch (\Exception $e) {
    //      return $e->message;
    //   }
    // }

        /**
    * Get product by id
    * @param $num
    * return product list
    */

    public function category()
    {
        return $this->belongsToMany('App\Models\Categories');
    }

    public function weight()
    {
        return $this->hasMany('App\Models\Weights');
    }

    public function get_product_by_id($pid)
    {
        // return Products::with('product_images')->get();->with('product_images')
      try {
          return $this::select('products.*','categories.title','categories.id','pc.id as parent_category_id' , 'pc.title as parent_category_name' )
                        ->leftJoin('categories','categories.id','=','products.category_id')
                        ->leftJoin('categories as pc','pc.id','=','categories.parent_id')
                        ->where('id',$pid)
                        ->where('status',1)
                        ->first();
          // $result = $this::first();

      } catch (\Exception $e) {
          return false;
      }
    }


    /**
    * Get all products from a category
    * @param $category_id
    * return product list
    */
    public function category_wise_products($category_slug = null )
    {
      try {
            if($category_slug != null)
            {
                return $this::with(['weight','category'])
                    ->whereHas('category', function($q) use($category_slug) {
                        $q->where('category_slug', '=', $category_slug); // '=' is optional
                    })->where('status',1)->get();
            }
            else
            {
                return $this::with(['weight','category'])->get();
            }

      } catch (\Exception $e) {
          return false;
      }
    }

    /**
    * Get all products with category and parent category
    * @param $num
    * return product list
    */

    public function all_products()
    {
      try {
          return $this::with('category')->where('status',1)->get();
      } catch (\Exception $e) {
          return $e->getMessage();
      }
    }

    // /**
    // * Get $num number of top products
    // * @param $num
    // * return top product list
    // */
    //
    // public function top_products($num)
    // {
    //   try {
    //       return $this::where('prod_type','top_products')->where('prod_active',1)->limit($num)->get();
    //   } catch (\Exception $e) {
    //       return $e->message;
    //   }
    // }

     /**
    * Delete product by id
    * @param $num
    * return top product list
    */

    public function delete_product($pid)
    {
      try {

          if(!empty($pid))
            return DB::table('products')->where('id', '=', $pid)->delete();
          else
            return false;

      } catch (\Exception $e) {
          return false;
      }
    }

     /**
    * Delete product by id
    * @param $num
    * return top product list
    */

    public function create_product($data)
    {
      try {
          DB::beginTransaction();

      } catch (\Exception $e) {
          DB::rollback();
          return false;
      }
    }

}
