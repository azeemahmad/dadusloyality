<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_Status_Progress extends Model
{
     protected $table = 'order_status_progress';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'order_id','user_id','status_id','created_at'
    ];


    public function order() {
        return $this->belongsTo('App\Models\Order');
    }

}
