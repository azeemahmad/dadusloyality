<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
     protected $table = 'order_status';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title','created_at','updated_at'
    ];


    public function order() {
        return $this->belongsTo('App\Models\Order');
    }

}
