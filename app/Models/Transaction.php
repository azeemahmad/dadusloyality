<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
     protected $table = 'transactions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'razorpay_payment_id','razorpay_signature','payment_method','order_id', 'user_id','data','created_at','updated_at'
    ];


    public function order() {
        return $this->belongsTo('App\Models\Order');
    }

}
