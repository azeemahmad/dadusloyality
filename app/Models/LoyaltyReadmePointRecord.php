<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoyaltyReadmePointRecord extends Model
{
    protected $table = 'loyalty_readme_point_records';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id','balance_points','points_value','redeem_points','referenceNo','order_id','store_code','uniqueno'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function order() {
        return $this->belongsTo('App\Models\Order');
    }

}
