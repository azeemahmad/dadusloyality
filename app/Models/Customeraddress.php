<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customeraddress extends Model
{
     protected $table = 'customer_address';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name' , 'address' , 'state' , 'city' , 'pincode' , 'mobile_number', 'default_address'
    ];


    public function User() {
        return $this->belongsTo('App\Models\User');
    }

}
