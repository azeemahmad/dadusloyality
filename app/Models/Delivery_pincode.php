<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Delivery_pincode extends Model
{
     protected $table = 'delivery_pincode';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pincode', 'status'
    ];


    /**
    * get product by id
    * @param productid
    */
    public function pincode_detail($id)
    {
      try {
         return $this::select('delivery_pincode.*')->where('delivery_pincode.id',$id)->get();
      } catch (\Exception $e) {
         return $e->getMessage();
      }
    }


}
