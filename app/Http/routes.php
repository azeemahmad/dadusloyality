<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middlewareGroups' => ['web']], function () {
Route::get('/', function () {
    return view('pages.welcome');
});
Route::group(['middlewareGroups' => ['client']], function () {
Route::get('/product/{product_slug?}', 'ProductsController@fetchProducts');
Route::get('/product/details/{product_slug}', 'ProductsController@fetchProductsdetails');
Route::get('/category/{category_slug?}', 'ProductsController@index');

// Route::get('/test', 'CartController@test');
//Route::get('/home', 'HomeController@index')->name('home');


Route::get('/storelocator', 'CartController@storelocator');
Route::get('/career', 'CartController@career');
Route::get('/aboutus', 'CartController@aboutus');
Route::get('/faq', 'CartController@faq');
Route::get('/privacypolicy', 'CartController@privacypolicy');
Route::get('/contactus', 'CartController@contactus');
Route::post('/contact_submit' , 'CartController@contact_submit');

Route::get('/checkoutstep1', 'CartController@checkoutstep1');
Route::get('/checkoutstep2', 'CartController@checklog');
Route::get('/checkoutstep3', 'CartController@checkoutstep3');
Route::get('/checkoutstep4/{address_id}', 'CartController@checkoutstep4');
Route::post('/confirm_order', 'CartController@confirmorder');
Route::post('/login', 'CartController@login_auth');
Route::post('/validateotp', 'CartController@validateotp');
Route::post('/registeruser', 'CartController@register');
Route::get('/redirect', 'CartController@redirect');
Route::get('/callback', 'CartController@callback');
Route::get('/googleredirect', 'CartController@googleredirect');
Route::get('/googlecallback', 'CartController@googlecallback');
Route::get('/logout', 'CartController@logout');
Route::post('/add_to_cart_submit' , 'CartController@add_to_cart_submit');
Route::any('/cart', 'CartController@cart');
Route::any('/saveaddress', 'CartController@saveaddress');
Route::get('/placeorder' , 'CartController@confirmorder');
Route::get('/invoice/{order_id}/{recent?}' , 'CartController@invoice');
Route::get('/my_invoice/{order_id}/{recent?}' , 'CartController@invoice');
Route::post('/check_pincode' , 'CartController@check_pincode');
Route::post('/pincodestrictcheck' , 'CartController@pincodestrictcheck');
Route::post('search_result' , 'CartController@search_result');
Route::post('search_result1' , 'CartController@search_result1');
Route::post('update_cart_qty' , 'CartController@update_cart_qty'); // update cart from cart page
Route::get('my_account' , 'CartController@my_account');
Route::post('cancel_order' , 'CartController@cancel_order');
Route::post('update_profile' , 'CartController@update_profile');
// Get Route For Show Payment Form
// Post Route For Makw Payment Request
Route::any('/payment', 'RazorpayController@payment');
Route::post('/razorpaypayment', 'RazorpayController@razorpaypayment');


Route::get('login' , 'CartController@login'); // update cart from cart page
// Route::get('/merge' , 'CartController@cart_merge');

Route::get('mail' , 'CartController@mail');

});

Route::group(['prefix' => 'adm','namespace'=>'Admin'], function () {


  Route::get('logout', 'DashboardController@logout');
  Route::get('/', 'DashboardController@index');
  Route::post('login_submit' , 'DashboardController@login_submit');

  Route::get('/dashboard', 'DashboardController@dashboard');
  Route::get('/add_product', 'DashboardController@add_product');
  Route::post('/add_product_submit', 'DashboardController@add_product_submit');
  Route::get('/manage_product', 'DashboardController@manage_product');
  Route::get('/manage_customer', 'DashboardController@manage_customer');
  Route::get('/manage_order', 'DashboardController@manage_order');
  Route::get('/order_details/{order_id}', 'DashboardController@order_details');
  Route::post('/delete_product', 'DashboardController@delete_product_submit');
  Route::post('/delete_category', 'DashboardController@delete_category_submit');
  Route::get('/edit_product/{product_id}', 'DashboardController@edit_product');
  Route::get('/edit_category/{category_id}', 'DashboardController@edit_category');
  Route::post('/edit_product_submit', 'DashboardController@edit_product_submit');
  Route::post('/edit_category_submit', 'MainController@edit_category_submit');
  Route::post('/delete_banner', 'DashboardController@delete_banner_submit');

  //pincode routes start here
  Route::get('/add_pincode' , 'DashboardController@add_pincode');
  Route::get('/delivery_charges' , 'DashboardController@delivery_charges');
  Route::get('/delivery_pincode' , 'DashboardController@delivery_pincode');
  Route::get('/edit_pincode/{pin_id}', 'DashboardController@edit_pincode');
  Route::post('/edit_pincode_submit', 'DashboardController@edit_pincode_submit');
  Route::post('/add_pincode_submit', 'DashboardController@add_pincode_submit');
  Route::post('/delete_pincode', 'DashboardController@delete_pincode_submit');
  Route::post('/send_sms', 'DashboardController@send_sms');
  Route::get('/add_pincode_bulk', 'DashboardController@add_pincode_bulk');
  Route::post('/add_pincode_bulk', 'DashboardController@add_pincode_bulk_submit');

  //pincode for routes end here
  Route::post('addrewards',['as'=>'addrewards','uses'=>'DashboardController@addrewards']);
  Route::get('customers',['as'=>'customers','uses'=>'DashboardController@customers']);
  Route::post('get_details',['as'=>'get_details','uses'=>'DashboardController@get_details']);
  Route::get('home','DashboardController@showDashboard');

  //category routes start here
  Route::get('/add_category', 'DashboardController@add_category');
  Route::post('/add_category_submit', 'DashboardController@add_category_submit');
  Route::get('/manage_category', 'DashboardController@manage_category');
  //category for routes end here

  //sms mobile number start here
  Route::get('/add_mobilenumber' , 'DashboardController@add_mobilenumber');
  Route::post('/add_mobile_submit', 'DashboardController@add_mobile_submit');
  Route::get('/mobile_number' , 'DashboardController@mobile_number');
  Route::get('/edit_mobile_number/{mobile_id}' , 'DashboardController@edit_mobile_number');
  Route::post('/edit_mobilenumber_submit', 'DashboardController@edit_mobilenumber_submit');
  Route::post('/delete_mobile_number', 'DashboardController@delete_mobilenumber_submit');
  //sms mobile number end here
  Route::post('/delivery_charges_submit' , 'DashboardController@delivery_charges_submit');
  Route::any('/changestatus/{id}' , 'DashboardController@update_order_status');
  //manage sms template start here
  Route::get('/manage_sms_template' , 'DashboardController@manage_sms_template')->name('adm/manage_sms_template');
  Route::post('/edit_smstemplate_submit' , 'DashboardController@edit_smstemplate_submit');
  Route::get('/log_details' , 'DashboardController@log_details');

  });

Route::get('/home', 'HomeController@index');
});


// Route::auth();

Route::get('/home', 'HomeController@index');

// API Routes Start
// Route::group(['prefix' => 'api', 'middleware' => ['api']], function(){
Route::group(['prefix' => 'api', 'middleware' => ['api']], function(){
    Route::post('/check_points' , 'ApiController@check_points');
    Route::post('/upload_bill' , 'ApiController@upload_bill');

});
// API Routes End


Route::get('/time',function(){
  echo date("Y-m-d H:i:s");
});


Route::get('/getdetailsloyality', 'LoyalityPointController@INSERT_ITEM_DATA');

/***********************Start Loyality Reedme Program **************************/
Route::post('/getreadmepasscode','LoyalityPointController@getreadmepasscode');
Route::post('/verifypasscode','LoyalityPointController@verifypasscode');
/*******************************END Loyality Reedme Program *******************/
