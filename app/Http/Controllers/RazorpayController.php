<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Input;
use Razorpay\Api\Api;
use Session;
use Redirect;
use App\Models\Products;
use App\Models\Categories;
use App\Models\Order_Status_Progress;
use App\Models\User;
use App\Models\Customeraddress;
use App\Models\Order;
use App\Models\Delivery_pincode;
use App\Models\Cart as CartModel;
use App\Models\Weights;
use App\Models\Transaction;
use App\Models\LoyaltyReadmePointRecord;
use Illuminate\Support\Facades\DB;


class RazorpayController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cart = new CartModel;
        $this->products = new Products;
        $this->customeraddress = new Customeraddress;
        $this->delivery_pincode = new Delivery_pincode;
        $this->order = new Order;
        $this->weight = new Weights;
        $this->transaction = new Transaction;
    }

    public function payment(Request $request)
    {
        try {
            $data = $request->only('payment_method', 'address_id');
            if (Auth::check() && $data['payment_method'] != null && $data['address_id'] != null) {
                $cart_data = app('App\Http\Controllers\CartController')->get_cart_data();
                $user = Auth::user();
                // dd($user);
                $customer_address_id = base64_decode($data['address_id']);
                $customer_address = Customeraddress::where('id', $customer_address_id)->first();
                //dd($totalLoyalitypointvale);
                $result = Order::create(
                    [
                        'user_id' => $user->id,
                        'cart_total' => $cart_data['cart_total'],
                        'delivery_charge' => $cart_data['delivery_charges'],
                        'discount' => $cart_data['delivery_charges'],
                        'grand_total' => $cart_data['grand_total'],
                        'payment_method' => $data['payment_method'],
                        'cart' => serialize($cart_data['cart_items']),
                        'name' => $user->name,
                        'mobile' => $user->mobile,
                        'contact_name' => $customer_address->name,
                        'contact_number' => $customer_address->mobile_number,
                        'address' => $customer_address->address,
                        'state' => $customer_address->state,
                        'city' => $customer_address->city,
                        'pincode' => $customer_address->pincode,

                    ]
                );


                if ($result) {
                    //********************order status pending*********************

                    DB::table('order_status_progress')->insert(
                        array('user_id' => $user->id,
                            'order_id' => $result->id,
                            'status_id' => '2',
                            'created_at' => date("Y-m-d H:i:s"),
                        )
                    );

                    //********************order status pending*********************

                    $order_id = $result->id;

                    $input = Input::all();
                    session_start();

                    /**** SMS CODE ***/

                    $order_id = $result->id;
                    if(isset($request['passcode'])&& strlen($request['passcode'])==6){
                        try {
                            $getLoyalitypoint = $this->GET_CUSTOMER_TRANS_INFO(Auth::user()->mobile);
                            $totalLoyalitypointvale = $getLoyalitypoint['LoyalityPointsValue'];
                            $readme = $totalLoyalitypointvale;
                            $totalamount=$cart_data['grand_total'];
                            if ($readme > $totalamount) {
                                $readMePoint = $totalamount;
                            } elseif ($readme == $totalamount) {
                                $readMePoint = $totalamount;
                            } elseif ($readme < $totalamount) {
                                $readMePoint = $readme;
                            }
                            $resultreadme = $this->REDEEM_LOYALTY_POINTS_ACTION(Auth::user()->mobile,$readMePoint,$request['passcode'],$order_id);
                            $a=json_decode($resultreadme,true);
                            if($a['REDEEM_LOYALTY_POINTS_ACTIONResult']['Success']==true){
                                $output=$a['REDEEM_LOYALTY_POINTS_ACTIONResult']['output'];
                                $readmedata=['user_id'=>Auth::user()->id,'balance_points'=>$output['balance_points'],'points_value'=>$output['points_value'],'redeem_points'=>$output['redeem_points'],'referenceNo'=>$output['referenceNo'],'order_id' =>$order_id];
                                $addloyality=LoyaltyReadmePointRecord::create($readmedata);
                                if($readme < $totalamount){
                                    $order=Order::find($order_id);
                                    $order->payment_method='Razorpay+LoyaltyReadme';
                                    $order->save();
                                    $grandtotal=(int)$totalamount - (int)$readme;
                                    $keyId = env('RAZORPAY_KEY_ID');
                                    $keySecret = env('RAZORPAY_KEY_SECRET');
                                    $displayCurrency = env('RAZORPAY_KEY_CURRENCY');
                                    // dd($keyId,$keySecret,$displayCurrency);

                                    $api = new Api($keyId, $keySecret);
                                    $orderData = [
                                        'receipt' => $order_id,
                                        'amount' => (int)($grandtotal * 100), // amount in paise so x100\
                                        //'amount' => (int)(100), // amount in paise so x100
                                        'currency' => $displayCurrency,
                                        'payment_capture' => 1
                                    ];

                                    //

                                    $razorpayOrder = $api->order->create($orderData);
                                    $razorpayOrderId = $razorpayOrder['id'];
                                    $_SESSION['razorpay_order_id'] = $razorpayOrderId;
                                    $displayAmount = $amount = $orderData['amount'];

                                    /*if ($displayCurrency !== 'INR') {
                                        $url = "https://api.fixer.io/latest?symbols=$displayCurrency&base=INR";
                                        $exchange = json_decode(file_get_contents($url), true);

                                        $displayAmount = $exchange['rates'][$displayCurrency] * $amount / 100;
                                    }*/
                                    $data = [
                                        "key" => $keyId,
                                        "amount" => (int)$amount,
                                        "name" => $user->name,
                                        "description" => 'Payment for Dadus',
                                        "image" => "",
                                        "prefill" => [
                                            "name" => $user->name,
                                            "email" => $user->email,
                                            "contact" => $user->mobile,
                                        ],
                                        "notes" => [
                                            "address" => $customer_address->address . ' ' . $customer_address->city . ' ' . $customer_address->state . ' ' . $customer_address->pincode,
                                            "merchant_order_id" => $order_id,
                                        ],
                                        "theme" => [
                                            "color" => "#F37254"
                                        ],
                                        "order_id" => $razorpayOrderId,
                                    ];
                                    // dd($data);
                                    if ($displayCurrency !== 'INR') {
                                        $data['display_currency'] = $displayCurrency;
                                        $data['display_amount'] = $displayAmount;
                                    }
                                    $json = json_encode($data);
                                    // dd($order_id);
                                    return View("pages.payWithRazorpay", compact('json', 'order_id', 'customer_address_id'));

                                }
                                else{
                                    try {
                                        $user=Auth::user();
                                        $order=Order::find($order_id);
                                        $order->payment_method='LoyaltyReadme';
                                        $order->save();
                                        $res = Transaction::create([

                                            'user_id' => $user->id,
                                            'razorpay_payment_id' => null,
                                            'razorpay_signature' => null,
                                            'payment_method' => null,
                                            'order_id' => $order->id,
                                            'data' => null,

                                        ]);
                                        $transaction_id = $res->id;
                                        $order_id = $order->id;

                                        //////// SMS START  ////////

                                        $mask = 'daduss';
                                        $unicode = 0;

                                        $order_msg = DB::table('sms_template')->where('template_title', '=', 'pending')->pluck('template_description');

                                        $message = "Dear " . $user->name . ", " . $order_msg[0] . " Order Details - Order Id: " . $order_id . ", Total: " . $order->grand_total;


                                        $message_team = 'New order received with following details Order Id: '
                                            . $order_id . ' Customer Name: ' . $customer_address->name . ' Total: '
                                            . $order->grand_total . ' Contact: ' . $user->mobile;

                                        //$this->mail($user->id, $order_id, $order_msg[0]);
                                        app('App\Http\Controllers\CartController')->mail($user->id, $order_id, $order_msg[0]);

                                        //dd($mobile_numbers);
                                        //fetch mobile number list start
                                        $mobile_numbers = DB::table('mobile_number')->select('mobile_number')->where('status', 1)->get();          // Dadus' team numbers


                                        $mobile_number_array = array_column($mobile_numbers, 'mobile_number');


                                        $number_list = implode(',', $mobile_number_array);
                                        //fetch mobile number list end here

                                        $sendmessagearray = array('mask' => $mask, 'mobile' => $user->mobile, "message" => preg_replace("|\&|", "^*^*^", $message), 'from' => 3, 'unicode' => $unicode);

                                        $sendmessagejson = json_encode($sendmessagearray);
                                        $sendmessagedata = 'sendmessage=' . $sendmessagejson;
                                        $sendotpmessage = app('App\Http\Controllers\CartController')->sendsms($sendmessagejson, $sendmessagedata);

                                        //send sms to team
                                        $sendmessagearray_team = array('mask' => $mask, 'mobile' => $number_list, "message" => preg_replace("|\&|", "^*^*^", $message_team), 'from' => 3, 'unicode' => $unicode);

                                        $sendmessagejson_team = json_encode($sendmessagearray_team);


                                        $sendmessagedata_team = 'sendmessage=' . $sendmessagejson_team;

                                        $sendsmsteam = app('App\Http\Controllers\CartController')->sendsms($sendmessagejson_team, $sendmessagearray_team);


                                        //////// SMS END  ////////


                                        // ********************************************************************order status confirmed

                                        // DB::beginTransaction();

                                          Order::where('id', $order_id)
                                            ->update([
                                                'payment_status' => 1,
                                                'transaction_id' => $transaction_id,
                                            ]);
                                        $addreadmepoint=$this->INSERT_BILLING_DATA_ACTION($order_id);
                                        $json=json_decode($addreadmepoint,true);
                                        if($json['INSERT_BILLING_DATA_ACTIONResult']['Success']==true){
                                            $ghgf=LoyaltyReadmePointRecord::where('order_id',$order_id)->first();
                                            $uniqueno=$json['INSERT_BILLING_DATA_ACTIONResult']['output']['uniqueno'];
                                            $ghgf->uniqueno=$uniqueno;
                                            $ghgf->save();
                                        }
                                        //DB::commit();
                                        return redirect('/my_invoice/' . base64_encode($order_id) . '/y');
                                    } catch (\Exception $e) {
                                        //DB::rollBack();
                                        $order=Order::find($order_id);
                                        $order->delete();
                                        return redirect('/cart')->with('error_msg', 'Something went wrong');
                                    }
                                }
                            }
                            else{
                                $order=Order::find($order_id);
                                $order->delete();
                                return redirect('/cart')->with('error_msg',$a['REDEEM_LOYALTY_POINTS_ACTIONResult']['message']);
                            }

                        } catch (\Exception $e) {
                            $order=Order::find($order_id);
                            $order->delete();
                            return redirect('/cart')->with('error_msg','d Something went wrong');
                        }
                    }
                    else{


//              app('App\Http\Controllers\PrintReportController')->getPrintReport();
                        $keyId = env('RAZORPAY_KEY_ID');
                        $keySecret = env('RAZORPAY_KEY_SECRET');
                        $displayCurrency = env('RAZORPAY_KEY_CURRENCY');
                        // dd($keyId,$keySecret,$displayCurrency);

                        $api = new Api($keyId, $keySecret);
                        $orderData = [
                            'receipt' => $order_id,
                            'amount' => (int)($cart_data['grand_total'] * 100), // amount in paise so x100\
                            //'amount' => (int)(100), // amount in paise so x100
                            'currency' => $displayCurrency,
                            'payment_capture' => 1
                        ];

                        //

                        $razorpayOrder = $api->order->create($orderData);
                        $razorpayOrderId = $razorpayOrder['id'];
                        $_SESSION['razorpay_order_id'] = $razorpayOrderId;
                        $displayAmount = $amount = $orderData['amount'];

                        /*if ($displayCurrency !== 'INR') {
                            $url = "https://api.fixer.io/latest?symbols=$displayCurrency&base=INR";
                            $exchange = json_decode(file_get_contents($url), true);

                            $displayAmount = $exchange['rates'][$displayCurrency] * $amount / 100;
                        }*/
                        $data = [
                            "key" => $keyId,
                            "amount" => (int)$amount,
                            "name" => $user->name,
                            "description" => 'Payment for Dadus',
                            "image" => "",
                            "prefill" => [
                                "name" => $user->name,
                                "email" => $user->email,
                                "contact" => $user->mobile,
                            ],
                            "notes" => [
                                "address" => $customer_address->address . ' ' . $customer_address->city . ' ' . $customer_address->state . ' ' . $customer_address->pincode,
                                "merchant_order_id" => $order_id,
                            ],
                            "theme" => [
                                "color" => "#F37254"
                            ],
                            "order_id" => $razorpayOrderId,
                        ];
                        // dd($data);
                        if ($displayCurrency !== 'INR') {
                            $data['display_currency'] = $displayCurrency;
                            $data['display_amount'] = $displayAmount;
                        }

                        $json = json_encode($data);
                        // dd($order_id);
                        return View("pages.payWithRazorpay", compact('json', 'order_id', 'customer_address_id'));

                    }

                } else {
                    return redirect('/cart')->with('error_msg', 'a Something went wrong');
                }
            } else {
                return redirect('/cart')->with('error_msg', 'b Something went wrong');
            }


        } catch (\Exception $e) { //dd($e);

            return redirect('/cart')->with('error_msg', 'c Something went wrong');
        }
    }


    public function getRefund($razorpay_payment_id, $user_id, $order_id)
    {


        try {
            $keyId = env('RAZORPAY_KEY_ID');
            $keySecret = env('RAZORPAY_KEY_SECRET');
            $displayCurrency = env('RAZORPAY_KEY_CURRENCY');
            $api = new Api($keyId, $keySecret);
            $payment = $api->payment->fetch($razorpay_payment_id);
            $refund = $payment->refund();

            DB::table('order_status_progress')->insert(
                array('user_id' => $user_id,
                    'order_id' => $order_id,
                    'status_id' => '7',
                    'created_at' => date("Y-m-d H:i:s"),
                )
            );


            //******************************************************************************** order status refunded


            return true;

        } catch (\Exception $e) {
            return false;
        }
    }

    public function razorpaypayment(Request $request)
    {

        try {

            $data = $request->all();


            $user = Auth::user();
            $user_id = $user->id;

            $cart_data = app('App\Http\Controllers\CartController')->get_cart_data();


//      $customer_address_id = base64_decode($data['address_id']);
            $customer_address_id = $data['address_id'];


            $customer_address = Customeraddress::where('id', $customer_address_id)->first();

//***************     *********************

            $res = Transaction::create([

                'user_id' => $user->id,
                'razorpay_payment_id' => $data['razorpay_payment_id'],
                'razorpay_signature' => $data['razorpay_signature'],
                'payment_method' => $data['payment'],
                'order_id' => $data['order_id'],
                'data' => serialize($data),

            ]);


            $transaction_id = $res->id;
            $order_id = $data['order_id'];

            //////// SMS START  ////////

            $mask = 'daduss';
            $unicode = 0;

            $order_msg = DB::table('sms_template')->where('template_title', '=', 'pending')->pluck('template_description');

            $message = "Dear " . $user->name . ", " . $order_msg[0] . " Order Details - Order Id: " . $order_id . ", Total: " . $cart_data['grand_total'];


            $message_team = 'New order received with following details Order Id: '
                . $order_id . ' Customer Name: ' . $customer_address->name . ' Total: '
                . $cart_data['grand_total'] . ' Contact: ' . $user->mobile;

            //$this->mail($user->id, $order_id, $order_msg[0]);
            app('App\Http\Controllers\CartController')->mail($user->id, $order_id, $order_msg[0]);

            //dd($mobile_numbers);
            //fetch mobile number list start
            $mobile_numbers = DB::table('mobile_number')->select('mobile_number')->where('status', 1)->get();          // Dadus' team numbers


            $mobile_number_array = array_column($mobile_numbers, 'mobile_number');


            $number_list = implode(',', $mobile_number_array);
            //fetch mobile number list end here

            $sendmessagearray = array('mask' => $mask, 'mobile' => $user->mobile, "message" => preg_replace("|\&|", "^*^*^", $message), 'from' => 3, 'unicode' => $unicode);

            $sendmessagejson = json_encode($sendmessagearray);
            $sendmessagedata = 'sendmessage=' . $sendmessagejson;
            $sendotpmessage = app('App\Http\Controllers\CartController')->sendsms($sendmessagejson, $sendmessagedata);

            //send sms to team
            $sendmessagearray_team = array('mask' => $mask, 'mobile' => $number_list, "message" => preg_replace("|\&|", "^*^*^", $message_team), 'from' => 3, 'unicode' => $unicode);

            $sendmessagejson_team = json_encode($sendmessagearray_team);


            $sendmessagedata_team = 'sendmessage=' . $sendmessagejson_team;

            $sendsmsteam = app('App\Http\Controllers\CartController')->sendsms($sendmessagejson_team, $sendmessagearray_team);


            //////// SMS END  ////////


            // ********************************************************************order status confirmed


              Order::where('id', $order_id)
                ->update([
                    'payment_status' => 1,
                    'transaction_id' => $transaction_id,
                ]);

            $addreadmepoint=$this->INSERT_BILLING_DATA_ACTION($order_id);
            $json=json_decode($addreadmepoint,true);
            if($json['INSERT_BILLING_DATA_ACTIONResult']['Success']==true){
                $ghgf=LoyaltyReadmePointRecord::where('order_id',$order_id)->first();
                $uniqueno=$json['INSERT_BILLING_DATA_ACTIONResult']['output']['uniqueno'];
                $ghgf->uniqueno=$uniqueno;
                $ghgf->save();
            }
            return redirect('/my_invoice/' . base64_encode($order_id) . '/y');
        } catch (\Exception $e) {
            return redirect('/cart')->with('error_msg', 'Something went wrong');
        }
    }
}

?>
