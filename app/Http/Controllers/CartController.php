<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Validator;
use Session;
use DB;
use Config;
use App\Models\Products;
use App\Models\Categories;
use App\Models\User;
use App\Models\Customeraddress;
use App\Models\Order;
use App\Models\Delivery_pincode;
use App\Models\Cart as CartModel;
use App\Models\Weights;
use App\Models\ContactUs;
use Cart;
use Hash;
use Socialite;
use Google_Client;
use Illuminate\Support\Str;
use Mail;
use Razorpay\Api\Api;


class CartController extends Controller {

    public function __construct() {
        parent::__construct();
        $this->generate_cart_token();

        $this->cart = new CartModel;
        $this->products = new Products;
        $this->customeraddress = new Customeraddress;
        $this->delivery_pincode = new Delivery_pincode;
        $this->order = new Order;
        $this->weight = new Weights;
    }

    public function generate_cart_token() {
        if (!session()->has('cart_token')) {
            $cart_id = Str::random(64);
            session()->put('cart_token', $cart_id);
        }
    }

    public function add_to_cart_submit(Request $request)
    {
        $data = $request->all();
        // dd($data);
        if (isset($data['product_id']) && isset($data['product_qty']) &&  isset($data['weight']))
        {
            $res = $this->cart_add($data['product_id'],$data['product_qty'],$data['weight']);
            if($res != false)
            {
                return redirect('cart');
            }
            else
            {
                return redirect()->back()->with('error_msg', ['Something went wrong']);
            }
        }
        else
        {
            return redirect()->back()->with('error_msg', ['Something went wrong']);
        }
    }


    public function get_cart_data()
    {

        try {

            $cart_items = [];
            $cart_total = 0;
            $grand_total = 0;
            $delivery_charges = 0;
            $discount = 0;
            if (session()->has('cart_token'))
            {
                $cart_token = session()->get('cart_token');
                $cart_data = $this->cart->where('cart_token', $cart_token)->first();

                $cart_items_ser = $cart_data->cart_data;
                $cart_items = unserialize($cart_items_ser);

                // dd($cart_items);

                foreach ($cart_items as $pid_wt => $product_data)
                {
                    $pid_wt_arr = explode('_', $pid_wt);
                    // dd($pid_wt_arr);
                    $product_id = $pid_wt_arr[0];
                    $weight_id = $pid_wt_arr[1];
                    $p_data = $this->products->where('id',$product_id)->first();
                    $w_data = $this->weight->where('id',$weight_id)->first();
                    // dd($p_data,$w_data);
                    // dd($w_data->weight,$w_data->price);
                    if($p_data != null)
                    {
                        // dd($p_data->toArray(), $w_data->toArray());
                        $cart_items[$pid_wt]['data'] = $p_data->toArray();
                        $cart_items[$pid_wt]['weight'] = $w_data->toArray();
                        $cart_total = $cart_total + $cart_items[$pid_wt]['weight']['price'] * $product_data['qty'];
                    }
                    else
                    {
                        $cart_items[$pid_wt]['data'] = null;
                    }
                }

                $delivery_data = DB::table('delivery_charges')->where('id', 1)->first();

                if($cart_total < $delivery_data->min_cart_value)
                {
                    $delivery_charges = $delivery_data->delivery_charges;
                }

                $grand_total = $delivery_charges + $cart_total - $discount;
            }

            // dd($cart_items);
            return [ 'cart_items' => $cart_items , 'cart_total' => $cart_total,  'grand_total' => $grand_total , 'delivery_charges' => $delivery_charges , 'discount' => $discount ];


        } catch (\Exception $e) {
            return false;
        }
    }


    public function cart(Request $request)
    {
        try {

            $cart_items = [];
            $cart_total = 0;
            $grand_total = 0;
            $delivery_charges = 0;
            $discount = 0;

            $data = $this->get_cart_data();

            $cart_items = $data['cart_items'];
            $cart_total = $data['cart_total'];
            $grand_total = $data['grand_total'];
            $delivery_charges = $data['delivery_charges'];
            $discount = $data['discount'];
            // dd($cart_items);
            return view('pages.checkoutstep1',['cart' => $cart_items , 'cart_total' =>  $cart_total, 'grand_total' => $grand_total, 'delivery_charges' => $delivery_charges, 'discount' => $discount ]);

        } catch (\Exception $e) {

            return redirect()->back()->with('error_msg', 'Something went wrong');
        }

    }

    public function cart_add($product_id,$product_qty,$weight) {

        if (!session()->has('cart_token'))
        {
            $this->generate_cart_token();
        }

        $user_id = null;
        if(Auth::check())
        {
            $user_id = Auth::id();
        }

        if ( !empty($product_id) && !empty($product_qty) && !empty($weight) )
        {
            $cart_token = session()->get('cart_token');
            $cart_data = $this->cart->where('cart_token', $cart_token)->first();
            $pid_wt = $product_id.'_'.$weight;                // combining product_id and weight_id to form unique combination
            // dd($pid_wt);

            if ($cart_data != false && $cart_data->cart_data !=null )
            {
                $cart_items_ser = $cart_data->cart_data;
                $cart_items = unserialize($cart_items_ser);
                // dd($cart_items);

                if ( array_key_exists($pid_wt,$cart_items))
                {
                    $cart_items[$pid_wt]['qty'] = $cart_items[$pid_wt]['qty'] + $product_qty;
                }
                else
                {
                    $new_cart_item = [
                        $pid_wt => [
                            'qty' => $product_qty,
                             'weight' => $weight,
                        ]
                    ];
                    $cart_items = $cart_items + $new_cart_item;

                }
            }
            else
            {
                $new_cart_item = [
                    $pid_wt => [
                        'qty' => $product_qty,
                        'weight' => $weight,
                        //                           'price' => $product_price
                    ]
                ];
                $cart_items = $new_cart_item;
            }

            $cart_items_ser = serialize($cart_items);

            $res = $this->cart->updateOrCreate(
                ['cart_token' => $cart_token],
                ['cart_data' => $cart_items_ser,'user_id' => $user_id ]
            );
            if($res)
            return true;
            else
            return false;
        }
        else
        {
            return false;
        }
    }

    public function cart_remove($product_id,$product_qty,$weight) {

        if (!session()->has('cart_token'))
        {
            $this->generate_cart_token();
        }

        $user_id = null;
        if(Auth::check())
        {
            $user_id = Auth::id();
        }

        if ( !empty($product_id) && !empty($product_qty) && !empty($weight) )
        {
            $cart_token = session()->get('cart_token');
            $cart_data = CartModel::where('cart_token', $cart_token)->first();
            $pid_wt = $product_id.'_'.$weight;                // combining product_id and weight_id to form unique combination

            if ($cart_data != false)
            {
                $cart_items_ser = $cart_data->cart_data;
                $cart_items = unserialize($cart_items_ser);
                //                dd($cart_items);
                if (array_key_exists($pid_wt,$cart_items))
                {
                    if( $cart_items[$pid_wt]['qty'] - $product_qty <= 0 )
                    {
                        unset($cart_items[$pid_wt]);
                    }
                    else
                    {
                        $cart_items[$pid_wt]['qty'] = $cart_items[$pid_wt]['qty'] - $product_qty;
                    }

                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            $cart_items_ser = serialize($cart_items);

            $res = CartModel::updateOrCreate(
                ['cart_token' => $cart_token],
                ['cart_data' => $cart_items_ser,'user_id' => $user_id ]
            );

            if($res)
            return true;
            else
            return false;
        }
        else
        {
            return false;
        }
    }



    public function cart_merge() {

        try {
            DB::beginTransaction();

            if (session()->has('cart_token'))
            {
                $this->generate_cart_token();
            }

            if(Auth::check() && session()->has('cart_token'))
            {
                $user_id = Auth::id();
                $cart_token = session()->get('cart_token');

                $current_cart_data_ser = CartModel::where('cart_token',$cart_token)->first();
                $current_cart_data = unserialize($current_cart_data_ser->cart_data);

                $all_cart_data = CartModel::where('user_id',$user_id)->where('cart_token','<>',$cart_token)->get();
                foreach ($all_cart_data as $c_data)
                {
                    $unser = unserialize($c_data->cart_data);
                    foreach ($unser as $pid => $pdata)
                    {
                        $cnt = count(Products::where('id',$pid)->first());
                        if($cnt > 0)
                        {
                            if( array_key_exists($pid, $current_cart_data ))
                            {
                                $current_cart_data[$pid]['qty'] = $current_cart_data[$pid]['qty']+ $pdata['qty'];
                            }
                            else
                            {
                                $current_cart_data[$pid]['qty'] = $pdata['qty'];
                            }
                        }
                    }
                }
                $cart_data_new_ser = serialize($current_cart_data);

                $res_1 = CartModel::where('cart_token',$cart_token)->update(['cart_data' => $cart_data_new_ser]);
                $res_2 = CartModel::where('cart_token', '<>' , $cart_token )->where('user_id',$user_id)->delete();
                if($res_1 && $res_2)
                {
                    DB::commit();
                    return true;
                }
                else
                {
                    DB::rollBack();
                    return false;
                }
            }
            else
            {
                DB::rollBack();
                return false;
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return false;
        }
    }


    public function checklog(Request $request) {
        // $checksession = $request->session()->get('islogin');
        if (Auth::check())
        {
            return Redirect('/checkoutstep3');
        }
        else
        {
            return view('pages.checkoutstep2');
        }
    }

    public function checkoutstep1(Request $request) {
        return view('pages.checkoutstep1');
    }

    public function checkoutstep3(Request $request)
    {

        if ( Auth::check() && $this->cart_quantity() != false && $this->cart_quantity() > 0 )
        {
            $user_data = User::with('customeraddresses')->where('id',Auth::user()->id)->first();
            if ($user_data)
            {
                return view('pages.checkoutstep3', ['user_data' => $user_data]);
            }
            else
            {
                return view('pages.checkoutstep3');
            }
        }
        else
        {
            return Redirect('/cart');
        }
    }

    public function checkoutstep4($add_id)
    {
        if($add_id!= null)
        {
            $address_id = base64_decode($add_id);
            $customer_address = Customeraddress::where('id',$address_id)->first();
            $del_pincode = $customer_address->pincode;

            $delivery_status = Delivery_pincode::where('pincode',$del_pincode)->where('status',1)->first();

            if($delivery_status == null)
            {
                return redirect('checkoutstep3')->with('del_msg',$del_pincode.' is out of servicable area.');
            }

            if ( Auth::check() && $this->cart_quantity() != false && $this->cart_quantity() > 0 )
            {
                $user=Auth::user();
                $cart_items = [];
                $cart_total = 0;
                $grand_total = 0;
                $delivery_charges = 0;
                $discount = 0;
                $totalLoyalitypointvale=0;
                $data = $this->get_cart_data();
                $this->INSERT_CUSTOMER_REGISTRATION_ACTION($user,$customer_address);
                if($data!= false)
                {
                    $getLoyalitypoint=$this->GET_CUSTOMER_TRANS_INFO(Auth::user()->mobile);
                    if(isset($getLoyalitypoint) && $getLoyalitypoint != null) {
                        $totalLoyalitypointvale = $getLoyalitypoint['LoyalityPointsValue'];
                    }
                    $cart_items = $data['cart_items'];
                    $cart_total = $data['cart_total'];
                    $grand_total = $data['grand_total'];
                    $delivery_charges = $data['delivery_charges'];
                    $discount = $data['discount'];
                }
                    // dd($add_id);
                    return view('pages.checkoutstep4', ['cart' => $cart_items , 'cart_total' =>  $cart_total, 'grand_total' => $grand_total, 'delivery_charges' => $delivery_charges, 'discount' => $discount , 'address_id'=> $add_id, 'customer_address' => $customer_address,'totalLoyalitypointvale' =>$totalLoyalitypointvale ]);
            }
        }

        return Redirect('/cart');
    }

    public function aboutus(Request $request) {
        return view('pages.about');
    }

    public function login_auth(Request $request) {

        try {

            $user_date = $request->only('mobilenumber');

            if($user_date != false && !empty($user_date['mobilenumber']))
            {
                $userdetail = User::where('mobile',$user_date['mobilenumber'])->where('otp_status',1)->first();

                if ($userdetail)
                {
                    $mask = 'daduss';
                    $otp = rand(1000, 9999);
                    $unicode = 0;
                    $message = 'Your verification code for Dadus login is ' . $otp;
                    $sendmessagearray = array('mask' => $mask, 'mobile' => $user_date['mobilenumber'], "message" => preg_replace("|\&|", "^*^*^", $message), 'from' => 3, 'unicode' => $unicode);

                    $user_update = User::where('mobile', $user_date['mobilenumber'])->update(['otp' => $otp]);

                    $sendmessagejson = json_encode($sendmessagearray);
                    $sendmessagedata = 'sendmessage=' . $sendmessagejson;
                    $sendotpmessage = $this->sendsms($sendmessagejson, $sendmessagedata);

                    return response()->json(['status' => true, 'message' => 'OTP sent']);
                }
                else
                {
                    return response()->json(['status' => false, 'message' => 'No user found with this mobile number.']);
                }
            }
            else
            {
                return response()->json(['status' => false, 'message' => 'Something went wrong']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => 'Something went wrong']);
        }
    }

    public function validateotp(Request $request) {

        $user = new User();
        $user_data = $request->only('otpnumner','mobile');
        $result = $user::where('mobile',$user_data['mobile'])->where('otp', $user_data['otpnumner'])->first();

        if ($result)
        {

            $utm_source     = null;
            $utm_campaign   = null;
            $utm_medium     = null;

            if(Session::has('utm_source'))
            {
                $utm_source = Session::get('utm_source');
            }

            if(Session::has('utm_campaign'))
            {
                $utm_campaign = Session::get('utm_campaign');
            }

            if(Session::has('utm_medium'))
            {
                $utm_medium = Session::get('utm_medium');
            }


            $result->otp_status     = 1;
            $result->utm_source     = $utm_source;
            $result->utm_campaign   = $utm_campaign;
            $result->utm_medium     = $utm_medium;
            $result->save();

            if(Auth::loginUsingId($result->id))
            {
                return response()->json(['status' => true, 'message' => 'OTP verified succesfully.']);
            }
            else
            {
                return response()->json(['status' => false, 'message' => 'Something went wrong']);
            }
        }
        else
        {
            return response()->json(['status' => false, 'message' => 'Invalid OTP']);
        }
    }

    public function check_pincode(Request $request) {
        $data = $request->all();
        //print_r($data);
        $result = DB::table('delivery_pincode')->where('pincode', '=', $data['val_pin'])->where('status', '=', 1)->first(); //
        if ($result) {

            return response()->json(['status' => true, 'message' => 'Delivery Available to this pincode.']);
        } else {

            return response()->json(['status' => false, 'message' => 'Delivery Not Available to this pincode.']);
        }
    }

    public function register(Request $request) {

        $user = new User();
        $userdetail = $user::where('mobile', $request->get('mobile'))->where('otp_status',1)->first();
        if ($userdetail)
        {
            return response()->json(['status' => false, 'message' => 'Mobile number is already present. Please signin.']);
        }
        else
        {
            $user_data = $request->only('name','email','dob','mobile');
            //send otp to user
            $mask = 'daduss';
            $otp = rand(1000, 9999);
            $user_data['otp'] = $otp;

            // dd($user_data);
            // dd($request->get('mobile'));


            $utm_source     = null;
            $utm_campaign   = null;
            $utm_medium     = null;

            if(Session::has('utm_source'))
            {
                $utm_source = Session::get('utm_source');
            }

            if(Session::has('utm_campaign'))
            {
                $utm_campaign = Session::get('utm_campaign');
            }

            if(Session::has('utm_medium'))
            {
                $utm_medium = Session::get('utm_medium');
            }


            $result = $user::updateOrCreate([
                'mobile'=> $user_data['mobile']
            ],[
                'name' => $user_data['name'], 'email' =>  $user_data['email'], 'dob' =>  $user_data['dob'], 'otp' =>  $user_data['otp'],'utm_source' => $utm_source,'utm_campaign' => $utm_campaign, 
                'utm_medium' => $utm_medium
            ]);

            if($result)
            {

                $unicode = 0;
                $message = 'Your verification code for Dadus login is ' . $otp;
                $sendmessagearray = array('mask' => $mask, 'mobile' => $request->get('mobile'), "message" => preg_replace("|\&|", "^*^*^", $message), 'from' => 3, 'unicode' => $unicode);

                $sendmessagejson = json_encode($sendmessagearray);
                $sendmessagedata = 'sendmessage=' . $sendmessagejson;
                $sendotpmessage = $this->sendsms($sendmessagejson, $sendmessagedata);

                // $request->session()->put('islogin', 1);
                // $request->session()->put('usermobile', $request->get('mobile'));

                if ($sendotpmessage)
                {
                    return response()->json(['status' => true, 'message' => 'OTP sent']);
                }
                else
                {
                    return response()->json(['status' => false, 'message' => 'Something went wrong. Please try again. Err A']);
                }
            }
            else
            {
                return response()->json(['status' => false, 'message' => 'Something went wrong. Please try again. Err B']);
            }
        }
    }

    //shipping details start here
    public function saveaddress(Request $request)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            $data = $request->only('name','address','state','city','pincode','mobile_number');
            // dd($data);
            $res = $user->customeraddresses()->create($data);
            if($res)
            {
                return response()->json(['status' => true, 'address_id' => $res->id, 'message' => 'Address saved successfully.']);
            }
            else
            {
                return response()->json(['status' => false, 'message' => 'Something went wrong. Please try again.']);
            }
        }
        else
        {
            return response()->json(['status' => false, 'message' => 'Something went wrong. Please try again.']);
        }
    }
    //shipping details end here


    public function confirmorder(Request $request)
    {
        $data = $request->only('payment_method','address_id');


        if ( Auth::check() && $this->cart_quantity() != false && $this->cart_quantity() > 0 && $data['payment_method'] != null && $data['address_id'] != null)
        {
            $cart_data = $this->get_cart_data();
            // dd($cart_data);
            $user = Auth::user();
            $customer_address_id = base64_decode($data['address_id']);
            $customer_address = Customeraddress::where('id',$customer_address_id)->first();

            // dd($cart_data);

            $result = Order::create(
                [
                    'user_id'=> $user->id,
                    'cart_total'=> $cart_data['cart_total'],
                    'delivery_charge'=> $cart_data['delivery_charges'],
                    'discount' => $cart_data['delivery_charges'],
                    'grand_total' => $cart_data['grand_total'],
                    'payment_method' => $data['payment_method'],
                    'cart' => serialize($cart_data['cart_items']),
                    'name' => $user->name,
                    'mobile' => $user->mobile,
                    'contact_name' => $customer_address->name,
                    'contact_number' => $customer_address->mobile_number,
                    'address' => $customer_address->address,
                    'state' => $customer_address->state,
                    'city' => $customer_address->city,
                    'pincode' => $customer_address->pincode
                ]
            );


            if ($result)
            {
                $this->cart_destroy();

                $order_id = $result->id;
                //Send sms to customer and dadus team
                $mask = 'daduss';
                $unicode = 0;

                $order_msg = DB::table('sms_template')->where('template_title','=','pending')->pluck('template_description');

                $message = "Dear " . $user->name . ", ".$order_msg[0]." Order Details - Order Id: " . $order_id . ", Total: " . $cart_data['grand_total'];

                $message_team = 'New order received with following details Order Id: ' . $order_id . ' Customer Name: ' . $customer_address->name . ' Total: ' . $cart_data['grand_total'] . ' Contact: ' . $user->mobile;

                $this->mail($user->id, $order_id, $order_msg[0]);

                //fetch mobile number list start
                $mobile_numbers = DB::table('mobile_number')->select('mobile_number')->where('status',1)->get();          // Dadus' team numbers
                $mobile_number_array = array_column($mobile_numbers,'mobile_number');


                $number_list = implode(',', $mobile_number_array);
                //fetch mobile number list end here

                $sendmessagearray = array('mask' => $mask, 'mobile' => $user->mobile, "message" => preg_replace("|\&|", "^*^*^", $message), 'from' => 3, 'unicode' => $unicode);

                $sendmessagejson = json_encode($sendmessagearray);
                $sendmessagedata = 'sendmessage=' . $sendmessagejson;
                $sendotpmessage = $this->sendsms($sendmessagejson, $sendmessagedata);

                //send sms to team
                $sendmessagearray_team = array('mask' => $mask, 'mobile' => $number_list, "message" => preg_replace("|\&|", "^*^*^", $message_team), 'from' => 3, 'unicode' => $unicode);

                $sendmessagejson_team = json_encode($sendmessagearray_team);
                $sendmessagedata_team = 'sendmessage=' . $sendmessagejson_team;
                $sendotpmessage = $this->sendsms($sendmessagejson_team, $sendmessagedata_team);

                return redirect('/invoice/'. base64_encode($order_id));
            }
            else
            {
                return redirect('/cart')->with('error_msg','Something went wrong');
            }
        }
        else
        {
            return redirect('/cart')->with('error_msg','Something went wrong');
        }
    }

    public function invoice($order_id)
    {
        try {

            if(Auth::user())
            {
                    $order_id = base64_decode($order_id);
                    $order = Order::where('id',$order_id)->where('user_id',Auth::user()->id)->first();
                    if($order != null)
                    {
                        $order->cart = unserialize($order->cart);
                        return view('pages.ordercomplete', [ 'order' => $order]);
                    }
                    else
                    {
                        return redirect('/cart')->with('error_msg','Order not found');
                    }
            }
            else
            {
                return redirect('/login')->with('error_msg','Please login');
            }

        } catch (\Exception $e) {
            return redirect('/cart')->with('error_msg','Something went wrong');
        }
    }

    public function redirect() {
        return Socialite::driver('facebook')->redirect();
    }

    public function googleredirect() {
        return Socialite::driver('google')
        ->scopes(['openid', 'profile', 'email'])
        ->redirect();
    }

    public function googlecallback() {
        $user = Socialite::driver('google')->user();
        if ($user) {
            $name = $user->name;
            $email = $user->email;
            $check_user = DB::table('users')->where('email', $email)->first();
            ;

            if ($check_user) {
                Session::put('userid', $check_user->id);
                Session::put('islogin', 1);
                return Redirect('/checkoutstep3');
            } else {
                $create_user = DB::table('users')->insert(
                    ['email' => $email, 'name' => $name]
                );
                Session::put('userid', $create_user->id);
                Session::put('islogin', 1);
                return Redirect('/checkoutstep3');
            }
        }
    }

    public function sendsms($sendmessagejson, $sendmessagedata)
    {
        $params = "";
        $msg_data=json_decode($sendmessagejson , true );
        $msg_data['username'] = 'dadusmv';
        $msg_data['password'] = '123456';
        $msg_data['sms_type'] = '2';
        $msg_data['from'] = $msg_data['mask'];
        $msg_data['to'] = $msg_data['mobile'];
        $msg_data['message'] = urlencode($msg_data['message']);

        unset($msg_data['mobile'],$msg_data['mask']);
        foreach ($msg_data as $key => $value)
        {
            $params = $params.'&'.$key.'='.$value;
        }

        $url = 'http://www.metamorphsystems.com/index.php/api/bulk-sms';

        $params = trim($params, '&');
//        dd($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url.'?'.$params); //Url together with parameters
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $result = curl_exec($ch);
        curl_close($ch);

        if($result)
            return true;
        else
            return false;
    }


    public function logout() {
        Auth::logout();
        return redirect('/');
    }


    public function search_result(Request $request) {
        $data = $request->all();
        $search = $data['search'];
        $res = "";
        $searchres = Products::where('prod_title', 'like', '%' . $search . '%')->get();


        if (sizeof($searchres) > 0) {
            foreach ($searchres as $key => $value) {
                $res = '<form method="POST" action="{{ url(/cart) }}">
                    <input type="hidden" name="_token" value="' . csrf_token() . '">
                    <div class="col-sm-3 product">
                    <div class="productdiv">
                    <div class="product-img-box">
                    <img src="{{ url(/images/products/) }}' . $value["prod_image"] . '" alt="menu" class="img-responsive">
                    </div>
                    <div class="detail-box">
                    <div class="food-type veg"></div><h3>' . $value["prod_title"] . '</h3>
                    <h3>' . $value["prod_desc"] . '</h3>
                    <div class="price">
                    <span class="amount">₹ ' . $value["prod_price"] . '</span>
                    <span class="add_cart"><button type="submit" class="button add_to_cart_button">Add to Cart</button></span>

                    <div class="">
                    </div>
                    </div>
                    </div>
                    </div>
                    <input type="hidden" name="product_id" value="' . $value["id"] . '">
                    <span class="order">
                    </span>
                    </div>
                    </form>';

                    echo $res;
                }
            } else {
                $res = '<div class=""><img src="{{ url("images/noProductFound.png") }}" alt="menu" class="img-responsive"></div>';

                echo $res;
            }
        }

        public function search_result1(Request $request) {
            $data = $request->all();
            $search = $data['search'];
            $res = "";
            $searchres = Products::where('prod_title', 'like', '%' . $search . '%')->take(10)->get();

            if (!empty($searchres)) {
                foreach ($searchres as $key => $value) {
                    $url1 = url('product/'.$value["product_slug"]);
                    $url2 = url('images/products/'.$value["prod_image"]);
                    $res = '<div><a href="'.$url1.'"><img src="'.$url2.'" style="width:8%;">' . $value["prod_title"] . '</a></div>';
                    echo $res;
                }
            } else {
                echo '';
            }
        }

        public function pincodestrictcheck(Request $request) {
            $data = $request->all();
            $pin = $data['pin'];

            if (!empty($pin)) {
                $delivery_pincode = DB::table('delivery_pincode')->where('status', '=', 1)->where('pincode', 'like', $pin . '%')->get();

                if (empty($delivery_pincode)) {
                    return response()->json(['status' => false, 'message' => 'Delivery Not Available to this pincode.']);
                } else {

                }
            }
        }

        public function update_cart_qty(Request $request)
        {
            $data = $request->only(['pwid','action']);

            if( $data['pwid'] != null && $data['action'] != null )
            {
                $pwid_arr = explode('_', $data['pwid']);

                $product_id = $pwid_arr[0];
                $weight = $pwid_arr[1];
                $action = $data['action'];

                switch ($action) {

                    case 'add':
                    if($this->cart_add($product_id,1,$weight))
                    echo 1;
                    else
                    echo 0;
                    break;

                    case 'remove':
                    if($this->cart_remove($product_id,1,$weight))
                    echo 1;
                    else
                    echo 0;
                    break;

                    default:
                    echo 0;
                    break;
                }

            }
            else
            {
                echo 0;
            }
        }

        public function cart_quantity()
        {
            try {
                if (session()->has('cart_token'))
                {
                    $cart_token = session()->get('cart_token');
                    $cart = CartModel::where('cart_token', $cart_token)->first();
                    if ($cart != false)
                    {
                        return array_sum(array_column(unserialize($cart->cart_data),'qty'));
                    }
                }

                return false;

            } catch (\Exception $e) {
                return false;
            }
        }


        public function cart_destroy()
        {
            try {
                if (session()->has('cart_token'))
                {
                    $cart_token = session()->get('cart_token');
                    if(CartModel::where('cart_token', $cart_token)->delete())

                    {
                        session()->forget('cart_token');
                        return true;
                    }
                }
                return false;

            } catch (\Exception $e) {
                return false;
            }
        }

        // public function test()
        // {
        //     dd ( $this->products::where('id',199)->with('weight')->first());
        // }


        public function storelocator(){
            return view('pages.storelocator');
        }

        public function career(){
            return view('pages.career');
        }

        // public function aboutus(){
        //     return view('pages.aboutus');
        // }

        public function faq(){
            return view('pages.faq');
        }

         public function contact_submit(Request $request)
        {


            //dd($request->name);

              $validator = Validator::make($request->all(), [
                'name' => 'required',
                'phone' => 'required',
                'email' => 'required|email',
                'message' => 'required'
            ]);

              // if($validator->fails()) {
              //       return Redirect::back()->withErrors($validator);
              //       return redirect()->back()->with($validator);

              //   }

            DB::table('contact_us')->insert([
                ['name' => $request->name, 'phone' => $request->phone, 'email' => $request->email,'message' => $request->message]
            ]);

             // $data = ['name' => $request->name, 'phone' => $request->phone ,'user_email' => $request->email, 'message' => $request->message ];

             //    Mail::send('email.contact', $data, function($message) {
             //        $message->to('rahuljat@firsteconomy.com');
             //        $message->subject('Dadus Form submitted');
             //    });

            return redirect()->back()->with('message', 'Thank you for contacting us!');

        }

        public function privacypolicy(){
            return view('pages.privacy_policy');
        }

        public function contactus(){
            return view('pages.contactus');
        }

        public function my_account()
        {
            if(Auth::check())
            {
                $id = Auth::id();
                $user = new User;
                $user_order_details = $user::where('id',$id)->with(['orders','orders.status'])->first();
                // dd($user_order_details);
                return view('pages.myaccount', ['user_order_details' => $user_order_details ]);
            }
            else
            {
                return redirect('/login');
            }
        }

        public function update_profile(Request $request)
        {
            if(Auth::check())
            {

                $validatedData = $this->validate($request, [
                       'name' => 'required|max:50|regex:/^[\pL\s\-]+$/u',
                       'email' => 'required|email',
                       'dob' => 'required|date']);
                try {

                    $data = $request->only(['name','email','dob']);
                    $user = Auth::user();
                    $user->name = $data['name'];
                    $user->email = $data['email'];
                    $user->dob = $data['dob'];
                    $user->save();
                    return Redirect::back()->with(['msg', '<div class="alert alert-success" role="alert">Profile updated</div>']);
                } catch (\Exception $e) {
                    return Redirect::back()->with(['msg', '<div class="alert alert-danger" role="alert">Something went wrong</div>']);
                }
            }
            else {
                return redirect('/login');
            }


        }

        public function cancel_order(Request $request)
        {
            if(Auth::check())
            {
                $user = Auth::user();
                $order_id = $request->input('oid');
                if($order_id != null)
                {
                    $res = Order::where('id', $order_id)->where('user_id',$user->id)->update(['order_status' => 6]);
                    if($res == 1)
                    {
                        $cancellation_msg = DB::table('sms_template')->where('template_title','=','cancelled')->pluck('template_description');

                        $mask = 'daduss';
                        $unicode = 0;

                        $message = "Dear " . $user->name ." ". $cancellation_msg[0] . " Order Details - order id: ". $order_id;
                        $message_team = "Order cancellation request for order with order id: " . $order_id . " is received";

                        $this->mail($user->id, $order_id, $cancellation_msg[0]);

                        //fetch mobile number list start
                        $mobile_numbers = DB::table('mobile_number')->select('mobile_number')->where('status',1)->get();          // Dadus' team numbers
                        $mobile_number_array = array_column($mobile_numbers,'mobile_number');
                        $number_list = implode(',', $mobile_number_array);
                        //fetch mobile number list end here

                        $sendmessagearray = array('mask' => $mask, 'mobile' => $user->mobile, "message" => preg_replace("|\&|", "^*^*^", $message), 'from' => 3, 'unicode' => $unicode);
                        $sendmessagejson = json_encode($sendmessagearray);
                        $sendmessagedata = 'sendmessage=' . $sendmessagejson;
                        $sendotpmessage = $this->sendsms($sendmessagejson, $sendmessagedata);

                        //send sms to team
                        $sendmessagearray_team = array('mask' => $mask, 'mobile' => $number_list, "message" => preg_replace("|\&|", "^*^*^", $message_team), 'from' => 3, 'unicode' => $unicode);
                        $sendmessagejson_team = json_encode($sendmessagearray_team);
                        $sendmessagedata_team = 'sendmessage=' . $sendmessagejson_team;
                        $sendotpmessage = $this->sendsms($sendmessagejson_team, $sendmessagedata_team);
                         // var_dump($sendotpmessage);die;
                        return 1;
                    }
                }
            }
            return 0;
        }

        public function login()
        {
            if(!Auth::check())
            {
                return view('pages.login');
            }
            else
            {
                    return redirect('/cart');
            }
        }

        public function mail($user_id, $order_id , $msg)
        {
            try {


                $user = User::find($user_id);
                $order = Order::find($order_id);
                // dd($order);

                if($user != null && $order != null && isset($user->email) && $user->email != null )
                {
                    $data = [ 'name' => $user->name , 'order_id' => $order->id , 'total' => $order->grand_total , 'msg'  => $msg , 'user_email' => $user->email ];

                    Mail::send('email.template', $data, function($message) use ( $data ) {
                        $message->to($data['user_email']);
                        $message->subject('Dadus Order Update');
                    });
                    return true;
                }
                else
                {
                    return false;
                }


            } catch (\Exception $ex) {
                // dd($ex);
                return false;
            }
        }

    }

    ?>
