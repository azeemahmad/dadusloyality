<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\RewardPointsTransaction;
use DB;

class ApiController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->user = new User;
        $this->reward_trans = new RewardPointsTransaction;
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function check_points(Request $request)
    {
        try {

            $postdata = $request->only('mobile','name','email');

            if($postdata['mobile'] == null)
                return json_encode(['code'=>417,'data'=>null,'message'=>'Mobile number is required']);

            if($postdata['name'] == null)
                return json_encode(['code'=>417,'data'=>null,'message'=>'Name is required']);

            $user = $this->user::where('mobile',$postdata['mobile'])->first();
            if($user !== null)
            {
                if($user->reward_points == 0 || $user->reward_points == null )
                    $reward_points = 0;
                else
                    $reward_points = $user->reward_points;

                $data = [
                    'reward_points' => $reward_points
                ];

                return json_encode(['code'=>200,'data'=> $data ,'message'=>'User account exists']);
            }
            else
            {
                $data = [
                    'reward_points' => 0                 /** For new account reward points will be zero */
                ];

                $postdata['otp_status'] = 1;

                $user = $this->user::create($postdata);
                if($user)
                    return json_encode(['code'=>200,'data'=> $data ,'message'=>'User account created']);
                else
                    return json_encode(['code'=>417,'data'=>null,'message'=>'Unable to create user account']);
            }

        } catch (\Exception $e) {
            return json_encode(['code'=>500,'data'=>null,'message'=>'Something went wrong']);
        }
    }

    /**
    *    Calculates the reward point and records transaction
    *
    *    Ratio is defined in reward_points_ratio table
    *
    **/
    public function upload_bill(Request $request)
    {
        try {
            DB::beginTransaction();

            $postdata = $request->only('mobile','bill_amount','points_used','bill_no');

            if($postdata['mobile'] == null)
                return json_encode(['code'=>417,'data'=>null,'message'=>'Mobile number is required']);

            if($postdata['bill_amount'] == null)
                return json_encode(['code'=>417,'data'=>null,'message'=>'Bill amount is required']);

            if($postdata['points_used'] == null)
                return json_encode(['code'=>417,'data'=>null,'message'=>'Points Used is required, send 0 if no point is used']);

            if($postdata['bill_no'] == null)
                return json_encode(['code'=>417,'data'=>null,'message'=>'Bill No. is required']);

            $user = $this->user::where('mobile',$postdata['mobile'])->first();

            if($user != null )
            {
                if($postdata['points_used'] == 0)
                {
                    $ration_details = DB::table('reward_points_ratio')->where('status',1)->first();
                    // dd($ration_details);
                    if($ration_details == null)
                        return json_encode(['code'=>417,'data'=>null,'message'=>'Reward ration not set']);

                    $amount = (float)$postdata['bill_amount'];

                    $ratio_amount = $ration_details->amount;
                    $ratio_points = $ration_details->points;

                    $points_earned = (int)(($amount / $ratio_amount) * $ratio_points);

                    // dd($postdata);die;

                    $res_1 = $this->reward_trans::create([
                        'mobile' => $postdata['mobile'],
                        'points_used' => $points_earned,
                        'transaction_amount' => $amount,
                        'bill_no' => $postdata['bill_no'],
                        'mode' => 2,
                    ]);

                    $user->reward_points = $user->reward_points + $points_earned;
                    $res_2 = $user->save();

                    if($res_1 && $res_2)
                    {
                        $data = [
                                    'points_earned' => $points_earned
                                ];

                        DB::commit();
                        return json_encode(['code'=>200,'data'=> $data,'message'=>'Points updated']);
                    }
                    else
                    {
                        DB::rollBack();
                        return json_encode(['code'=>500,'data'=>null,'message'=>'Unable to update']);
                    }
                }
                else
                {
                    $res_1 = $this->reward_trans::create([
                        'mobile' => $postdata['mobile'],
                        'points_used' => (-1) * $postdata['points_used'],
                        'transaction_amount' => $postdata['bill_amount'],
                        'bill_no' => $postdata['bill_no'],
                        'mode' => 2,
                    ]);


                    if( $user->reward_points - $postdata['points_used'] < 0 )
                    {
                        DB::rollBack();
                        return json_encode(['code'=>400,'data'=>null,'message'=>'Unable to process, points used is greater than points balance']);
                    }
                    else
                    {
                        $user->reward_points = $user->reward_points - $postdata['points_used'];
                        $res_2 = $user->save();
                    }

                    $data = [
                                'points_earned' => 0
                            ];

                    if($res_1 && $res_2)
                    {
                        DB::commit();
                        return json_encode(['code'=>200,'data'=> $data ,'message'=>'Points updated']);
                    }
                    else
                    {
                        DB::rollBack();
                        return json_encode(['code'=>500,'data'=>null,'message'=>'Unable to update']);
                    }

                }
            }
            else
            {

            }

        } catch (\Exception $e) {
             DB::rollBack();
            dd($e);
            return json_encode(['code'=>500,'data'=>null,'message'=>'Something went wrong']);
        }
    }
}
