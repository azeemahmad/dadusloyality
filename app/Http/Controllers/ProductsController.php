<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Config;
use App\Models\Products;
use App\Models\Categories;

class ProductsController extends Controller
{

    public function index($category_slug = null)
    {
        $product = new Products;
        $products = $product->category_wise_products($category_slug);
        $category = [];

        if($category_slug)
        {
            $category = Categories::where('category_slug',$category_slug)->first();
        }
        else
        {
            $category = Categories::where('category_slug','all_products')->first();
        }
        if($products!= false)
    	   return view('pages.productlist',['products' => $products, 'category'=> $category ] );
        else
            return view('pages.productlist',['products' =>[] ,'category'=> [] ]);
    }

    public function fetchProducts($product_slug = null)
    {
        if(!empty($product_slug))
        {
            $prod = Products::where('product_slug','=',$product_slug)->with(['weight','category'])->first();
            if($prod==null||$prod==false)
            {
            	return redirect(url('/'));
            	// return view('pages.productnotfound');
            }

            $category_title = $prod->category->pluck('title')->toArray();
            $categories = implode(' - ', $category_title);

            return view('pages.products',['prod' => $prod , 'categories'=>$categories]);
        }
        else
        {
            return view('pages.productnotfound');
        }
    }

    public function fetchProductsdetails($product_slug = null)
    {
        if(!empty($product_slug))
        {

            $prod = Products::where('product_slug','=',$product_slug)->where('prod_active',1)->first();


            return view('pages.products_details',['prod' => $prod ]);
        }
        else
        {
            return view('pages.productnotfound');
        }
    }
}
