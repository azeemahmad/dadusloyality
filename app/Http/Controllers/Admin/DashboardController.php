<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Config;
use App\Models\Products;
use App\Models\Categories;
use App\Models\Transaction;
use Validator;
use App\Models\User;
use App\Models\Delivery_pincode;
use App\Models\Weights;
use App\Customer;
use App\Models\Order;
use App\Models\Status;
// use Session;
use Illuminate\Support\Facades\Session;
use Redirect;
use Excel;

class DashboardController extends Controller
{
    function __construct()
    {
         $this->middleware('web');
         $this->weight = new Weights;
         $this->status = new Status;
    }
    public function index()
    {
        return view('admin.pages.login');
    }

    public function event_log($data,$action){
        $logdata = json_encode($data,true);
        $name = session('name');
        $userid = session('id');
        $res = DB::table('user_log')->insert(array('username'=>$name,'userid'=>$userid,'action'=>$action,'data'=>$logdata));

        if($res!==false){
            return false;
        } else {
            return true;
        }
    }

    public function login_submit(Request $request){
        $data = $request->all();
        $user_details = DB::table('admin_users')->where('username','=',$data['username'])->where('password','=',$data['password'])->first();
        if($user_details){
            $request->session()->put('name', $user_details->name);
            $request->session()->put('id', $user_details->id);
            $request->session()->put('username', $user_details->username);
            $request->session()->put('access_level', $user_details->access_level);

            if($user_details->access_level == 3)
                return redirect('adm/home');

            return redirect('adm/dashboard')->with('message','Login Succesfully.');
        }else{
            return redirect('adm/')->with('message','Invalid Username and Password. Please try again.');
        }
    }

    public function manage_sms_template(Request $request){
        $checkpermission = $this->checkuserlogin();
        if($checkpermission){
            $manage_sms_template = DB::table('sms_template')->get();
            return view('admin.pages.manage_sms_template',[ 'manage_sms_template' => $manage_sms_template]);
        }else{
            return redirect('adm/')->with('message','Login to Access Panel. Please try again.');
        }
    }

    public function edit_smstemplate_submit(Request $request){
        $data = $request->all();
        if(!empty($data))
        {
            $res = DB::table('sms_template')->where('id','=',$data['template_id'])->update(array('template_description'=>$data['template_description']));

            if($res!==false){
                $manage_sms_template = DB::table('sms_template')->get();
                $action = "SMS template Updated";
                $eventlog = $this->event_log($data,$action);
                session()->put('message', 'Template Updated Successfully');
                return redirect('adm/manage_sms_template');
            }else{
                session()->put('message', 'Something went wrong. Please try again');
                return redirect('adm/manage_sms_template');
            }
        }
        else
        {
            session()->put('message', 'Something went wrong. Please try again');
            return redirect('adm/manage_sms_template');
        }
    }

    public function logout(Request $request){
        $request->session()->forget('username');
        $request->session()->flush();

        return redirect('adm/')->with('message','Logout Succesfully.');
    }

    public function dashboard()
    {
        $checkpermission = $this->checkuserlogin();
        if($checkpermission){
            $product = Products::where('status','=',1)->count();
            $orders = Order::where('order_status','=',1)->count();
            return view('admin.pages.dashboard',compact('product','orders'));
        }
        else
        {
            return redirect('adm/')->with('message','Login to Access Panel. Please try again.');
        }
    }

    public function send_sms(Request $request){

        $data = $request->all();

        $sms = $data['sms_body'];

        $customer_ids = array();
        $customer_ids = $data['custids'];

        $customer_details = DB::table('users')->whereIn('id', $customer_ids)->get();
        $customer_mobile_number = array();
        foreach ($customer_details as $key => $value) {
            $customer_mobile_number[] = $value->mobile;
        }

        $number_list = implode(',',$customer_mobile_number);

        $user = 'dadusmv';
        $password = '123456';
        $message = urlencode ($sms);
        $senderid = 'mtmrph';
        $sms_type = 2;

        $url="http://www.metamorphsystems.com/index.php/api/bulk-sms?username=".$user."&password=".$password."&from=".$senderid."&to=".$number_list."&message=".$message."&sms_type=".$sms_type."";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($ch);

        if ($curl_response === false) {
            $info = curl_getinfo($ch);
            curl_close($ch);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($ch);
        $decoded = explode(',', $curl_response);

        $data = array();
        $data['numberlist'] = $number_list;
        $data['sms'] = $sms ;
        $action = "SMS SENT";
        $eventlog = $this->event_log($data,$action);

        if (isset($decoded[0]) && trim($decoded[0] == 'fail')) {
            return response()->json(['msg' => 'Fail to send SMS', 'error' => true]);

        }else{

            return response()->json(['msg' => 'Success', 'error' => false]);
        }

    }

    public function add_product()
    {
        $checkpermission = $this->checkuserlogin();
        if($checkpermission){
            $categories = new Categories;
            $all_categories = $categories::all();

            return view('admin.pages.add_product',[ 'categories' => $all_categories]);
        }else{
            return redirect('adm/')->with('message','Login to Access Panel. Please try again.');
        }
    }

    public function add_category()
    {
        $checkpermission = $this->checkuserlogin();
        $categories = new Categories;
        $all_categories = $categories::where('parent_id','=',0)->get();
        if($checkpermission){
            return view('admin.pages.add_category',compact('all_categories'));
        }else{
            return redirect('adm/')->with('message','Login to Access Panel. Please try again.');
        }
    }

    //add mobile number start here
    public function add_mobilenumber()
    {
        $checkpermission = $this->checkuserlogin();
        if($checkpermission){
            return view('admin.pages.add_mobilenumber');
        }else{
            return redirect('adm/')->with('message','Login to Access Panel. Please try again.');
        }
    }
    //add mobile number end here

    //add mobile number submit start here
    public function add_mobile_submit(Request $request)
    {
        $data = $request->all();
        //dd($data);
        if(!empty($data))
        {
            $res = DB::table('mobile_number')->insert(array('mobile_number'=>$data['mobile_number'],'status'=>$data['status']));
            if($res!==false){
                session()->put('message', 'Mobile Number Added Successfully');
                return redirect('adm/mobile_number');
            } else {
                session()->put('message', 'Something went wrong. Please try again');
                return redirect('adm/mobile_number');
            }
        }
        else
        {
            session()->put('message', 'Something went wrong. Please try again');
            return redirect('adm/mobile_number');
        }
    }
    //add mobile number end here
    //view mobile number start here
    public function mobile_number()
    {
        $checkpermission = $this->checkuserlogin();
        if($checkpermission){
            $mobile_number = DB::table('mobile_number')->get();
            return view('admin.pages.manage_mobilenumber',[ 'mobile_number' => $mobile_number]);
        }else{
            return redirect('adm/')->with('message','Login to Access Panel. Please try again.');
        }
    }
    //view mobile number end here

    //manage mobile number start here
    public function edit_mobile_number($mobile_id)
    {

        if(!empty($mobile_id))
        {
            $mobile_number = DB::table('mobile_number')->where('id','=',$mobile_id)->first();

            return view('admin.pages.edit_mobilenumber',['mobile_number' => $mobile_number ]);
        }
        else
        {
            return redirect('adm/mobile_number')->with('fail_msg','Something went wrong, id not found');
        }

    }
    //manage mobile number end here

    //edit mobile number submit start here
    public function edit_mobilenumber_submit(Request $request){
        $data = $request->all();

        if(!empty($data))
        {
            $res = DB::table('mobile_number')->where('id','=',$data['id'])->update(array('mobile_number'=>$data['mobile_number'],'status'=>$data['status']));

            if($res!==false){
                session()->put('message', 'Mobile Number Updated Successfully');
                return redirect('adm/mobile_number');
            } else {
                session()->put('message', 'Something went wrong');
                return redirect('adm/mobile_number');
            }
        }
        else
        {
            session()->put('message', 'Something went wrong');
            return redirect('adm/mobile_number');
        }
    }

    //edit mobile number submit end here


    //delete mobile number start here
    public function delete_mobilenumber_submit(Request $request)
    {
        $data = $request->all();
        $mid = $data['pid'];

        if(!empty($mid))
        {
            $res = DB::table('mobile_number')->where('id','=',$mid)->delete();//

            if($res!==false)
            {
                return redirect('adm/add_mobilenumber')->with('message','Mobile Number deleted successfully');
            }
            else {
                return redirect('adm/add_mobilenumber')->with('message','Something went wrong');
            }
        }
        else
        {
            return redirect('adm/add_mobilenumber')->with('message','Something went wrong, Mobile Number not found');
        }
    }
    //delete mobile number end here


    // edit & delete product start here
    public function edit_product($product_id)
    {
        $products = new Products;
        $categories = new Categories;


        if(!empty($product_id))
        {
            $product_data = $products::with(['category','weight'])->where('id',$product_id)->first();
            $all_categories = $categories::all();

		$current_weight_data['one_p_price'] = null;
        $current_weight_data['one_p_id'] =  null;
        $current_weight_data['one_g_price'] = null;
        $current_weight_data['one_g_id'] =  null; 
        $current_weight_data['one_k_price'] =  null;
        $current_weight_data['one_k_id'] =  null;   
        foreach ( $product_data->weight as $wkey => $wvalue){
         switch($wvalue->weight){
             case '1 Piece':
             	$current_weight_data['one_p_price'] = $wvalue->price;
                $current_weight_data['one_p_id'] = $wvalue->id;
                break;
                                                     	                                      
            case '500 Grams':                                           
                    $current_weight_data['one_g_price'] = $wvalue->price;
                    $current_weight_data['one_g_id'] = $wvalue->id;                                           
             break;
               
             case '1 KG':                                         
                    $current_weight_data['one_k_price'] = $wvalue->price;
                    $current_weight_data['one_k_id'] = $wvalue->id;                                           
             break;
              
        	}
    	}



            $selected_categories = array_column( $product_data->category->toArray(), 'id');
            $selected_categories_csv = implode(',',$selected_categories);

            return view('admin.pages.edit_product',[ 'categories' => $all_categories, 'product' => $product_data , 'selected_categories' => $selected_categories_csv , 'current_weight_data' => $current_weight_data ]);
        }
        else
        {
            return redirect('adm/manage_product')->with('fail_msg','Something went wrong, id not found');
        }
    }

    public function edit_category($category_id)
    {
        $categories = new Categories;
        if(!empty($category_id))
        {
            $category = Categories::where('id','=',$category_id)->first();
            $parent_categories = Categories::whereNotIn('id',[$category_id])->where('parent_id',0)->get();
            // dd($parent_categories);
            return view('admin.pages.edit_category',[ 'parent_categories' => $parent_categories, 'category' => $category ]);
        }
        else
        {
            return redirect('adm/manage_category')->with('fail_msg','Something went wrong, id not found');
        }
    }

    //manage delivery pincode start
    public function delivery_pincode()
    {
        $checkpermission = $this->checkuserlogin();
        if($checkpermission){
            $delivery_pincode = DB::table('delivery_pincode')->get();//dd($delivery_pincode);
            return view('admin.pages.manage_pincode',[ 'delivery_pincode' => $delivery_pincode]);
        }else{
            return redirect('adm/')->with('message','Login to Access Panel. Please try again.');
        }
    }
    //manage delivery pincode end

    //add delivery pincode start
    public function add_pincode()
    {
        $checkpermission = $this->checkuserlogin();
        if($checkpermission){
            return view('admin.pages.add_pincode');
        }else{
            return redirect('adm/')->with('message','Login to Access Panel. Please try again.');
        }
    }
    //add delivery pincode end

    //edit delivery pincode start
    public function edit_pincode($pin_id)
    {

        if(!empty($pin_id))
        {
            $pincode = DB::table('delivery_pincode')->where('id','=',$pin_id)->first();//dd($pincode);

            return view('admin.pages.edit_pincode',['pincode' => $pincode ]);
        }
        else
        {
            return redirect('adm/delivery_pincode')->with('fail_msg','Something went wrong, id not found');
        }

    }
    //edit delivery pincode end
    //edit delivery pincode submit start
    public function add_pincode_submit(Request $request)
    {
        $data = $request->all();
        $datein = date("Y-m-d H:i:s");
        if(!empty($data))
        {
            $res = DB::table('delivery_pincode')->insert(array('pincode'=>$data['pincode'],'status'=>$data['status'],'created_at'=>$datein));//dd($res);

            if($res!==false){
                $action = "New Pincode Added";
                $eventlog = $this->event_log($data,$action);
                return redirect('adm/delivery_pincode')->with('success_msg','Pincode Added successfully');
            } else {
                return redirect('adm/delivery_pincode')->with('fail_msg','Something went wrong');
            }
        }
        else
        {
            return redirect('adm/delivery_pincode')->with('fail_msg','Something went wrong, id not found');
        }

    }
    //edit delivery pincode submit end

    //edit delivery pincode submit start
    public function edit_pincode_submit(Request $request)
    {
        $data = $request->all();

        if(isset($data['dadus_pin_id']) && $data['dadus_pin_id'] !='')
        {
            $res = DB::table('delivery_pincode')->where('id','=',$data['dadus_pin_id'])->update(array('pincode'=>$data['pincode'],'status'=>$data['status']));//dd($pincode);

            if($res!==false){
                $action = "Pincode Updated";
                $eventlog = $this->event_log($data,$action);
                return redirect('adm/delivery_pincode')->with('success_msg','Pincode Updated successfully');
            } else {
                return redirect('adm/delivery_pincode')->with('fail_msg','Something went wrong');
            }
        }
        else
        {
            return redirect('adm/delivery_pincode')->with('fail_msg','Something went wrong, id not found');
        }

    }
    //edit delivery pincode submit end


    public function delete_pincode_submit(Request $request)
    {
        $data = $request->all();

        $pid = $data['pid'];
        $pincode_details = DB::table('delivery_pincode')->where('id','=',$pid)->first();

        if(!empty($pid))
        {
            $res = DB::table('delivery_pincode')->where('id','=',$pid)->delete();//

            if($res!==false)
            {
                $action = "Pincode Deleted";
                $eventlog = $this->event_log($pincode_details,$action);
                return redirect('adm/delivery_pincode')->with('success_msg','Product deleted successfully');
            }
            else {
                return redirect('adm/delivery_pincode')->with('fail_msg','Something went wrong');
            }
        }
        else
        {
            return redirect('adm/delivery_pincode')->with('fail_msg','Something went wrong, id not found');
        }
    }

    //manage delivery charges start
    public function delivery_charges()
    {
        $checkpermission = $this->checkuserlogin();
        if($checkpermission){
            $delivery_charges = DB::table('delivery_charges')->first();
            return view('admin.pages.manage_delivery',[ 'delivery_charges' => $delivery_charges]);
        }else{
            return redirect('adm/')->with('message','Login to Access Panel. Please try again.');
        }
    }
    //manage delivery charges end

    //update delivery charges start
    public function delivery_charges_submit(Request $request)
    {
        $this->validate($request, [
            'delivery_charges' => 'required',
            'min_cart_value' => 'required'
        ],['required' => 'This field is required']
    );

    $data = $request->all();

    $values=array('delivery_charges'=>$data['delivery_charges'],'min_cart_value'=>$data['min_cart_value']);
    $query = DB::table('delivery_charges')
    ->where('id', 1)
    ->update($values);

    if($query !== false){
        $action = "Delivery Charges Updated";
        $eventlog = $this->event_log($data,$action);
        session()->put('message', 'Charges Updated Successfully');
        return redirect('adm/delivery_charges');
    } else {
        session()->put('message', 'Something went wrong');
        return redirect('adm/delivery_charges');
    }
}
//update delivery charges

public function edit_product_submit(Request $request)
{
	$data = $request->all();
	//dd($data);
	$prod_price = $data['prod_price']; 

	//dd($prod_price);

	$pro_id = $data['id'];

	$test=Weights::where('products_id' ,$pro_id)->get();
	
	//dd($test[0]['id']);
	//foreach ($test as $key => $value) {
	//	$weight_id=$test[$key]['id'];
	//}

	//dd($weight_id);
    $products = new Products;
    //dd($products);


    try {

        if ($request->hasFile('prod_image')) {
            $image = $request->file('prod_image');
            $data['prod_image']  = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('images/products');
            $image->move($destinationPath, $data['prod_image']);
        }
        else
        {
            $data['prod_image'] = $data['prod_image_old'];
        }

        $category_id = $data['category_id'];
        $weight = $data['prod_price'];

        unset($data['_token'],$data['prod_image_old'],$data['content'],$data['category_id'],$data['prod_price']);

        $res = $products::find($data['id']);

        $data['product_slug'] = str_replace(' ', '-',  strtolower($data['prod_title']));
        $p = $res->update($data);
        // $prod = $products::where('id',$data['id'])->with('category')->first();
        // dd($res);

        if($res!==false)
        {
            $res->category()->sync( $category_id );
          

            foreach ($prod_price as $wt => $wtvalue) {
            	
	           	$price_val = reset($wtvalue);
				$wt_id = key($wtvalue);
				//dd( $prod_price , $first_key , $first_val );
            	if( trim($wt_id) != '' && $wt_id != null)
            	{
            		$this->weight::where('id', $wt_id)->update(['price' => $price_val]);	
            	}
            	else
            	{	

            		//DB::enableQueryLog();
            		$this->weight::create([
            						'products_id' => $pro_id,
            						'weight' => $wt,
            						'price' => 	$price_val,
            						'status' => 1
            						]);	
            		/*dd(
            			DB::getQueryLog()
        			); */
            	}
            }

            /*foreach ($weight as $weight_id => $price)
            {	
            	$weight_id=$test[$weight_id]['id'];
            	//dd($weight_id);
                $this->weight::where('id', $weight_id)->update(['price' => $price]);
            }*/


            //$res->weight()->sync( $category_id );
            return redirect('adm/manage_product')->with('success_msg','Product Updated successfully');
        }
        else
        {
            return redirect('adm/manage_product')->with('fail_msg','Something went wrong');
        }
            } catch (\Exception $e) {
            	//dd($e);
                return redirect('adm/manage_product')->with('fail_msg','Something went wrong');
            }
    }


public function delete_product_submit(Request $request)
{
    $data = $request->all();
    $products = new Products;
    $pid = $data['pid'];

    $product_data = $products->get_product_by_id($pid);

    if(!empty($pid))
    {
        $result = $products->delete_product($pid);
        if($result!==false)
        {
            $action = "Delete Product";
            $eventlog = $this->event_log($product_data,$action);
            return redirect('adm/manage_product')->with('success_msg','Product deleted successfully');
        }
        else {
            return redirect('adm/manage_product')->with('fail_msg','Something went wrong');
        }
    }
    else
    {
        return redirect('adm/manage_product')->with('fail_msg','Something went wrong, id not found');
    }
}

public function delete_category_submit(Request $request)
{
    $data = $request->all();
    $categories = new Categories;
    $pid = $data['pid'];

    $category_data = Categories::where('id','=',$pid);

    if(!empty($pid))
    {
        $result = $categories->delete_category($pid);
        if($result!==false)
        {
            $action = "Delete Category";
            $eventlog = $this->event_log($category_data,$action);
            return redirect('adm/manage_category')->with('success_msg','Category deleted successfully');
        }
        else {
            return redirect('adm/manage_category')->with('fail_msg','Something went wrong');
        }
    }
    else
    {
        return redirect('adm/manage_category')->with('fail_msg','Something went wrong, id not found');
    }
}

public function delete_banner_submit(Request $request)
{
    $data = $request->all();
    $home_banner = new Homebanner;

    $bid = $data['bid'];
    if(!empty($bid))
    {
        $result = $home_banner->delete_banner($bid);
        if($result!==false)
        {
            return redirect('adm/bannerlist')->with('success_msg','Banner deleted successfully');
        }
        else {
            return redirect('adm/bannerlist')->with('fail_msg','Something went wrong');
        }
    }
    else
    {
        return redirect('adm/bannerlist')->with('fail_msg','Something went wrong, id not found');
    }
}
// edit & delete product end here
public function add_product_submit(Request $request)
{
    $products = new Products;
    $this->validate($request, [
        'prod_title' => 'required',
        'prod_qty' => 'required',
        'category_id' => 'required',
        'prod_image' => 'required',
        ],['required' => 'This field is required']
    );

    try {

        DB::beginTransaction();

        $data = $request->all();
        // dd($data);

        if ($request->hasFile('prod_image'))
        {
            $image = $request->file('prod_image');
            $data['prod_image']  = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('images/products');
            $image->move($destinationPath, $data['prod_image']);
        }
        else
        {
            $image_file = '';
        }

        $new_data = $data;
        unset($data['category_id'],$data['prod_price']);
        $data['product_slug'] = str_replace(' ', '-',  strtolower($data['prod_title']));
       
       

        if($res = $products::create($data)){
            $res->category()->sync( $new_data['category_id'] );

           if(!empty( $new_data['prod_price'][0]) ){
            $res->weight()->createMany([
                [
                    'weight' => '500 Grams',
                    'price' => $new_data['prod_price'][0],
                    'status' => 1
                ],
                [
                    'weight' => '1 KG',
                    'price' => $new_data['prod_price'][1],
                    'status' => 1
                ],
                [
                    'weight' => '1 Piece',
                    'price' => $new_data['prod_price'][2],
                    'status' => 1
                ],
            ]);
           }

           if(!empty( $new_data['prod_price'][1]) ){
            $res->weight()->createMany([
                [
                    'weight' => '500 Grams',
                    'price' => $new_data['prod_price'][0],
                    'status' => 1
                ],
                [
                    'weight' => '1 KG',
                    'price' => $new_data['prod_price'][1],
                    'status' => 1
                ],
                [
                    'weight' => '1 Piece',
                    'price' => $new_data['prod_price'][2],
                    'status' => 1
                ],
               
            ]);
           }
           
           if(!empty( $new_data['prod_price'][2]) ){
            $res->weight()->createMany([
                [
                    'weight' => '500 Grams',
                    'price' => $new_data['prod_price'][0],
                    'status' => 1
                ],
                [
                    'weight' => '1 KG',
                    'price' => $new_data['prod_price'][1],
                    'status' => 1
                ],
                [
                    'weight' => '1 Piece',
                    'price' => $new_data['prod_price'][2],
                    'status' => 1
                ],
               
            ]);
           }
            

            $action = "Add Product";
            $eventlog = $this->event_log($data,$action);

            DB::commit();
            return redirect('adm/dashboard')->with('success_msg','Product added successfully');
        }
        else
        {
            DB::rollBack();
            return redirect('adm/dashboard')->with('fail_msg','Something went wrong');
        }
    } catch (\Exception $e) {
        // dd($e);
        DB::rollBack();
        return redirect('adm/dashboard')->with('fail_msg','Something went wrong');
    }
}

public function add_category_submit(Request $request)
{
    $categories = new Categories;
    $this->validate($request, [
        'title' => 'required',
        'parent_id' => 'required',
        'sequence' => 'required',
    ],['required' => 'This field is required']
);

$data = $request->all();
if($request->hasFile('category_banner')) {
    $image = $request->file('category_banner');
    $data['category_banner']  = time().'.'.$image->getClientOriginalExtension();
    $destinationPath = public_path('images/banner');
    $image->move($destinationPath, $data['category_banner']);
}
else {
    $cat_file = '';
}

if($request->hasFile('category_mobile_banner')) {
    $image1 = $request->file('category_mobile_banner');
    $data['category_mobile_banner']  = time().'.'.$image1->getClientOriginalExtension();
    $destinationPath = public_path('images/banner');
    $image1->move($destinationPath, $data['category_mobile_banner']);
}
else {
    $cat_file = '';
}

$data['category_slug'] = str_replace(' ', '-',  strtolower($data['title']));


if($res = $categories::create($data)){
    $action = "Add categories";
    $eventlog = $this->event_log($data,$action);
    return redirect('adm/dashboard')->with('success_msg','Category added successfully');
} else {
    return redirect('adm/dashboard')->with('fail_msg','Something went wrong');
}
}

public function manage_product()
{
    $checkpermission = $this->checkuserlogin();
    if($checkpermission){
        $products = new Products;
        $all_products = $products->all_products();
        return view('admin.pages.manage_products',['all_products'=>$all_products]);
    }else{
        return redirect('adm/')->with('message','Login to Access Panel. Please try again.');
    }
}

public function manage_category()
{
    $checkpermission = $this->checkuserlogin();
    if($checkpermission){
        $categories = new Categories;
        $all_categories = Categories::with('parent')->where('status','=',1)->get();
        return view('admin.pages.manage_category',['all_categories'=>$all_categories]);
    }else{
        return redirect('adm/')->with('message','Login to Access Panel. Please try again.');
    }

}

public function manage_customer()
{
    $checkpermission = $this->checkuserlogin();
    if($checkpermission){
        $users = new User;
        $all_user = $users::all();
        return view('admin.pages.manage_customer',['all_user'=>$all_user]);
    }else{
        return redirect('adm/')->with('message','Login to Access Panel. Please try again.');
    }

}

public function manage_order()
{
    $checkpermission = $this->checkuserlogin();
    if($checkpermission)
    {
        $order = new Order;
        $order_data = $order->with(['status','user'])->get();
        // print_r('<pre>');    
        // print_r($order_data);die;

        return view('admin.pages.manage_order',['all_order'=>$order_data]);
    }
    else
    {
        return redirect('adm/')->with('message','Login to Access Panel. Please try again.');
    }
}


function test()
{
    $order = new Order;
    $order_data = $order->with(['status','user'])->get();
    print_r('<pre>');    
    print_r($order_data);die;    
}

public function order_details($order_id)
{
    $checkpermission = $this->checkuserlogin();

    if($checkpermission)
    {
        if( isset($order_id) && $order_id != null)
        {
            $order = new Order;
            $status = $this->status::all();
            $order_data = $order->with(['status','user'])->where('id',$order_id)->first();
            $order_data->cart = unserialize($order_data->cart);
            
             
            return view('admin.pages.order_details',['order'=>$order_data, 'status' => $status ]);
        }
        else
        { 
            return redirect()->back();
        }
    }
    else
    {
        return redirect('adm/')->with('message','Login to Access Panel. Please try again.');
    }
}

public function update_order_status($id, Request $request){

    

    $str = $id;
    $parts = explode('_', $str);
    $order_status = $parts[0];
    $order_id = $parts[1];



    if($order_status==4){

        $transactions = Transaction::where('order_id', $order_id)->first();

       
      
       if($transactions==null || empty($transactions->razorpay_payment_id))
       {
            return redirect('adm/manage_order')->with('fail_msg','Something went wrong in Refund');
       }
        $razorpay_payment_id = $transactions->razorpay_payment_id;
        $user_id = $transactions->user_id;
        $order_id = $transactions->order_id;

        app('App\Http\Controllers\RazorpayController')->getRefund($razorpay_payment_id,$user_id,$order_id); 

    }



    $ord = new Order;

    $order = $ord::where('id',$order_id)->first();
    $order->order_status = $order_status;
    $res =  $order->save();




    $mask = 'daduss';
    $unicode=0;

    switch ($order_status)
    {
        case 1:
            $condition = 'confirmed';
        break;

        case 3:
            $condition = 'shipped';
        break;

        case 4:
         $condition = 'Refund';
        case 6:
            $condition = 'cancelled';
        break;

        case 5:
            $condition = 'delivered';
        break;

        default:
            $condition = 'pending';
        break;
    }


    if($res)
    {
      
        
        $msg_template = DB::table('sms_template')->where('template_title','=',$condition)->pluck('template_description');
        $message = "Dear " . $order->name ." ". $msg_template[0] . " Order Details - order id: ". $order->id;

        $sendmessagearray=array('mask'=>$mask,'mobile'=>$order->mobile,"message"=>preg_replace("|\&|","^*^*^",$message),'from'=>3,'unicode'=>$unicode);
        $sendmessagejson = json_encode($sendmessagearray);
        $sendmessagedata = 'sendmessage='.$sendmessagejson;
        $sendotpmessage = $this->sendsms($sendmessagejson,$sendmessagedata);
       
        app('App\Http\Controllers\CartController')->mail($order->user_id, $order->id, $msg_template[0]);

        $action = "Order Status Change";


        $data = array(
            'id' => $order_id,
            'name'=>$order->name, 
    );

       
        $eventlog = $this->event_log($data,$action);


        return redirect('adm/manage_order')->with('success_msg','Status Updated successfully');
    }
    else
    {
        return redirect('adm/manage_order')->with('fail_msg','A Something went wrong');
    }
}

public function sendsms($sendmessagejson,$sendmessagedata){
    $url = "http://webapp.ediffy.com/ediffyv2/sendmessage/index";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, !empty($sendmessagejson));
    curl_setopt($ch, CURLOPT_POSTFIELDS,  $sendmessagedata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $curl_scraped_page = curl_exec($ch);
    curl_close($ch);
    if($curl_scraped_page){
        return true;
    }else{
        return false;
    }
}

public function log_details(){
    $log_data = DB::table('user_log')->get();
    return view('admin.pages.log_details',[ 'log_data' => $log_data]);
}

public function checkuserlogin(){
    $session_data = session()->all();
    if(!isset($session_data['id']) || $session_data['id'] == ''){
        return false;
    }else{
        return true;
    }
}

public function showDashboard()
{
    $title = 'Dadus loyalty Program | Dashboard';
    return view('admin.dashboard.form',compact('title'));
}

public function addrewards(Request $request)
{
    $this->validate($request, [
        'mobile_number'=>'required|numeric',
        'first_name'=>'required',
        'last_name'=>'required',
        'dob'=>'required',
        'last_name'=>'required',
    ],
    [
        'mobile_number.required'=>'Please enter mobile number',
        'mobile_number.numeric' => 'Please enter valid mobile number',
        'first_name.required' => 'Please enter customer First Name',
        'last_name.required' => 'Please enter customer Last Name',

    ]);

    $user = new User();

    // $validator = request()->validate($rules,$message);
    $data=$request->all();
    $message = "";
    $findCustomer = $user::where('mobile','=',$data["mobile_number"])->first();
    if(!empty($findCustomer))
    {
        // dd($findCustomer);
        $updateRewardpoint = $findCustomer->increment('reward_points', $data["reward_points"]);
        // dd($updateRewardpoint);
        $message = "The reward point is successfull added to existing customer!";
    }
    else
    {
        // $customer=new Users();
        $user->mobile = $data["mobile_number"];
        $user->name = $data["first_name"] . ' ' . $data["last_name"];
        $user->reward_points = $data["reward_points"];
        $user->dob = date("Y-m-d" , strtotime($data["dob"]));
        // dd($customer);
        $user->save();
        // dd($customer);
        $message = "New customers has been added with the mobile number, reward point and username";
    }
    \Session::flash('success_msg',$message); //<--FLASH MESSAGE
    return redirect('adm/home');
}

public function customers()
{
    $title= 'Dadus loyalty Program | Dashboard';
    $customers = User::all();
    // dd($customers);
    return view('admin.dashboard.customers',compact('title','customers'));
}

public function get_details(Request $request){
    $title= 'Dadus loyalty Program | Dashboard';
    $customer = Customer::where('mobile_number','LIKE',$request->mobile_number.'%')->get(['first_name','last_name','mobile_number']);
    return $customer;
}

public function add_pincode_bulk(Request $request){
    $title= 'Dadus loyalty Program | Dashboard';
    return view('admin.pages.add_pincode_bulk',compact('title'));
}

public function add_pincode_bulk_submit(Request $request){
    try {
        DB::beginTransaction();
    if ($request->hasFile('pincode_file')) {

            $del_pincode = new Delivery_pincode;
            $path = $request->file('pincode_file')->getRealPath();//dd($path);
            $data = Excel::load($path, function ($reader) {})->get();
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                $res  = $del_pincode::updateOrCreate(
                        ['pincode' => (int)$value->pincode],
                        ['status' => (int)$value->status]
                    );
                    if($res == false)
                    {
                        DB::rollback();
                        $request->session()->put('errmsg', 'Something went wrong..');
                        return redirect('adm/add_pincode_bulk');
                    }
                }
                DB::commit();
                $request->session()->put('msg', 'Pincode Uploaded..');
                return redirect('adm/add_pincode_bulk');
            }
        }
        else
        {
                $request->session()->put('errmsg', 'Pincode CSV missing');
                return redirect('adm/add_pincode_bulk');
        }
    } catch (\Exception $e) {
         DB::rollback();
         $request->session()->put('errmsg', 'Something went wrong..');
         return redirect('adm/add_pincode_bulk');
    }
}

}
