<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Config;
use App\Models\Products;
use App\Models\Categories;
use Validator;
use App\Models\User;
use App\Customer;
use App\Models\Order;
// use Session;
use Illuminate\Support\Facades\Session;
use Redirect;


class MainController extends Controller
{
    function __construct()
    {
         $this->middleware('web');
    }


public function edit_category_submit(Request $request)
{

    $categories = new Categories;

    $this->validate($request, [
        'title' => 'required',
       ],['required' => 'This field is required']
);

$data = $request->all();
if ($request->hasFile('category_banner')) {
    $image = $request->file('category_banner');
    $data['category_banner']  = time().'.'.$image->getClientOriginalExtension();
    $destinationPath = public_path('images/banner');
    $image->move($destinationPath, $data['category_banner']);
}
else {
    $data['category_banner'] = $data['category_banner_old'];
}

if ($request->hasFile('category_mobile_banner')) {
    $image1 = $request->file('category_mobile_banner');
    $data['category_mobile_banner']  = time().'.'.$image1->getClientOriginalExtension();
    $destinationPath1 = public_path('images/banner');
    $image1->move($destinationPath1, $data['category_mobile_banner']);
}
else {
    $data['category_mobile_banner'] = $data['category_mobile_banner_old'];
}

unset($data['_token'],$data['category_banner_old']);
unset($data['_token'],$data['category_mobile_banner_old']);

$data['category_slug'] = str_replace(' ', '-',  strtolower($data['title']));

$res = $categories::where('id', $data['id'])->update($data);
if($res!==false)
return redirect('adm/manage_category')->with('success_msg','Category Updated successfully');
else
return redirect('adm/manage_category')->with('fail_msg','Something went wrong');
}


}
