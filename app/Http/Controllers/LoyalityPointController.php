<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\User;
use DB;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\LoyaltyReadmePointRecord;

class LoyalityPointController extends Controller
{
    public function OLD_INSERT_CUSTOMER_REGISTRATION_ACTION()
    {

        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/INSERT_CUSTOMER_REGISTRATION_ACTION';
        $data = [
            "objClass" => [
                [
                    "store_code" => "001",
                    "registration_date" => "2017-01-05 16:10",
                    "customer_name" => "Sujit Singh",
                    "customer_mobile" => "9555420197",
                    "customer_email" => "sujit@mobiquest.com",
                    "customer_city" => "New Delhi",
                    "customer_pin" => "110092",
                    "customer_dob" => "1990-09-13",
                    "customer_doa" => "",
                    "customer_gender" => "Male",
                ],
                [
                    "store_code" => "001",
                    "registration_date" => "2017-01-05 16:10",
                    "customer_name" => "Sujit Singh",
                    "customer_mobile" => "9560629264",
                    "customer_email" => "sujit@mobiquest.com",
                    "customer_city" => "New Delhi",
                    "customer_pin" => "110092",
                    "customer_dob" => "1990-09-13",
                    "customer_doa" => "",
                    "customer_gender" => "Male",
                ]

            ]

        ];
        $result = $this->curl_post($endpoint, $data);

    }

    public function OLD_GET_CUSTOMER_TRANS_INFO()
    {

        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/GET_CUSTOMER_TRANS_INFO';
        $data = [
            "objClass" => [
                "customer_mobile" => "8082066391"
            ]
        ];
        $result = $this->curl_post($endpoint, $data);
        $customerdata = json_decode($result, true);
        $customerdetail = $customerdata['GET_CUSTOMER_TRANS_INFOResult']['output']['response'];
        $customerdetails = json_decode($customerdetail, true);
        $personaldetails = $customerdetails['CUSTOMER_DETAILS'][0];
        $pointhistory = $customerdetails['EARN_BURN_HISTORY'];
        $coupoHistory = $customerdetails['COUPON_HISTORY'];

    }

    public function INSERT_ITEM_DATA()
    {

        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/INSERT_ITEM_DATA';
        $data = [
            "objClass" => [
                [
                 "item_code" => "1",
                 "item_name" => "Test-01",
                 "item_description" => "",
                 "item_category" => "",
                 "item_category_code" => "",
                 "item_group" => "",
                 "item_group_code" => "",
                 "item_product_code" => "",
                 "item_product" => "",
                 "item_sizecode" => "",
                 "item_colorcode" => "",
                 "item_price" => "",
                 "item_Points" => "",
                 "item_brand" => "",
                 "item_brand_code" => ""
                ]
            ]
        ];
        $result = $this->curl_post($endpoint, $data);
        $b=json_decode($result,true);
        $status=$b['INSERT_ITEM_DATAResult']['Success'];
        if($status==true){
            $uniqueno=$b['INSERT_ITEM_DATAResult']['output']['uniqueno'];
            $duplicateentry=$b['INSERT_ITEM_DATAResult']['output']['duplicate_item_code'];
        }


    }

    public function GET_PROMOTIONAL_COUPON_VALIDATION_INFO()
    {
        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/GET_PROMOTIONAL_COUPON_VALIDATION_INFO';
        $data = [
            "objClass" => [
                "coupon_code" => "ABCG12D",
                "store_code" => "001",
                "strRefrenceBillNo" => "001",
                "type" => "0",
            ]
        ];
        $result = $this->curl_post($endpoint, $data);
    }

    public function REDEEM_VOUCHER_EXHAUSTION_ACTION()
    {
        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/REDEEM_VOUCHER_EXHAUSTION_ACTION';
        $data = [
            "objClass" => [
                "voucher_code" => "ACDD123DA",
                "store_code" => "HO-123",
                "ref_bill_no" => "Test-0123",
                "passcode" => "121323",
            ]
        ];
        $result = $this->curl_post($endpoint, $data);
    }

    public function GET_VOUCHER_VALIDATION_INFO()
    {
        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/GET_VOUCHER_VALIDATION_INFO';
        $data = [
            "objClass" => [
                "voucher_code" => "ACDD123DA",
                "store_code" => "HO-123",
            ]
        ];
        $result = $this->curl_post($endpoint, $data);
    }

    public function OLD_REDEEM_LOYALTY_POINTS_ACTION()
    {
        $output='{"REDEEM_LOYALTY_POINTS_ACTIONResult":{"Success":true,"message":"you have redeemed","methodname":"Points_Redemption","output":{"balance_points":"4100","points_value":"900","redeem_points":"900","referenceNo":"6A1DB26F-32EB-4DC9-8E68-53A1EED8E154-20190829151901063"}}}';
        $a=json_decode($output,true);
        dd($a);
        $output=$a['REDEEM_LOYALTY_POINTS_ACTIONResult']['output'];
        $apiOutput=[
                  "balance_points" => "4100",
                  "points_value" => "900",
                  "redeem_points" => "900",
                  "referenceNo" => "6A1DB26F-32EB-4DC9-8E68-53A1EED8E154-20190829151901063"
                ];

        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/REDEEM_LOYALTY_POINTS_ACTION';
        $data = [
            "objClass" => [
                "customer_mobile" => 8082066391,
                "customer_points" => 900,
                "passcode" => 974564,
                "ref_bill_no" => "Test-01",
                "store_code" => "1111100000",
            ]
        ];
        $result = $this->curl_post($endpoint, $data);
        dd($result);
    }

    public function OLD_GET_POINTS_VALIDATION_INFO()
    {

//        $result=$this->GET_POINTS_VALIDATION_INFO(Auth::user()->mobile,$readMePoint);
//        $loyala='{"GET_POINTS_VALIDATION_INFOResult":{"Success":true,"message":"coupon has been sent on customer mobile 918082066391.","methodname":"Points_Validation","output":{"points":"900","points_value":"900"}}}';
//        $outputresult=json_decode($loyala,true);
//
//        if($outputresult['GET_POINTS_VALIDATION_INFOResult']['Success']==true){
//
//        }
        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/GET_POINTS_VALIDATION_INFO';
        $data = [
            "objClass" => [
                "customer_mobile" => "9769828965",
                "customer_points" => "10",
                "store_code" => "HO-01",
            ]
        ];
        $result = $this->curl_post($endpoint, $data);
    }

    public function OLD_REVERSE_POINTS()
    {

        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/REVERSE_POINTS';
        $data = [
            "objClass" => [
                "store_code" => "0000011111",
                "redeem_points" => "900",
                "redeem_date" => "2019-08-29",
                "customer_mobile" => "8082066391",
                "ref_bill_no" => "TEST-01",
            ]
        ];
        $result = $this->curl_post($endpoint, $data);


    }

    public function OLD_INSERT_BILLING_DATA_ACTION($orderID=667)
    {
        $order=Order::find($orderID);
        $user=User::find($order->user_id);
        $cartItem=unserialize($order->cart);

        $transaction=Transaction::where('order_id',$orderID)->first();
        $finaloutput=[];
        foreach($cartItem as $key => $value){
            $output['item_serial_no']=$key;
            $output['item_quantity']=$value['qty'];
            $output['item_code']=$value['weight']['products_id'];
            $output['item_rate']=$value['weight']['price'];
            $output['item_net_amount']=$value['weight']['price'];
            $output['item_gross_amount']=$value['weight']['price'];
            $output['item_name']=$value['data']['prod_title'];
            $output['item_tax']=0;
            $output['item_discount_per']='';
            $output['item_service_tax']='';
            $output['item_barcode']='';
            $output['item_discount']='';
            $output['item_barcode']='';
            $output['item_brand_code']='';
            $output['item_brand_name']='';
            $output['item_category_code']='';
            $output['item_category_name']='';
            $output['item_group']='';
            $output['item_status']='New';
            $output['item_group_name']='';
            $output['item_color_code']='';
            $output['item_color_name']='';
            $output['item_size_code']='';
            $output['item_size_name']='';
            $output['item_sub_category_code']='';
            $output['item_sub_category_name']='';
            $output['item_department_code']='';
            $output['item_department_name']='';
            $output['item_remarks1']='';
            $output['item_remarks2']='';
            $output['item_remarks3']='';
            $output['item_remarks4']='';
            $output['item_remarks5']='';
            $finaloutput[]=$output;
        }

        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/INSERT_BILLING_DATA_ACTION';
        $data = [
            "objClass" => [
                [
                    "store_code" => "ONLINE",
                    "bill_date" => date('Y-m-d',strtotime($order->created_at)),
                    "bill_no" => $order->id,
                    "bill_grand_total" =>  $order->grand_total,
                    "bill_discount" =>  $order->discount,
                    "bill_tax" =>  $order->tax,
                    "bill_transaction_type" => "Home",
                    "bill_gross_amount" => $order->grand_total,
                    "bill_net_amount" => $order->grand_total,
                    "customer_fname" => $user->name,
                    "customer_mobile" => $user->mobile,
                    "customer_email" => $user->email,
                    "voucher_code" => "",
                    "voucher_value" => "",
                    "voucher_type" => "",
                    "bill_tender_type" => "Online",
                    "customer_dob" => "",
                    "customer_doa" => "",
                    "bill_time" => date('Y-m-d H:i:s',strtotime($order->created_at)),
                    "bill_type" => "Home",
                    "bill_status" => "NEW",
                    "bill_transcation_no" => $transaction->id,
                    "bill_discount_per" => "1",
                    "bill_service_tax" => "",
                    "bill_cancel_date" => "",
                    "bill_cancel_time" => "",
                    "bill_cancel_reason" => "",
                    "bill_cancel_amount" => "",
                    "bill_cancel_against" => "",
                    "bill_modify" => "",
                    "bill_modify_reason" => "",
                    "bill_modify_datetime" => "",
                    "bill_remarks1" => "",
                    "bill_remarks2" => "",
                    "bill_remarks3" => "",
                    "bill_remarks4" => "",
                    "bill_remarks5" => "",
                    "customer_code" => "1",
                    "customer_lname" => "",
                    "customer_gender" => "",
                    "customer_city" => "",
                    "customer_area" => "",
                    "customer_address" => "",
                    "customer_state" => "",
                    "customer_remarks1" => "",
                    "customer_remarks2" => "",
                    "customer_remarks3" => "",
                    "customer_remarks4" => "",
                    "customer_remarks5" => "",
                    "ext_param1" => "",
                    "ext_param2" => "",
                    "ext_param3" => "",
                    "ext_param4" => "",
                    "ext_param5" => "",
                    "bill_round_off_amount" => "",
                    "output" => $finaloutput
                ]

            ]
        ];
        $result = $this->curl_post($endpoint, $data);


    }


    public function getreadmepasscode(Request $request)
    {
        $data = $request->all();
        $readme = (int)$data['readme'];
        $mobile = (int)$data['mobile'];
        $totalamount = (int)$data['totalamount'];
        if ($readme && $mobile && $totalamount && !empty($readme) && !empty($mobile) && !empty($totalamount)) {
            if ($readme > 0) {
                if ($readme > $totalamount) {
                    $readMePoint = $totalamount;

                } elseif ($readme == $totalamount) {
                    $readMePoint = $totalamount;

                } elseif ($readme < $totalamount) {
                    $readMePoint = $readme;

                }
                if ($readMePoint) {
                    $result = $this->GET_POINTS_VALIDATION_INFO($mobile, $readMePoint);
                    $outputresult = json_decode($result, true);
                    if ($outputresult['GET_POINTS_VALIDATION_INFOResult']['Success'] == true) {
                        if ((int)$outputresult['GET_POINTS_VALIDATION_INFOResult']['output']['points_value'] == $readMePoint) {
                            return 1;

                        } else {
                            return 2;
                        }
                    } else {
                        return 2;
                    }
                } else {
                    return 2;
                }
            } else {
                return 2;
            }
        } else {
            return 2;
        }

    }

    public function verifypasscode(Request $request){

        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/REDEEM_LOYALTY_POINTS_ACTION';
        $data = [
            "objClass" => [
                "customer_mobile" => 8082066391,
                "customer_points" => 900,
                "passcode" => 974564,
                "ref_bill_no" => "Test-01",
                "store_code" => "ONLINE",
            ]
        ];
        $result = $this->curl_post($endpoint, $data);

    }


}
