<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\View;
use App\Models\Categories;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\LoyaltyReadmePointRecord;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {

        $categories = new Categories;
        $category_tree = [];
        $category_tree = $categories->get_categories_tree();

        /** Tracking UTM */
        $utm_source = Input::get('utm_source');
        $utm_campaign = Input::get('utm_campaign');
        $utm_medium = Input::get('utm_medium');
        // dd($utm_medium);

        if (!Session::has('utm_source')) {
            Session::put('utm_source', $utm_source);
        }

        if (!Session::has('utm_medium')) {
            Session::put('utm_medium', $utm_medium);
        }

        if (!Session::has('utm_campaign')) {
            Session::put('utm_campaign', $utm_campaign);
        }


        View::share('category_tree', $category_tree);

    }
    public function curl_post($endpoint, $data)
    {
        try {
            $payload = json_encode($data);
            $ch = curl_init($endpoint);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'userid:mob_usr',
                    'pwd:@pa$$w0rd')
            );
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function INSERT_CUSTOMER_REGISTRATION_ACTION($user,$customer_address){
        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/INSERT_CUSTOMER_REGISTRATION_ACTION';
        $data = [
            "objClass" => [
                [
                    "store_code" => "ONLINE",
                    "registration_date" => date('Y-m-d H:i'),
                    "customer_name" => $user->name,
                    "customer_mobile" => $user->mobile,
                    "customer_email" => $user->email,
                    "customer_city" => $customer_address->city,
                    "customer_pin" => $customer_address->pincode,
                    "customer_dob" => date('Y-m-d H:i',strtotime($user->dob)),
                    "customer_doa" => date('Y-m-d H:i',strtotime($user->dob)),
                    "customer_gender" => '',
                ]

            ]

        ];
        $result = $this->curl_post($endpoint, $data);
        return $result;
    }
    public function GET_CUSTOMER_TRANS_INFO($mobile)
    {

        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/GET_CUSTOMER_TRANS_INFO';
        $data = [
            "objClass" => [
                "customer_mobile" => $mobile
            ]
        ];
        $result = $this->curl_post($endpoint, $data);
        $customerdata = json_decode($result, true);
        $customerdetail = $customerdata['GET_CUSTOMER_TRANS_INFOResult']['output']['response'];
        $customerdetails = json_decode($customerdetail, true);
        $personaldetails = $customerdetails['CUSTOMER_DETAILS'][0];
       // $pointhistory = $customerdetails['EARN_BURN_HISTORY'];
       // $coupoHistory = $customerdetails['COUPON_HISTORY'];
        return($personaldetails);


    }
    public function REDEEM_LOYALTY_POINTS_ACTION($mobile,$point,$passcode,$billno)
    {

        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/REDEEM_LOYALTY_POINTS_ACTION';
        $data = [
            "objClass" => [
                "customer_mobile" => $mobile,
                "customer_points" => $point,
                "passcode" => $passcode,
                "ref_bill_no" => $billno,
                "store_code" => "ONLINE",
            ]
        ];
        $result = $this->curl_post($endpoint, $data);
        return $result;
    }

    public function GET_POINTS_VALIDATION_INFO($mobile,$readmepoint)
    {

        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/GET_POINTS_VALIDATION_INFO';
        $data = [
            "objClass" => [
                "customer_mobile" => $mobile,
                "customer_points" => $readmepoint,
                "store_code" => "ONLINE",
            ]
        ];
        $result = $this->curl_post($endpoint, $data);
        return $result;
    }

    public function REVERSE_POINTS($orderId)
    {
        $loyalityPoint=LoyaltyReadmePointRecord::where('order_id',$orderId)->first();
        if($loyalityPoint){
            $user=User::find($loyalityPoint->user_id);
            $endpoint = 'http://mqst.mloyalpos.com/Service.svc/REVERSE_POINTS';
            $data = [
                "objClass" => [
                    "store_code" => "ONLINE",
                    "redeem_points" => $loyalityPoint->redeem_points,
                    "redeem_date" => date('Y-m-d',strtotime($loyalityPoint->created_at)),
                    "customer_mobile" => $user->mobile,
                    "ref_bill_no" => $orderId,
                ]
            ];
            $result = $this->curl_post($endpoint, $data);
        }
        else{
           $result=false;
        }
        return $result;

    }

    public function INSERT_BILLING_DATA_ACTION($orderID)
    {
        $order=Order::find($orderID);
        $user=User::find($order->user_id);
        $cartItem=unserialize($order->cart);
        $transaction=Transaction::where('order_id',$orderID)->first();
        $finaloutput=[];
        foreach($cartItem as $key => $value){
            $output['item_serial_no']=$key;
            $output['item_quantity']=$value['qty'];
            $output['item_code']=$value['weight']['products_id'];
            $output['item_rate']=$value['weight']['price'];
            $output['item_net_amount']=$value['weight']['price'];
            $output['item_gross_amount']=$value['weight']['price'];
            $output['item_name']=$value['data']['prod_title'];
            $output['item_tax']=0;
            $output['item_discount_per']='';
            $output['item_service_tax']='';
            $output['item_barcode']='';
            $output['item_discount']='';
            $output['item_barcode']='';
            $output['item_brand_code']='';
            $output['item_brand_name']='';
            $output['item_category_code']='';
            $output['item_category_name']='';
            $output['item_group']='';
            $output['item_status']='New';
            $output['item_group_name']='';
            $output['item_color_code']='';
            $output['item_color_name']='';
            $output['item_size_code']='';
            $output['item_size_name']='';
            $output['item_sub_category_code']='';
            $output['item_sub_category_name']='';
            $output['item_department_code']='';
            $output['item_department_name']='';
            $output['item_remarks1']='';
            $output['item_remarks2']='';
            $output['item_remarks3']='';
            $output['item_remarks4']='';
            $output['item_remarks5']='';
            $finaloutput[]=$output;
        }
        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/INSERT_BILLING_DATA_ACTION';
        $data = [
            "objClass" => [
                [
                    "store_code" => "ONLINE",
                    "bill_date" => date('Y-m-d',strtotime($order->created_at)),
                    "bill_no" => $order->id,
                    "bill_grand_total" =>  $order->grand_total,
                    "bill_discount" =>  $order->discount,
                    "bill_tax" =>  $order->tax,
                    "bill_transaction_type" => "Home",
                    "bill_gross_amount" => $order->grand_total,
                    "bill_net_amount" => $order->grand_total,
                    "customer_fname" => $user->name,
                    "customer_mobile" => $user->mobile,
                    "customer_email" => $user->email,
                    "voucher_code" => "",
                    "voucher_value" => "",
                    "voucher_type" => "",
                    "bill_tender_type" => "Online",
                    "customer_dob" => "",
                    "customer_doa" => "",
                    "bill_time" => date('Y-m-d H:i:s',strtotime($order->created_at)),
                    "bill_type" => "Home",
                    "bill_status" => "NEW",
                    "bill_transcation_no" => $transaction->id,
                    "bill_discount_per" => "1",
                    "bill_service_tax" => "",
                    "bill_cancel_date" => "",
                    "bill_cancel_time" => "",
                    "bill_cancel_reason" => "",
                    "bill_cancel_amount" => "",
                    "bill_cancel_against" => "",
                    "bill_modify" => "",
                    "bill_modify_reason" => "",
                    "bill_modify_datetime" => "",
                    "bill_remarks1" => "",
                    "bill_remarks2" => "",
                    "bill_remarks3" => "",
                    "bill_remarks4" => "",
                    "bill_remarks5" => "",
                    "customer_code" => "1",
                    "customer_lname" => "",
                    "customer_gender" => "",
                    "customer_city" => "",
                    "customer_area" => "",
                    "customer_address" => "",
                    "customer_state" => "",
                    "customer_remarks1" => "",
                    "customer_remarks2" => "",
                    "customer_remarks3" => "",
                    "customer_remarks4" => "",
                    "customer_remarks5" => "",
                    "ext_param1" => "",
                    "ext_param2" => "",
                    "ext_param3" => "",
                    "ext_param4" => "",
                    "ext_param5" => "",
                    "bill_round_off_amount" => "",
                    "output" => $finaloutput
                ]

            ]
        ];
        $result = $this->curl_post($endpoint, $data);
        return $result;
    }



}
