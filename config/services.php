<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
    'client_id' => '298721780664176',
    'client_secret' => '9d3be1e7731ab62786789ab2c5a869e3',
    'redirect' => 'http://dadus.in/callback',
    ],

    'google' => [
    'client_id' => '875778585841-969rn56fkurjisradujnfouapggod6dq.apps.googleusercontent.com',
    'client_secret' => 'IWdaC9k_l71E3jPr07NL-JAL',
    'redirect' => 'http://dadus.in/googlecallback',
    ],
];
