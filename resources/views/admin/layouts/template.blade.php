<!DOCTYPE html>
<html lang="en">

<head>

  <title>Dadus loyalty program </title>
  <meta name="description" content=""/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

  <!-- basic styles -->
  <link href="{{env('APP_URL')}}css/AdminLTE.min.css" rel="stylesheet"/>

  <link href="{{env('APP_URL')}}skin/css/A-bootstrap-min.css" rel="stylesheet"/>


  <!--[if IE 7]>
  <link rel="stylesheet" href="{{env('APP_URL')}}skin/css/font-awesome-ie7.min.css" />
  <![endif]-->

  <!-- page specific plugin styles TODO -->

  <link href="{{env('APP_URL')}}css/A-dashboards.css" rel="stylesheet"/>

  <!-- fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300"/>

  <!-- font awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

  <link rel="stylesheet" href="{{env('APP_URL')}}skin/css/A-ace-fonts.css"/>
  <!-- ace styles -->
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
  <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css"/>
  <!-- Favicons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{env('APP_URL')}}assets/ico/xapple-touch-icon-144-precomposed.png.pagespeed.ic.w6FJYEbIZQ.webp">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{env('APP_URL')}}assets/ico/xapple-touch-icon-114-precomposed.png.pagespeed.ic.G9ySfrK3xR.webp">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{env('APP_URL')}}assets/ico/xapple-touch-icon-72-precomposed.png.pagespeed.ic._6E48hbHEx.webp">
  <link rel="apple-touch-icon-precomposed" href="{{env('APP_URL')}}assets/ico/xapple-touch-icon-57-precomposed.png.pagespeed.ic.uxpe88347n.webp">
  <link rel="shortcut icon" href="{{env('APP_URL')}}assets/ico/xfavicon.png.pagespeed.ic.CTrlm49068.webp">
  <!--[if lte IE 8]>
  <link rel="stylesheet" href="{{env('APP_URL')}}skin/css/ace-ie.min.css" />
  <![endif]-->

  <!-- inline styles related to this page TODO -->

  <!-- ace settings handler -->
  <script src="{{env('APP_URL')}}skin/js/ace-extra.min.js.pagespeed.jm.IYI7vEXTPz.js"></script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="{{env('APP_URL')}}skin/js/html5shiv.js"></script>
  <script src="{{env('APP_URL')}}skin/js/respond.min.js"></script>
  <![endif]-->
<meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="skin-1">

@include('admin.includes.nav')


<div class="main-container" id="main-container">
  <div class="main-container-inner">

    <a class="menu-toggler" id="menu-toggler" href="#"><span class="menu-text"></span></a>

    @include('admin.includes.sidebar')

    <div class="main-content">

      <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">try{ace.settings.check('breadcrumbs','fixed')}catch(e){}</script>

        <ul class="breadcrumb">
          <li><i class="icon-dashboard home-icon"></i><a href="{{ env('APP_URL') }}home">Dashboard</a></li>    </ul><!--.breadcrumb-->


      </div>

      <div class="page-content">
        @include('admin.includes.error')
        @yield('content')
      </div><!--/.page-content-->

    </div><!--/.main-content-->


  </div><!--/.main-container-inner-->
</div><!--/.main-container-->



<!-- basic scripts -->
<script type="text/javascript">
  window.jQuery || document.write("<script src='{{env("APP_URL")}}skin/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>

<!--[if IE]>
<script type="text/javascript">
  window.jQuery || document.write("<script src='{{env("APP_URL")}}skin/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

<script>
    var site_url='{{ env("APP_URL") }}';
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
<script src="{{env('APP_URL')}}skin/js/bootstrap.min.js"></script>
<script src="{{env('APP_URL')}}skin/js/ace-extra.min.js.pagespeed.jm.IYI7vEXTPz.js"></script>
<script src="{{env('APP_URL')}}skin/js/ace.min.js"></script>
<script src="{{env('APP_URL')}}js/dashboard.js"></script>
@yield('scripts')
</body>
</html>