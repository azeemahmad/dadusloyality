<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ $title }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{URL::asset('panel-css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{URL::asset('panel-dist/css/dashboard.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{URL::asset('panel-plugins/iCheck/square/blue.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page" style="overflow: hidden;background: url({{ env('APP_URL') }}/images/background.png);background-size: cover;">
<div class="login-box">
  <div class="login-logo">
      <a href="{{ env('APP_URL') }}"><img src="{{URL::asset('images/logo.png')}}" height="200" width="200" /></a>
  </div>
    <div class="text-center">
        <h2>Loyalty Program</h2>
    </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><span style="font-style: italic;font-weight: bold;">Hi!</span> Sign in to your account.</p>
    @if ($errors->first())
      <div class="alert alert-danger">
        {{ $errors->first() }}
      </div>
    @endif
    {!! Form::open(array( 'method' => 'post','autocompolete'=>'false' , 'autofill'=>'false')) !!}
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
 <!--a href="{{ getenv('APP_URL') }}iad/forgotpassword">I forgot my password</a --><br>
    <!--a href="{{ getenv('APP_URL') }}iad/register" class="text-center">Register a new membership</a-->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="{{URL::asset('panel-plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{URL::asset('panel-js/bootstrap.min.js')}}"></script>
<!-- iCheck -->
<script src="{{URL::asset('panel-plugins/iCheck/icheck.min.js')}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
