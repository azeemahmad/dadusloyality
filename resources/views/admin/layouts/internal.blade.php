<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Dadus Admin</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('admin/css/bootstrap.min.css') }}?v=20180316">
    <link rel="stylesheet" href="{{ URL::asset('admin/css/waves.min.css') }}?v=20180316">
    <link rel="stylesheet" href="{{ URL::asset('admin/css/animate.min.css') }}?v=20180316">
    <link rel="stylesheet" href="{{ URL::asset('admin/css/style.css') }}?v=20180316">
    <link rel="stylesheet" href="{{ URL::asset('admin/css/themes/all-themes.css') }}?v=20180316">
    @yield('pagestyle')
</head>
<body class="theme-blue">
    @include('admin.includes.header')
    @include('admin.includes.sidebar')
    @yield('content')
    <script type="text/javascript" src="{{ URL::asset('admin/js/jquery.min.js') }}?v=20180316"></script>
    <script type="text/javascript" src="{{ URL::asset('admin/js/bootstrap.min.js') }}?v=20180316"></script>
    <script type="text/javascript" src="{{ URL::asset('admin/js/waves.min.js') }}?v=20180316"></script>
    <script type="text/javascript" src="{{ URL::asset('admin/js/admin.js') }}?v=20180316"></script>
    @yield('pagescript')
</body>
</html>
