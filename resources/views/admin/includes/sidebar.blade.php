<section>
<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info" style="height:auto;background:transparent;text-align: center;">
        <a href="{{url('adm/dashboard')}}"><img src="{{ URL::asset('images/rsz_divider.png') }}" style="max-width: 70%;margin:0 auto;"></a>


    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            @if(session('access_level') == 1 || session('access_level') == 2 )
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="{{url('adm/dashboard')}}">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">widgets</i>
                    <span>Products</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('adm/add_product')}}">Add Product</a>
                    </li>
                    <li>
                        <a href="{{url('adm/manage_product')}}">Manage Product</a>
                    </li>
                </ul>
            </li>
             <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">widgets</i>
                    <span>Category</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('adm/add_category')}}">Add Category</a>
                    </li>
                    <li>
                        <a href="{{url('adm/manage_category')}}">Manage Category</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">widgets</i>
                    <span>Customer Details</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('adm/manage_customer')}}">Manage Customer</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">widgets</i>
                    <span>Orders</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('adm/manage_order')}}">Manage Order</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">widgets</i>
                    <span>Delivery Charges</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('adm/delivery_charges')}}">Manage Delivery Charges</a>
                    </li>


                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">widgets</i>
                    <span>Delivery Pincode</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('adm/add_pincode')}}">Add Pincode</a>
                    </li>

                     <li>
                        <a href="{{url('adm/delivery_pincode')}}">Manage Delivery Pincodes</a>
                    </li>
                     <li>
                        <a href="{{url('adm/add_pincode_bulk')}}">Add Pincodes Using CSV</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">widgets</i>
                    <span>Mobile Number</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('adm/add_mobilenumber')}}">Add Mobile Number</a>
                    </li>

                     <li>
                        <a href="{{url('adm/mobile_number')}}">Manage Mobile Numbers</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">widgets</i>
                    <span>SMS Template</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('adm/manage_sms_template')}}">Manage SMS Template</a>
                    </li>
                </ul>
            </li>
            @endif
            <li ">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">widgets</i>
                    <span>Loyalty Program</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('adm/home')}}">Add Rewards</a>
                    </li>
                    <li>
                        <a href="{{url('adm/customers')}}">Manage Customer</a>
                    </li>
                </ul>
            </li>
            @if(session('access_level') == 1)
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">widgets</i>
                    <span>Log Details</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('adm/log_details')}}">User Logs</a>
                    </li>
                </ul>
            </li>
            @endif
        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2018 - 2019 <a href="javascript:void(0);">First Economy Pvt Ltd</a>.
        </div>
        <div class="version">
            <b>Version: </b> 1.0.5
        </div>
    </div>
    <!-- #Footer -->
</aside>
<!-- #END# Left Sidebar -->
</section>
