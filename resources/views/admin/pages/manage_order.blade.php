@extends('admin.layouts.internal')
@section('pagestyle')
  <!-- JQuery DataTable Css -->
   <link href="{{ url('admin/js/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
@endsection
@section('content')

 

  <section class="content">
      <div class="container-fluid">
                <!-- Exportable Table -->
      <div class="row clearfix">
                @if (session('success_msg'))
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="alert alert-success">
                      {{ session('success_msg') }}
                  </div>
                </div>
                @endif
                @if (session('fail_msg'))
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="alert alert-warning">
                      {{ session('fail_msg') }}
                  </div>
                </div>
                @endif
            </div>
          
          <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="card">
                      <div class="header">
                          <h2>
                              Manage Orders
                          </h2>
                          <ul class="header-dropdown m-r--5">
                              <li class="dropdown">
                                  <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                      <i class="material-icons">more_vert</i>
                                  </a>
                                  <ul class="dropdown-menu pull-right">
                                      <li><a href="javascript:void(0);">Action</a></li>
                                      <li><a href="javascript:void(0);">Another action</a></li>
                                      <li><a href="javascript:void(0);">Something else here</a></li>
                                  </ul>
                              </li>
                          </ul>
                      </div>
                      <div class="body">
                          <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover dataTable product-exportable">
                                  <thead>
                                      <tr>
                                          <th>Order Id</th>
                                          <th>Name</th>
                                          <th>Mobile Number</th>
                                          <th>Order Status</th>
                                          <th>View/Modify Order</th>
                                          <th>Payment Status</th>
                                          <th>Order Total</th>
                                          <th>State</th>
                                          <th>City</th>
                                          <th>Pincode</th>
                                          <th>Order On</th>
                                          <th>Email Id</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                  <?php
                                  // var_dump($all_order);exit;
                                  ?>
                                    @foreach ($all_order as $okey => $ovalue)

                                    <?php
                                      $order_status = App\Models\Order_Status_Progress::where('order_id',$ovalue['id'])->where('status_id','7')->first();
                                    ?>
                                        {{-- {{ dd($ovalue) }} --}}
                                    <tr>
                                      <th>{{ $ovalue->id }}</th>
                                      <th>{{ $ovalue->name }}</th>
                                      <th>{{ $ovalue->mobile }}</th>
                                      <th>{{ $ovalue->status->title }}</th>
                                      <th>
                                          <a href="{{ url('adm/order_details') }}/{{ $ovalue->id }}">View/Modify Order</a>
                                     </th>
                                     <th>@if($order_status['status_id'] == 7){{'Payment Refunded'}}
                                        @elseif($ovalue->payment_status == 0){{'Payment Pending'}}
                                        @else {{'Payment Completed'}}
                                        @endif
                                      </th>
                                      <th>{{ $ovalue->grand_total }}</th>
                                      <th>{{ $ovalue->state }}</th>
                                      <th>{{ $ovalue->city }}</th>
                                      <th>{{ $ovalue->pincode }}</th>
                                      <th>{{ date( 'd-m-Y h:i:s A' ,  strtotime($ovalue->created_at)) }}</th>
                                      <th>{{ $ovalue->user->email }}</th>
                                      
                                </tr>
                                    @endforeach
                                  </tbody>
                                  <tfoot>
                                      <tr>
                                          <th>Order Id</th>
                                          <th>Name</th>
                                          <th>Mobile Number</th>
                                          <th>Order Status</th>
                                          <th>View/Modify Order</th>
                                          <th>Payment Status</th>
                                          <th>Order Total</th>
                                          <th>State</th>
                                          <th>City</th>
                                          <th>Pincode</th>
                                          <th>Order On</th>
                                          <th>Email Id</th>
                                      </tr>
                                  </tfoot>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- #END# Exportable Table -->
      </div>
  </section>
@endsection
@section('pagescript')
  <!-- Jquery DataTable Plugin Js -->
     <script src="{{ url('admin/js/jquery-datatable/jquery.dataTables.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
     <script type="text/javascript">
       //Exportable table
      $('.product-exportable').DataTable({
          order : [ 0, 'desc' ],
          dom: 'Bfrtip',
          responsive: true,
          buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
          ]
      });
     </script>
@endsection
