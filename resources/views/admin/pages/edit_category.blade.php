@extends('admin.layouts.internal')
@section('content')
  <section class="content">
         <div class="container-fluid">
             <div class="block-header">
                 <h2>Edit Category</h2>
             </div>

             <!-- Vertical Layout -->
             <div class="row clearfix">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="card">
                         <div class="header">
                             <h2>
                                 Category Details
                             </h2>
                             <ul class="header-dropdown m-r--5">
                                 <li class="dropdown">
                                     <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                         <i class="material-icons">more_vert</i>
                                     </a>
                                     <ul class="dropdown-menu pull-right">
                                         <li><a href="javascript:void(0);">Action</a></li>
                                         <li><a href="javascript:void(0);">Another action</a></li>
                                         <li><a href="javascript:void(0);">Something else here</a></li>
                                     </ul>
                                 </li>
                             </ul>
                         </div>
                         <div class="body">
                             <form method="post" action="{{ url('adm/edit_category_submit') }}" id="edit_category" autocomplete="off" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="id" value="{{ isset($category->id) ? $category->id : null }}" >
                                <label for="category_name">Category Name</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" id="cat_title" name="title" class="form-control" placeholder="Category Name" value="{{ isset($category->title) ? $category->title : null }}">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('cat_title') }}</span>
                                 </div>

                                 <label for="prod_type">Parent Category</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         @if($category->parent_id != 0)
                                         <select class="form-line" name="parent_id" id="parent_id">
                                             <option value="0">Parent Category</option>
                                           @foreach ($parent_categories as $key => $value)
                                             <option @if(isset($category->parent_id)) @if($category->parent_id == $value->id)  selected  @endif @endif value="{{ $value->id }}">{{ title_case($value->title) }}</option>
                                           @endforeach
                                         </select>
                                         @else
                                          <input type="text" value="N/A" class="form-control" placeholder="Parent Category" readonly>
                                          <input type="hidden" id="parent_id" name="parent_id" value="0" class="form-control" placeholder="Parent Category">
                                         @endif
                                     </div>
                                     <span class="col-pink">{{ $errors->first('parent_id') }}</span>
                                 </div>


                                 <label for="category_banner">Category Banner</label>
                                 <div class="form-group">
                                   <div class="form-line">
                                       <h6>Upload Category Banner</h6>
                                       <input type="file" id="category_banner" name="category_banner" class="form-control">
                                       <input type="hidden" name="category_banner_old"  value="{{ isset($category->category_banner) ? $category->category_banner : null }}">
                                   </div>
                                   <span class="col-pink">{{ $errors->first('category_banner') }}</span>
                                 </div>
                                 <div class="form-group">
                                    <h6>Current Category Banner</h6>
                                    @if(!empty($category->category_banner))
                                    <img src="{{ url('images/banner') }}/{{ $category->category_banner }}" style="width:30%" alt="">
                                    @endif
                                 </div>

                                 <label for="category_banner">Category Mobile Banner</label>
                                 <div class="form-group">
                                   <div class="form-line">
                                       <h6>Upload Category Mobile Banner</h6>
                                       <input type="file" id="category_mobile_banner" name="category_mobile_banner" class="form-control">
                                       <input type="hidden" name="category_mobile_banner_old"  value="{{ isset($category->category_mobile_banner) ? $category->category_mobile_banner : null }}">
                                   </div>
                                   <span class="col-pink">{{ $errors->first('category_mobile_banner') }}</span>
                                 </div>
                                 <div class="form-group">
                                    <h6>Current Category Mobile Banner</h6>
                                    @if(!empty($category->category_mobile_banner))
                                    <img src="{{ url('images/banner') }}/{{ $category->category_mobile_banner }}" style="width:30%" alt="">
                                    @endif
                                 </div>


                                 <label for="cat_sequence">Category Sequence</label>
                                 <div class="form-group">
                                   <div class="form-line">
                                       <input type="number" id="cat_sequence" name="sequence" class="form-control" placeholder="Category Sequence" value="{{ isset($category->sequence) ? $category->sequence : null }}">
                                   </div>
                                   <span class="col-pink">{{ $errors->first('sequence') }}</span>
                                 </div>

                                 <label for="status">Display On Website</label>
                                 <div class="form-group">
                                    <select class="form-line" name="status" id="status">
                                       <!--  <option value="1">Yes</option>
                                        <option value="0">No</option> -->
                                        <?php
                                          $status_array = array
                                          (
                                          array("title"=>'yes','id'=>1),
                                          array("title"=>'no','id'=>0)
                                          );
                                        ?>
                                        @foreach ($status_array as $key => $value)

                                         <option @if(isset($category->status)) @if($category->status == $value['id'])  selected  @endif @endif value="{{ $value['id'] }}">{{ title_case($value['title']) }}</option>
                                       @endforeach
                                    </select>
                                   <span class="col-pink">{{ $errors->first('status') }}</span>
                                 </div><br>
                                 <button type="submit" class="btn btn-primary m-t-15 waves-effect">Update Category</button>
                             </form>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Vertical Layout -->

         </div>
     </section>
@endsection
