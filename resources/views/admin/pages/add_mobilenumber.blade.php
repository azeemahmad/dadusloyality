@extends('admin.layouts.internal')
@section('content')
  <section class="content">
         <div class="container-fluid">
             <div class="block-header">
                 <h2>Mobile Number</h2>
             </div>

             <!-- Vertical Layout -->
             <div class="row clearfix">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="card">
                         <div class="header">
                         @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                        @endif
                             <h2>
                                 Mobile Number Details
                             </h2>
                             <ul class="header-dropdown m-r--5">
                                 <li class="dropdown">
                                     <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                         <i class="material-icons">more_vert</i>
                                     </a>
                                     <ul class="dropdown-menu pull-right">
                                         <li><a href="javascript:void(0);">Action</a></li>
                                         <li><a href="javascript:void(0);">Another action</a></li>
                                         <li><a href="javascript:void(0);">Something else here</a></li>
                                     </ul>
                                 </li>
                             </ul>
                         </div>
                         <div class="body">
                             <form method="post" action="{{ url('adm/add_mobile_submit') }}" id="add_mobile" autocomplete="off" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                  
                                 <label for="product_name">Mobile Number</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" id="mobile_number" name="mobile_number" class="form-control" placeholder="Mobile Number" minlength="1" maxlength="10" value="" required="">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('mobile_number') }}</span>
                                 </div>
                                 <label for="prod_type">Mobile Number Status</label>
                                 <div class="form-group">
                                     <div class="form-line">

                                        <input type="radio" name="status" id="status" class="radio-custom" value="1"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Active 
                                        <br/>
                                        <input type="radio" name="status" id="status" class="radio-custom"  value="0"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Deactivate <br/>
                                        
                                        
                                     </div>
                                     <span class="col-pink">{{ $errors->first('status') }}</span>
                                 </div>
                                 
                                 <button type="submit" class="btn btn-primary m-t-15 waves-effect">Update Mobile Number</button>
                             </form>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Vertical Layout -->

         </div>
     </section>
@endsection
