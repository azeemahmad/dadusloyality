@extends('admin.layouts.internal')
@section('content')
  <section class="content">
         <div class="container-fluid">
             <div class="block-header">
                 <h2>Pincode</h2>
             </div>

             <!-- Vertical Layout -->
             <div class="row clearfix">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="card">
                         <div class="header">
                             <h2>
                                 Pincode Details
                             </h2>
                             <ul class="header-dropdown m-r--5">
                                 <li class="dropdown">
                                     <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                         <i class="material-icons">more_vert</i>
                                     </a>
                                     <ul class="dropdown-menu pull-right">
                                         <li><a href="javascript:void(0);">Action</a></li>
                                         <li><a href="javascript:void(0);">Another action</a></li>
                                         <li><a href="javascript:void(0);">Something else here</a></li>
                                     </ul>
                                 </li>
                             </ul>
                         </div>
                         <div class="body">
                             <form method="post" action="{{ url('adm/add_pincode_submit') }}" id="add_pincode" autocomplete="off" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                  
                                 <label for="product_name">Pincode</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" id="pincode" name="pincode" class="form-control" placeholder="pincode" minlength="1" maxlength="6" value="" required="">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('pincode') }}</span>
                                 </div>
                                 <label for="prod_type">Pincode Status</label>
                                 <div class="form-group">
                                     <div class="form-line">

                                        <input type="radio" name="status" id="status" class="radio-custom" value="1"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Active 
                                        <br/>
                                        <input type="radio" name="status" id="status" class="radio-custom"  value="0"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Deactivate <br/>
                                        
                                        
                                     </div>
                                     <span class="col-pink">{{ $errors->first('status') }}</span>
                                 </div>
                                 
                                 <button type="submit" class="btn btn-primary m-t-15 waves-effect">Update Pincode</button>
                             </form>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Vertical Layout -->

         </div>
     </section>
@endsection
