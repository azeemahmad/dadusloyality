@extends('admin.layouts.internal')
@section('pagestyle')
  <!-- JQuery DataTable Css -->
   <link href="{{ url('admin/js/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
@endsection
@section('content')

  <section class="content">
      <div class="container-fluid">
          <!-- Exportable Table -->
          <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="card">
                      <div class="header">
                          <div class="text-center">
                          @if(Session::has('success_msg'))
                            {{Session::get('success_msg')}}
                          @elseif(Session::has('fail_msg'))
                            {{Session::get('fail_msg')}}
                          @else
                          @endif
                          </div>
                          <h2>
                              Manage Categories
                          </h2>
                          <ul class="header-dropdown m-r--5">
                              <li class="dropdown">
                                  <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                      <i class="material-icons">more_vert</i>
                                  </a>
                                  <ul class="dropdown-menu pull-right">
                                      <li><a href="javascript:void(0);">Action</a></li>
                                      <li><a href="javascript:void(0);">Another action</a></li>
                                      <li><a href="javascript:void(0);">Something else here</a></li>
                                  </ul>
                              </li>
                          </ul>
                      </div>
                      <div class="body">
                          <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                  <thead>
                                      <tr>
                                          <th>Category</th>
                                          <th>Parent Category</th>
                                          <th>Category Banner</th>
                                          <th>Category Mobile Banner</th>
                                          <th>Sequence</th>
                                          <th>Delete</th>
                                          <th>Edit</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                    @foreach ($all_categories as $pkey => $pvalue)
                                     <tr>
                                        <th>@if(!empty($pvalue->title)){{$pvalue->title}}@endif</th>
                                        <th>@if(!empty($pvalue->parent)) {{$pvalue->parent->title}}  @endif</th>
                                        <th>@if(!empty($pvalue->category_banner))<img style="width: 40%;" src="../images/banner/{{$pvalue->category_banner}}">@endif</th>
                                        <th>@if(!empty($pvalue->category_mobile_banner))<img style="width: 40%;" src="../images/banner/{{$pvalue->category_mobile_banner}}">@endif</th>
                                        <th>@if(!empty($pvalue->sequence)){{$pvalue->sequence}}@endif</th>
                                     <th><button type="button" class="btn bg-red waves-effect delete_category" data-toggle="modal" data-target="#delete_confirm" data-pid="{{ $pvalue->id }}" data-pname="{{ $pvalue->title }}" >Delete</button></th>
                                      <th><a href="{{ url('adm/edit_category') }}/{{ $pvalue->id }}" class="btn bg-indigo waves-effect">Edit</a></th>
                                    </tr>
                                    @endforeach
                                  </tbody>
                                  <tfoot>
                                      <tr>
                                          <th>Category</th>
                                          <th>Parent Category</th>
                                          <th>Category Banner</th>
                                          <th>Category Mobile Banner</th>
                                          <th>Sequence</th>
                                          <th>Delete</th>
                                          <th>Edit</th>
                                      </tr>
                                  </tfoot>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- #END# Exportable Table -->
      </div>
  </section>
   <!-- Delete Confirm -->
  <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
        </div>
        <div class="modal-footer">
          <form method="post" action="{{ url('adm/delete_category') }}">
          {!! csrf_field() !!}
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <input type="hidden" name="pid" id="delete_pid">
          <button type="submit" class="btn btn-primary">Confirm</button>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('pagescript')
  <!-- Jquery DataTable Plugin Js -->
     <script src="{{ url('admin/js/jquery-datatable/jquery.dataTables.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
     <script type="text/javascript">
       //Exportable table
      $('.js-exportable').DataTable({
          dom: 'Bfrtip',
          responsive: true,
          buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
          ]
      });
     </script>
@endsection
