@extends('admin.layouts.internal')
@section('content')
<section class="content">
  <div class="container-fluid">
      <div class="block-header">
          <h2>DASHBOARD</h2>
      </div>
      <div class="row clearfix">
          @if (session('success_msg'))
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-success">
                {{ session('success_msg') }}
            </div>
          </div>
          @endif
          @if (session('fail_msg'))
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-warning">
                {{ session('fail_msg') }}
            </div>
          </div>
          @endif
      </div>
      <!-- Widgets -->
      <div class="row clearfix">
      @if(!empty($product))
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-pink hover-expand-effect">
                  <div class="icon">
                      <i class="material-icons">playlist_add_check</i>
                  </div>
                  <div class="content">
                      <div class="text">Total Products</div>
                      <div class="number">{{$product}}</div>
                  </div>
              </div>
          </div>
          @endif
           @if(!empty($orders))
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-cyan hover-expand-effect">
                  <div class="icon">
                      <i class="material-icons">forum</i>
                  </div>
                  <div class="content">
                      <div class="text">Total Orders</div>
                      <div class="number">{{$orders}}</div>
                  </div>
              </div>
          </div>
          @endif
        </div>
      <!-- #END# Widgets -->
<!--  -->
  </div>
</section>
@endsection
