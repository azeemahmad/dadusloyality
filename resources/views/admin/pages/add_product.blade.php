@extends('admin.layouts.internal')
@section('pagestyle')
<link rel="stylesheet" href="{{ URL::asset('admin/css/multi-select.css') }}?v=20180316">
@endsection
@section('content')
  <section class="content">
         <div class="container-fluid">
             <div class="block-header">
                 <h2>Add Product</h2>
             </div>

             <!-- Vertical Layout -->
             <div class="row clearfix">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="card">
                         <div class="header">
                             <h2>
                                 Product Details
                             </h2>
                             <ul class="header-dropdown m-r--5">
                                 <li class="dropdown">
                                     <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                         <i class="material-icons">more_vert</i>
                                     </a>
                                     <ul class="dropdown-menu pull-right">
                                         <li><a href="javascript:void(0);">Action</a></li>
                                         <li><a href="javascript:void(0);">Another action</a></li>
                                         <li><a href="javascript:void(0);">Something else here</a></li>
                                     </ul>
                                 </li>
                             </ul>
                         </div>
                         <div class="body">
                             <form method="post" action="{{ url('adm/add_product_submit') }}" autocomplete="off" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                 <label for="product_name">Product Name</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" id="prod_title" name="prod_title" class="form-control" placeholder="Product Name" value="{{ old('prod_title') }}">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('prod_title') }}</span>
                                 </div>

                                 <label for="product_desc">Product Description</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" id="prod_desc" name="prod_desc" class="form-control" placeholder="Product Description" value="{{ old('prod_desc') }}">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('prod_desc') }}</span>
                                 </div>

                                 <label for="prod_type">Best Selling Product</label>
                                 <div class="form-group">
                                         <input type="checkbox" id="best_selling_prod" name="best_selling_prod" @if(old('best_selling_prod')) {{ 'checked' }}  @endif >
                                     <span class="col-pink">{{ $errors->first('best_selling_prod') }}</span>
                                 </div>
                                 <label for="prod_type">Product Quantity</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="number" id="prod_qty" name="prod_qty" class="form-control" placeholder="Product Quantity" value="{{ old('prod_qty') }}">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('prod_qty') }}</span>
                                 </div>

                                 {{-- <label for="prod_type">Product Price - 250 grams</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" name="prod_price[]" class="form-control" placeholder="Product Price for 250 Grams">
                                     </div>
                                 </div> --}}
                                 <label for="prod_type">Product Price - 500 grams</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" name="prod_price[]" class="form-control" placeholder="Product Price for 500 Grams">
                                     </div>
                                 </div>
                                 <label for="prod_type">Product Price - 1 KG</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" name="prod_price[]" class="form-control" placeholder="Product Price for 1KG">
                                     </div>
                                 </div>
                                 <label for="prod_type">Product Price Per Piece</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" name="prod_price[]" class="form-control" placeholder="Product Price per piece">
                                     </div>
                                 </div>

                                 <label for="prod_type">Product Category (select multiple)</label>
                                 <div class="form-group">
                                     <select class="form-line" name="category_id[]" id="category_id" multiple="multiple" >
                                       <option value="" disabled>Select Category</option>
                                       @foreach ($categories as $key => $value)
                                         <option value="{{ $value->id }}">{{ title_case($value->title) }}</option>
                                       @endforeach
                                     </select>
                                     <span class="col-pink">{{ $errors->first('category_id') }}</span>
                                 </div>

                                 <input type="hidden" name="prod_active" value="1">

                                 <label for="prod_sequence">Product Sequence</label>
                                 <div class="form-group">
                                   <div class="form-line">
                                       <input type="number" id="prod_sequence" name="prod_sequence" class="form-control" placeholder="Product Sequence" value="{{ old('prod_sequence') }}">
                                   </div>
                                   <span class="col-pink">{{ $errors->first('prod_sequence') }}</span>
                                 </div>

                                 <label for="status">Display On Website</label>
                                 <div class="form-group">
                                   <select class="form-line" name="status" id="status">
                                        <option value="1" selected>Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                   <span class="col-pink">{{ $errors->first('status') }}</span>
                                 </div>


                                 <label for="prod_sequence">Product Image</label>
                                 <div class="form-group">
                                   <div class="form-line">
                                       <input type="file" id="prod_image" name="prod_image" class="form-control">
                                   </div>
                                   <span class="col-pink">{{ $errors->first('prod_image') }}</span>
                                 </div>
                                 <br>
                                 <button type="submit" class="btn btn-primary m-t-15 waves-effect">Add Product</button>
                             </form>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Vertical Layout -->

         </div>
     </section>
@endsection
@section('pagescript')
<script src="{{ URL::asset('admin/js/jquery.multi-select.js?v=20180316') }}"></script>
<script type="text/javascript">
    $('#category_id').multiSelect();
</script>
@endsection
