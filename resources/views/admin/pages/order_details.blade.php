@extends('admin.layouts.internal')
@section('pagestyle')
    <!-- JQuery DataTable Css -->
    <link href="{{ url('admin/js/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
@endsection
@section('content')

            <?php

                $order_status = App\Models\Order_Status_Progress::where('order_id',$order['id'])->where('status_id','7')->first();
              
           ?>


    <section class="content">
        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Orders Details
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            
                            <div class="section-body center cl-black">
                                <div class="row invo">
                                    <div class="col-md-6 text-left">
                                        Delivery to:<br>
                                        <strong>{{ title_case($order->name) }}</strong><br>
                                        {{ $order->mobile }}<br>
                                        {{ $order->address }}<br>
                                        {{ $order->state }}<br>
                                        {{ $order->city }}<br>
                                        {{ $order->pincode }}<br>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        Order Date: {{ date('d-m-Y H:i:s', strtotime($order->created_at)) }}<br>
                                        Order Id: {{ $order->id }} <br>
                                        Payment Method: {{ $order->payment_method }}
                                    </div>
                                    <div class="col-md-12">
                                        <table class="table invoice_table">
                                            <tr>
                                                <th>Product Id</th>
                                                <th>Product Name</th>
                                                <th>Pack Weight/Units</th>
                                                <th>Product Quantity</th>
                                                <th>Product Rate</th>
                                                <th>Product Amount</th>
                                            </tr>
                                            @foreach ($order['cart'] as $pwid => $pval)
                                                <tr>
                                                    <td>{{ $pval['data']['id'] }}</td>
                                                    <td>{{ $pval['data']['prod_title'] }}</td>
                                                    <td>{{ $pval['weight']['weight'] }}</td>
                                                    <td>{{ $pval['qty'] }}</td>
                                                    <td>{{ $pval['weight']['price'] }}</td>
                                                    <td><strong> {{ $pval['qty'] * $pval['weight']['price'] }}</strong></td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td class="noborder" colspan="6"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" class="text-right">Cart Total</td>
                                                <td><strong>{{ $order->cart_total }}</strong></td>
                                            </tr>
                                            {{-- <tr>
                                                <td colspan="5" class="text-right" >Tax</td>
                                                <td><strong> {{ $order->tax }}</strong></td>
                                            </tr> --}}
                                            <tr>
                                                <td colspan="5" class="text-right" >Discount</td>
                                                <td><strong> {{ $order->discount }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" class="text-right" >Delivery Charges</td>
                                                <td><strong> {{  $order->delivery_charge  }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" class="text-right">Total</td>
                                                <td><strong> {{  $order->grand_total  }}</strong></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-12">



                                        <strong>Status:</strong>

                                        <form action="javascript:void(0);" method="post">
                                            {{ csrf_field() }}
                                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                                        <select id="mySelect" class="order_status_sele form-control" name="order_status">
                                            @if($order_status['status_id'] ==7)
                                                @foreach ($status as $st)
                                                    <option disabled @if($st->id == $order->status->id) selected @endif value="{{ $st->id }}">{{ $st->title }}</option>
                                                @endforeach
                                            @else

                                                @foreach ($status as $st)
                                                    <option @if($st->id == $order->status->id) selected @endif value="{{ $st->id }}">{{ $st->title }}</option>
                                                @endforeach                                          
                                            @endif
                                        </select>
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect" onclick="CheckConfirm()" >Update Status</button>
                                        </form>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
@endsection
@section('pagescript')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ url('admin/js/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{ url('admin/js/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ url('admin/js/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{ url('admin/js/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{ url('admin/js/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{ url('admin/js/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{ url('admin/js/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{ url('admin/js/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{ url('admin/js/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
    <script type="text/javascript">
    //Exportable table
    $('.product-exportable').DataTable({
        order : [ 0, 'desc' ],
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    // $('.order_status_sele ').on('change',function (){
    //    var id = $(this).val();
    //    if(id == 6)
    //    {
    //     alert('dsfdsfdsfdsf');
    //    }
    // });
    </script>
    <script>

function CheckConfirm() {
    
   var selector = document.getElementById('mySelect');
    var val = selector[selector.selectedIndex].value;

    var url = window.location.href;
    var id = url.substring(url.lastIndexOf('/') + 1);

    var value = val.concat('_'+id);


    var host = 'https://'+location.hostname;
   if(val == 4){

      var result = confirm("You are proceed to Cancel and Refund?");
            if (result==true){
                window.location.href= host+'/adm/changestatus/'+value;
            }
            else{
                return false;
                
            }
   }
else{

    window.location.href= host+'/adm/changestatus/'+value;
}

}
</script>
@endsection
