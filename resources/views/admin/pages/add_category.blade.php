@extends('admin.layouts.internal')
@section('content')
  <section class="content">
         <div class="container-fluid">
             <div class="block-header">
                 <h2>Add Category</h2>
             </div>

             <!-- Vertical Layout -->
             <div class="row clearfix">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="card">
                         <div class="header">
                             <h2>
                                 Category Details
                             </h2>
                             <ul class="header-dropdown m-r--5">
                                 <li class="dropdown">
                                     <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                         <i class="material-icons">more_vert</i>
                                     </a>
                                     <ul class="dropdown-menu pull-right">
                                         <li><a href="javascript:void(0);">Action</a></li>
                                         <li><a href="javascript:void(0);">Another action</a></li>
                                         <li><a href="javascript:void(0);">Something else here</a></li>
                                     </ul>
                                 </li>
                             </ul>
                         </div>
                         <div class="body">

                             <form method="post" action="{{ url('adm/add_category_submit') }}" autocomplete="off" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                 <label for="category_name">Category Name</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" id="cat_title" name="title" class="form-control" placeholder="Category Name">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('cat_title') }}</span>
                                 </div>

                                 <label for="prod_type">Parent Category</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         @if(!empty($all_categories))
                                         <select class="form-line" name="parent_id" id="parent_id">
                                             <option value="0">Parent Category</option>
                                           @foreach ($all_categories as $key => $value)
                                             <option value="{{ $value->id }}">{{ title_case($value->title) }}</option>
                                           @endforeach
                                         </select>
                                         @else
                                          <input type="text" id="parent_id" name="parent_id" class="form-control" placeholder="Parent Category">
                                         @endif
                                     </div>
                                     <span class="col-pink">{{ $errors->first('parent_id') }}</span>
                                 </div>
                                 <label for="category_banner">Category Banner</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="file" id="category_banner" name="category_banner" class="form-control" placeholder="Category Banner">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('category_banner') }}</span>
                                 </div>

                                 <label for="category_banner">Category Mobile Banner</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="file" id="category_mobile_banner" name="category_mobile_banner" class="form-control" placeholder="Category Mobile Banner">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('category_mobile_banner') }}</span>
                                 </div>

                                 <label for="prod_type">Category Sequence</label>
                                 <div class="form-group">
                                    <input type="text" id="sequence" name="sequence" class="form-control" placeholder="Sequence">
                                     <span class="col-pink">{{ $errors->first('sequence') }}</span>
                                 </div>

                                 <label for="status">Display On Website</label>
                                 <div class="form-group">
                                   <select class="form-line" name="status" id="status">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                   <span class="col-pink">{{ $errors->first('status') }}</span>
                                 </div>

                                 <br>
                                 <button type="submit" class="btn btn-primary m-t-15 waves-effect">Add Category</button>
                             </form>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Vertical Layout -->

         </div>
     </section>
@endsection
