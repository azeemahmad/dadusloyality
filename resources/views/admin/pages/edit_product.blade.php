@extends('admin.layouts.internal')
@section('pagestyle')
<link rel="stylesheet" href="{{ URL::asset('admin/css/multi-select.css') }}?v=20180316">
@endsection
@section('content')
  <section class="content">
         <div class="container-fluid">
             <div class="block-header">
                 <h2>Add Product</h2>
             </div>

             <!-- Vertical Layout -->
             <div class="row clearfix">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="card">
                         <div class="header">
                             <h2>
                                 Product Details
                             </h2>
                             <ul class="header-dropdown m-r--5">
                                 <li class="dropdown">
                                     <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                         <i class="material-icons">more_vert</i>
                                     </a>
                                     <ul class="dropdown-menu pull-right">
                                         <li><a href="javascript:void(0);">Action</a></li>
                                         <li><a href="javascript:void(0);">Another action</a></li>
                                         <li><a href="javascript:void(0);">Something else here</a></li>
                                     </ul>
                                 </li>
                             </ul>
                         </div>
                         <div class="body">
                             <form method="post" action="{{ url('adm/edit_product_submit') }}" id="edit_product" autocomplete="off" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="id" value="{{ isset($product->id) ? $product->id : null }}" >
                                 <label for="product_name">Product Name</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" id="prod_title" name="prod_title" class="form-control" placeholder="Product Name" value="{{ isset($product->prod_title) ? $product->prod_title : null }}">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('prod_title') }}</span>
                                 </div>
                                 <input type="hidden" value="{{ $selected_categories }}" id="selected_categories">
                                 <label for="product_desc">Product Description</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" id="prod_desc" name="prod_desc" class="form-control" placeholder="Product Description" value="{{ isset($product->prod_desc) ? $product->prod_desc : null }}">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('prod_desc') }}</span>
                                 </div>

                                 <label for="prod_type">Best Selling Product</label>
                                 <div class="form-group">
                                          <input type="checkbox" id="best_selling_prod" name="best_selling_prod" >
                                     <span class="col-pink">{{ $errors->first('best_selling_prod') }}</span>
                                 </div>
                                 <label for="prod_type">Product Quantity</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="number" id="prod_qty" name="prod_qty" class="form-control" placeholder="Product Quantity" value="{{ isset($product->prod_qty) ? $product->prod_qty : null }}">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('prod_qty') }}</span>
                                 </div>

                                 <label for="prod_type">Product Price - 500 grams</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" name="prod_price[500 Grams][{{ $current_weight_data['one_g_id'] or null }}]" class="form-control" value="{{ isset($current_weight_data['one_g_price']) ? $current_weight_data['one_g_price'] : null }}" placeholder="Product Price for 500 Grams">
                                     </div>
                                 </div>
                                 <label for="prod_type">Product Price - 1 KG</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" name="prod_price[1 KG][ {{ $current_weight_data['one_k_id'] or null }} ]" class="form-control" value="{{ isset($current_weight_data['one_k_price']) ? $current_weight_data['one_k_price'] : null }}" placeholder="Product Price for 1KG">
                                     </div>
                                 </div>
                                 <label for="prod_type">Product Price Per Piece</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" name="prod_price[1 Piece][{{ $current_weight_data['one_p_id'] or null }}]" class="form-control" value="{{ isset($current_weight_data['one_p_price']) ? $current_weight_data['one_p_price'] : null }}" placeholder="Product Price per piece">
                                     </div>
                                 </div>

                                 {{-- @foreach ($product->weight as $weight)
                                 <label for="prod_type">Product Price - {{ $weight->weight }}</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" name="prod_price[{{ $weight->id }}]" class="form-control" placeholder="Product Price for {{ $weight->weight }}" value="{{ $weight->price }}">
                                     </div>
                                 </div>
                                @endforeach --}}

                                 <label for="prod_type">Product Category</label>
                                 <div class="form-group">
                                      <select class="form-line" name="category_id[]" id="category_id" multiple>
                                       <option disabled>Select Category</option>
                                       @foreach ($categories as $catk => $catv)
                                         <option value="{{ $catv->id }}">{{ $catv->title }}</option>
                                       @endforeach
                                     </select>
                                     <span class="col-pink">{{ $errors->first('category_id') }}</span>
                                 </div>

                                 <input type="hidden" name="prod_active" value="1">

                                 <label for="prod_sequence">Product Sequence</label>
                                 <div class="form-group">
                                   <div class="form-line">
                                       <input type="number" id="prod_sequence" name="prod_sequence" class="form-control" placeholder="Product Sequence" value="{{ isset($product->prod_sequence) ? $product->prod_sequence : null }}">
                                   </div>
                                   <span class="col-pink">{{ $errors->first('prod_sequence') }}</span>
                                 </div>

                                 <label for="status">Display On Website</label>
                                 <div class="form-group">
                                    <select class="form-line" name="status" id="status">
                                       <!--  <option value="1">Yes</option>
                                        <option value="0">No</option> -->
                                        <?php
                                          $status_array = array
                                          (
                                          array("title"=>'yes','id'=>1),
                                          array("title"=>'no','id'=>0)
                                          );
                                        ?>
                                        @foreach ($status_array as $key => $value)

                                         <option @if(isset($product->status)) @if($product->status == $value['id'])  selected  @endif @endif value="{{ $value['id'] }}">{{ title_case($value['title']) }}</option>
                                       @endforeach
                                    </select>
                                   <span class="col-pink">{{ $errors->first('status') }}</span>
                                 </div>

                                 <label for="prod_sequence">Product Image</label>
                                 <div class="form-group">
                                   <div class="form-line">
                                       <h6>Upload New image</h6>
                                       <input type="file" id="prod_image" name="prod_image" class="form-control">
                                       <input type="hidden" name="prod_image_old"  value="{{ isset($product->prod_image) ? $product->prod_image : null }}">
                                   </div>
                                   <span class="col-pink">{{ $errors->first('prod_image') }}</span>
                                 </div>
                                 <div class="form-group">
                                    <h6>Current image</h6>

                                    <img src="{{ url('images/products') }}/{{ $product->prod_image }}" alt="">
                                 </div>
                                 <br>
                                 <button type="submit" class="btn btn-primary m-t-15 waves-effect">Update Product</button>
                             </form>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Vertical Layout -->

         </div>
     </section>
@endsection
@section('pagescript')
<script src="{{ URL::asset('admin/js/jquery.multi-select.js?v=20180316') }}"></script>
<script type="text/javascript">
$(document).ready(function(){

    var selected_cat = $('#selected_categories').val();
    var selected_cat_ar = selected_cat.split(",");

    $('#category_id').multiSelect();
    $('#category_id').multiSelect('select',selected_cat_ar);
});
</script>
@endsection
