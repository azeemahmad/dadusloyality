@extends('admin.layouts.internal')
@section('pagestyle')
  <!-- JQuery DataTable Css -->
   <link href="{{ url('admin/js/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
   <style>
     .template{
      width: 800px;
     }
   </style>
@endsection
@section('content')
{{print_r(Session::all())}}
  <section class="content">
      <div class="container-fluid">
          <!-- Exportable Table -->
          <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="card">
                      <div class="header">
                       @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}
                          {{ session()->forget('message') }}
                        </p>
                        @endif
                          <h2>
                              Manage SMS Templates
                          </h2>
                          <ul class="header-dropdown m-r--5">
                              <li class="dropdown">
                                  <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                      <i class="material-icons">more_vert</i>
                                  </a>
                                  <ul class="dropdown-menu pull-right">
                                      <li><a href="javascript:void(0);">Action</a></li>
                                      <li><a href="javascript:void(0);">Another action</a></li>
                                      <li><a href="javascript:void(0);">Something else here</a></li>
                                  </ul>
                              </li>
                          </ul>
                      </div>
                      <div class="body">
                          <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                  <thead>
                                      <tr>
                                          <th>Title</th>                        
                                          <th>SMS Templates</th>
                                          <th>Update</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                    @foreach ($manage_sms_template as $pkey => $pvalue)
                                    <tr>
                                    <form method="post" action="{{ url('adm/edit_smstemplate_submit') }}" id="edit_pincode" autocomplete="off" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="template_id" value="{{ $pvalue->id }}">
                                      <th>{{ title_case($pvalue->template_title) }}</th>
                                      <th><input type="text" class="template" name="template_description" value="{{ title_case($pvalue->template_description) }}"></th>    
                                      <th><input type="submit" class="btn bg-indigo waves-effect" value="Update"></th>
                                    </form>  
                                    </tr>
                                    @endforeach
                                  </tbody>
                                  <tfoot>
                                      <tr>
                                         <th>Title</th>                        
                                          <th>SMS Templates</th>
                                          <th>Update</th>
                                      </tr>
                                  </tfoot>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- #END# Exportable Table -->
      </div>
  </section>
   <!-- Delete Confirm -->
  <div class="modal fade" id="delete_mobile_confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
        </div>
        <div class="modal-footer">
          <form method="post" action="{{ url('adm/delete_mobile_number') }}">
          {!! csrf_field() !!}
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <input type="hidden" name="pid" id="delete_pid">
          <button type="submit" class="btn btn-primary">Confirm</button>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('pagescript')
  <!-- Jquery DataTable Plugin Js -->
     <script src="{{ url('admin/js/jquery-datatable/jquery.dataTables.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
     <script src="{{ url('admin/js/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
     <script type="text/javascript">
       //Exportable table
      $('.js-exportable').DataTable({
          dom: 'Bfrtip',
          responsive: true,
          buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
          ]
      });
     </script>
@endsection
`