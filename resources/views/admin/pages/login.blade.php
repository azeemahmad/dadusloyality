@extends('admin.layouts.external')
@section('content')
<div class="login-box">
    <div class="logo">
        <a href="javascript:void(0);">Admin<b>Dadus</b></a>
    </div>
    <div class="card">
        <div class="body">
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
            <form id="sign_in" method="POST" action="{{ url('adm/login_submit') }}">
            {{ csrf_field() }}
                <div class="msg">Sign in to start your session</div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">person</i>
                    </span>
                    <div class="form-line">
                        <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">lock</i>
                    </span>
                    <div class="form-line">
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-xs-4" style="display: block;margin: 0 auto;float: none;">
                        <input type="submit" id="loginsubmit" class="btn btn-block bg-pink waves-effect" value="SIGN IN">
                       <!--  <button onclick="location.href = '{{ url('adm/dashboard') }}'" class="btn btn-block bg-pink waves-effect" type="button">SIGN IN</button> -->
                    </div>
                </div>
                <div class="row m-t-15 m-b--20">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
