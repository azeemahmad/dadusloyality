@extends('admin.layouts.internal')
@section('content')
  <section class="content">
         <div class="container-fluid">
             <div class="block-header">
                 <h2>Pincode</h2>
             </div>
             <!-- Vertical Layout -->
             <div class="row clearfix">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     @if(session()->has('errmsg'))
                         <p class="alert alert-danger">{{ session()->get('errmsg') }}
                           {{ session()->forget('errmsg') }}
                         </p>
                     @endif
                     @if(session()->has('msg'))
                         <p class="alert alert-success">{{ session()->get('msg') }}
                           {{ session()->forget('msg') }}
                         </p>
                     @endif
                     <div class="card">
                         <div class="header">
                             <h2>
                                 Pincode Details
                             </h2>
                             <ul class="header-dropdown m-r--5">
                                 <li class="dropdown">
                                     <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                         <i class="material-icons">more_vert</i>
                                     </a>
                                     <ul class="dropdown-menu pull-right">
                                         <li><a href="javascript:void(0);">Action</a></li>
                                         <li><a href="javascript:void(0);">Another action</a></li>
                                         <li><a href="javascript:void(0);">Something else here</a></li>
                                     </ul>
                                 </li>
                             </ul>
                         </div>
                         <div class="body">
                             <form method="post" action="{{ url('adm/add_pincode_bulk') }}" autocomplete="off" enctype="multipart/form-data">
                                  {{ csrf_field() }}

                                 <label for="product_name">Pincode CSV <a download="sample.csv" href="{{ url('sample/example_pincode.csv') }}">Download Example File</a></label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="file" id="pincode_file" name="pincode_file" class="form-control" required>
                                     </div>
                                     <span class="col-pink">{{ $errors->first('pincode') }}</span>
                                 </div>
                                 <button type="submit" class="btn btn-primary m-t-15 waves-effect">Upload Pincode</button>
                             </form>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Vertical Layout -->

         </div>
     </section>
@endsection
