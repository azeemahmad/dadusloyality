@extends('admin.layouts.internal')
@section('content')
  <section class="content">
         <div class="container-fluid">
             <div class="block-header">
                 <h2>Manage Delivery Charges</h2>
             </div>

             <!-- Vertical Layout -->
             <div class="row clearfix">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="card">
                         <div class="header">
                             <h2>
                                 Product Details
                             </h2>
                             <ul class="header-dropdown m-r--5">
                                 <li class="dropdown">
                                     <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                         <i class="material-icons">more_vert</i>
                                     </a>
                                     <ul class="dropdown-menu pull-right">
                                         <li><a href="javascript:void(0);">Action</a></li>
                                         <li><a href="javascript:void(0);">Another action</a></li>
                                         <li><a href="javascript:void(0);">Something else here</a></li>
                                     </ul>
                                 </li>
                             </ul>
                         </div>
                         <div class="body">
                             <form method="post" action="{{ url('adm/delivery_charges_submit') }}" id="delivery_charges" autocomplete="off" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                 <label for="delivery_charges">Delivery Charges</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" id="delivery_charges" name="delivery_charges" class="form-control" placeholder="Delivery Charges" value="{{ isset($delivery_charges->delivery_charges) ? $delivery_charges->delivery_charges : null }}">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('delivery_charges') }}</span>
                                 </div>
                                 <label for="min_cart_value">Minimum Cart Amount</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" id="min_cart_value" name="min_cart_value" class="form-control" placeholder="Minimum Cart Value" value="{{ isset($delivery_charges->min_cart_value) ? $delivery_charges->min_cart_value : null }}">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('min_cart_value') }}</span>
                                 </div>

                                 <br>
                                 <button type="submit" class="btn btn-primary m-t-15 waves-effect">Update Delivery Charges</button>
                             </form>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Vertical Layout -->

         </div>
     </section>
@endsection
