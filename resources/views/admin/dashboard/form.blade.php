@extends('admin.layouts.internal')
@section('pagestyle')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
@endsection
@section('content')
  <section class="content">
         <div class="container-fluid">
             <div class="block-header">
                 <h2>Add Rewards</h2>
             </div>
            <!-- Vertical Layout -->
             <div class="row clearfix">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="card">
                         <div class="header">
                             <h2>
                                 Customer Reward Points
                             </h2>
                             <ul class="header-dropdown m-r--5">
                                 <li class="dropdown">
                                     <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                         <i class="material-icons">more_vert</i>
                                     </a>
                                     <ul class="dropdown-menu pull-right">
                                         <li><a href="javascript:void(0);">Action</a></li>
                                         <li><a href="javascript:void(0);">Another action</a></li>
                                         <li><a href="javascript:void(0);">Something else here</a></li>
                                     </ul>
                                 </li>
                             </ul>
                         </div>
                         <div class="body">
                             <form method="post" action="{{ url('adm/addrewards') }}" autocomplete="off" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                 <label for="category_name">Mobile Number</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" class="form-control typeahead" name="mobile_number" id="mobile_number" value="{{ old('mobile_number') }}"  placeholder="Enter Mobile Number">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('mobile_number') }}</span>
                                 </div>

                                <label for="first_name">Customer First Name</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" id="first_name" name="first_name" class="form-control" placeholder="Customer First Name">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('first_name') }}</span>
                                 </div>
                                 <label for="last_name">Customer Last Name</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Customer Last Name">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('last_name') }}</span>
                                 </div>

                                 <label for="dob">DOB</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="text" id="dob" name="dob" class="form-control datepicker" autocomplete="off" placeholder="Date of Birth">
                                     </div>
                                     <span class="col-pink">{{ $errors->first('dob') }}</span>
                                 </div>
{{-- 
                                 <label for="dob">Address (Optional)</label>
                                 <div class="form-group">
                                     <div class="form-line">
                                         <textarea type="text" id="address" name="address" class="form-control" autocomplete="off" placeholder="Address"></textarea>
                                     </div>
                                     <span class="col-pink">{{ $errors->first('address') }}</span>
                                 </div> --}}


                                 <label for="reward_points">Item</label>
                                 <select  class="form-control" name="reward_points" id="reward_points"   placeholder="Enter Reward Points">
                                     <option value="100">Rs. 500 </option>
                                     <option value="200">Rs. 1000 </option>
                                     <option value="300">Rs. 1500 </option>
                                 </select>
                                  <small class="alert-danger">{!!($errors->first('reward_points'))!!}</small>

                                 <br>
                                 <button type="submit" class="btn btn-primary m-t-15 waves-effect">Add Reward</button>
                             </form>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Vertical Layout -->

         </div>
     </section>
@endsection
@section('pagescript')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    $(document).ready( function() {
        $( ".datepicker" ).datepicker({
             defaultDate: "-25y",
             dateFormat:"dd-mm-yy",
        });
    });
</script>
@endsection
