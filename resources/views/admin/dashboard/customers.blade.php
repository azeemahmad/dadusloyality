@extends('admin.layouts.internal')
@section('pagestyle')
  <!-- JQuery DataTable Css -->
   <link href="{{ url('admin/js/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
@endsection
@section('content')

<div class="content-wrapper" >
    <!-- Main content -->
    <section class="content-header">
        <h1>
        + Customers
        <small>Dadus Loyalty Program</small>
      </h1>
        <ol class="breadcrumb">
             <li><a href="{{ env('APP_URL') }}home"><i class="fa fa-paperclip"></i> Dashboard </a></li>
            <li class="active">+ customers</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box-body">
                    <div class="box box-warning row">
                        <div class="box-header">Customer Details</div>
                         <div class="modal-content">
                               @if (session('status'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ session('status') }}
                            </div>
                            @endif
                            @if (session('message'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ session('message') }}
                            </div>
                            @endif
                        </div>
    <table id="datatable" class="table table-bordered table-striped">

            <thead><tr>
            <td>Sr.no</td>
                <td>Name</td>
                <td>Mobile number</td>
                <td>Reward points</td>
            </tr>
            </thead>
            <tbody>

            @foreach ($customers as $key=>$cust)
                <tr>
                <td>{{++$key}}</td>
                    <td>{{ title_case($cust->name) }}</td>
                    <td>{{$cust->mobile}}</td>
                    <td>{{$cust->reward_points}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
                   </div>
                </div>
            </div>
            </div>
        </div>
    </section>
    </div>

@endsection
