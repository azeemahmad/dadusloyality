<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<div>
    Dear {!! $name !!}, <br><br>
    {!! $msg !!}, <br>
    Order details<br>
    Order Id: {!! $order_id !!},<br>
    Order Total: {!! $total !!}
</div>
</body>
</html>
