<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107697284-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-107697284-4');
</script>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
   <title>@yield('title')</title>
   <meta name="keywords" content="@yield('keywords')" />
   <meta name="description" content="@yield('description')" />
   	<link rel="canonical" href="{{ url()->current() }}" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="shortcut icon" type="image/png" href="{{ url('images/favicon.png') }}"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{ URL::asset('/css/reset.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ URL::asset('/css/main.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>


    <!-- zoomplugin -->
    <link rel="stylesheet" href="{{ URL::asset('/css/jquery.exzoom.css') }}">
    <!-- <link rel="stylesheet" href="{{ URL::asset('/css/easyzoom.css') }}"> -->
   
    
    @yield('pagestyle')
    @include('includes.pixel')
    @yield('conversioncode')
</head>
<body>
    @include('includes.innerheadertop')
    @yield('content')
    @include('includes.innerfooter')
    <!--  Scripts-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.4/hammer.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.ui.min.js"></script>
    <script src="{{ URL::asset('js/vertical-slider.js') }}"></script>
    <script src="//matthew.wagerfield.com/parallax/deploy/jquery.parallax.js"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
    <!-- <script src="{{ URL::asset('js/jquery.selectric.min.js') }}"></script> -->
    <script src="{{ URL::asset('js/number_format.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.validate.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.cookie.js')}}"></script>
    <script src="{{ URL::asset('js/letscheckout.js') }}"></script>
    <script src="{{ URL::asset('js/notify.js') }}"></script>

    <script src="{{ URL::asset('js/main.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.exzoom.js') }}"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function () {
        // $('body').on('click',function(e){
        //     var checkbox = $('#checkedbox');
        //     if (checkbox.is(":checked")) {
        //       $("#exampleModalLong").modal('hide');
        //     }
        //     else{
        //         // do the confirmation thing here
        //         e.preventDefault();
        //         return false;
        //     }
        // });
        $('#exampleModalLong').modal({backdrop: 'static', keyboard: false})
        $("#checkedbox").on("click", function (e) {
            var checkbox = $(this);
            if (checkbox.is(":checked")) {
              $("#exampleModalLong").modal('hide');
            }
            else{
                // do the confirmation thing here
                e.preventDefault();
                return false;
            }
        });

        if($.fn.carousel)
        {
            $('#media').carousel({
                pause: true,
                interval: false,
            });
        }

        $('.owl-carousel').owlCarousel({
            items: 3,
            nav: true,
            dots: false,
            mouseDrag: true,
            responsiveClass: true,
            navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
            autoplay: true,
            autoplayTimeout: 6000,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 2
                },
                769: {
                    items: 3
                }
            }
        });

        //Background image
        $('.img-wrap').each(function () {
            var img = $(this).find('img');
            var src = img.attr('src');
            $(this).css('background-image', 'url( ' + src + ' )');
        });
        $(".datepicker").datepicker({
            dateFormat: 'yy-mm-dd'
        });

    });
    </script>
    @yield('pagescript')
</body>
</html>
