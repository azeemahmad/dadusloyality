<!DOCTYPE html>
<html lang="en" class="home_html">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107697284-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-107697284-4');
</script>
   <title>@yield('title')</title>
   <meta name="keywords" content="@yield('keywords')" />
   <meta name="description" content="@yield('description')" />
  <meta charset="UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
  <meta name="msapplication-tap-highlight" content="no" />
  <link rel="canonical" href="{{ url()->current() }}" />
  <link rel="shortcut icon" type="image/png" href="{{ url('images/favicon.png') }}"/>
  <link rel="stylesheet" href="{{ URL::asset('/css/reset.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('/css/vertical-slider.css') }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ URL::asset('/css/main.css') }}">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
  @include('includes.pixel')
</head>
<body class="home_body">
  @include('includes.outerheader')
  @yield('content')
  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.4/hammer.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.ui.min.js"></script>
  <script src="{{ URL::asset('js/vertical-slider.js') }}"></script>
  <script src="//matthew.wagerfield.com/parallax/deploy/jquery.parallax.js"></script>
  <script>
     // Initialise the vertical slider
  if(verticalSlider){
    verticalSlider.init();
    var $scene;
    $scene = $(".scene").parallax(), $scene.parallax("scalar", 120, 100)
  }
  </script>
  <script src="{{ URL::asset('js/main.js') }}"></script>
</body>
</html>
