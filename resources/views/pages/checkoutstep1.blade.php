@extends('layouts.internal')
@section('content')
    <!-- second product category header -->
    <div class="navbar navbar-secondary">
        <div class="steps">
            <div class="steps-inner">
                <div class="step-item active">
                    <div class="step-count">1</div>
                    <div class="step-label">Your Items</div>
                </div> <!-- /.item-step-item -->
                <div class="step-item">
                    <div class="step-count">2</div>
                    <div class="step-label">Information</div>
                </div> <!-- /.step-item -->
                <div class="step-item">
                    <div class="step-count">3</div>
                    <div class="step-label">Shipping</div>
                </div> <!-- /.step-item -->
                <div class="step-item">
                    <div class="step-count">4</div>
                    <div class="step-label">Payment</div>
                </div> <!-- /.step-item -->
            </div>
        </div> <!-- /.steps -->
    </div>
    <!-- end of header -->

    @if(session()->has('error_msg'))
    <div class="wrapper checkoutpage">
        <section class="section">
            <div class="containersection">
                <div class="section-inner">
                    <div class="alert alert-danger" role="alert">
	   		   		      {{ session('error_msg') }}
	      			</div>
                </div>
            </div>
        </section>
    </div>
    @endif

    <div class="wrapper checkoutpage">
        <section class="section">
            <div class="container section">
                <div class="section-inner">
                    <div class="section-body text-center">
                        @if( isset($cart) && !empty($cart) )
                            <h2 class="prod-title padding-top">Your Items</h2>
                            <div class="title_img"><img src="{{ URL::asset('images/rsz_divider.png') }}"></div>

                            <p class="section-p">You can remove and add quantity to each item in the cart</p>
                            <div class="items">
                                @foreach ($cart as $pid => $pdata)
                                    {{-- {{ dd($pdata) }} --}}
                                    <div class="item">
                                        <div class="item-inner">
                                            <figure>
                                                <a href="#">
                                                    <img src="images/products/{{ $pdata['data']['prod_image'] }}" alt="product photo">
                                                </a>
                                            </figure>
                                            <div class="item-details">
                                                <div class="item-title"><div class="food-type veg"></div>{{ $pdata['data']['prod_title'] }} - <span class="prod-qty">{{ $pdata['weight']['weight'] }}</span></div>
                                                <div class="item-title itm-desc-ft">{{ $pdata['data']['prod_desc'] }}</div>
                                            </div> <!-- /.item-details -->
                                            <div class="item-price">
                                                <div class="value" id="item-1-price" data-price>&#8377; {{ $pdata['weight']['price'] * $pdata['qty'] }}</div>
                                                <span class="quantity">
                                                    <input type="submit" value="-" class="cart_update" field="quantity" data-act="remove" data-pwid="{{ $pid }}">
                                                    <input type="text" name="quantity" value="{{ $pdata['qty'] }}" class="qty" readonly>
                                                    <input type="submit" value="+" class="cart_update" field="quantity" data-act="add" data-pwid="{{ $pid }}">
                                                </span>
                                            </div> <!-- /.item-price -->
                                            <div class="close"><i class="ion-close"></i></div>
                                        </div> <!-- /.item-inner -->
                                    </div> <!-- /.item -->
                                @endforeach
                            </div> <!-- /.items -->
                        </div> <!-- /.section-body -->
                        <div class="section-footer">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">

                                    <div class="coupon">
                                        <div class="coupon-inner coupon-form">

                                            <h2 class="section-title sm">Enter Your Pincode To Know Delivery </h2>

                                            <div class="form-group">
                                                <input type="text" name="pincode" autocomplete="off" id="val_pin" class="form-control" placeholder="Pincode">
                                                <button type="submit" class="btn btn-default" id="pincheck">Check</button>
                                                <div id="showError"></div>
                                            </div> <!-- /.form-group -->
                                            <!-- /.pincode-form -->
                                        </div> <!-- /.pincode-inner -->
                                    </div> <!-- /.pincode -->
                                </div> <!-- /.col-md-6 -->
                                <div class="col-md-6 col-sm-6">
                                    <h2 class="section-title sm">Estimation</h2>

                                    <div class="total-info">
                                        <div class="total-item">
                                            <div class="total-name">Order</div>
                                            <div class="total-value" id="total-order">Rs. {{ $cart_total }} </div>
                                        </div>
                                        <div class="total-item">
                                            <div class="total-name">Shipping</div>
                                            <div class="total-value" id="total-shipping">Rs.{{ $delivery_charges }}</div>
                                        </div>
                                        <div class="total-item" data-calculate-min="true">
                                            <div class="total-name">Discount</div>
                                            <div class="total-value" id="total-discount">Rs.0</div>
                                        </div>
                                        {{-- <div class="total-item">
                                            <div class="total-name">Tax</div>
                                            <div class="total-value" id="total-tax">Rs.0</div>
                                        </div> --}}
                                        <div class="total-item total">
                                            <div class="total-name">Total</div>
                                            <div class="total-value" id="total-all">Rs.{{ $grand_total }}
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- /.col-md-6 -->
                            </div> <!-- /.row -->
                        </div> <!-- /.footer -->
                        <div class="section-cta">
                            <p>After this step you will fill in the information</p>
                            <button class="btn btn-primary" data-href="checkoutstep2">Next Step <i class="ion-ios-arrow-thin-right"></i></button>
                        </div> <!-- /.section-cta -->
                    @else
                        <h1 class="section-title padding-top your_cart_title" style="font-size:25px;margin-bottom:30px;">Your Cart</h1>
                        <h2 class="section-title padding-top your_cart_title" style="margin-bottom:30px;">You have no items in the shopping cart</h2>
                        <button class="btn btn-primary" data-href="{{ url('category') }}">Continue Shopping<i class="ion-ios-arrow-thin-right"></i></button>
                    @endif
                </div> <!-- /.section-inner -->
            </div> <!-- /.container -->
        </section> <!-- /.section -->
    </div> <!-- /.wrapper -->
@endsection
