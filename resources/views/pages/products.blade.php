
@extends('layouts.internal')

@section('title', isset($categories) && !empty($categories) ? $prod['prod_title'] .' - '. $categories.' - ' : '')
@section('keywords', isset($categories) && !empty($categories) ? $prod['prod_title'] .' - '. $categories : '')
@section('description', isset($categories) && !empty($categories) ? $prod['prod_title'] .' - '. $categories : '')


@section('content')
<section class="productsection">
    <div class="container-fluid no-padding">

        <img class="bannerimage img-responsive" src="{{ URL('images/bannerdadus.jpg') }}" />

    </div>
    <div class="container mgtop50 ">

        <div class="col-md-12 ">
        <!--   <div class="col-md-6 equal_innerdiv" >
            <img src="images/newproductimages/dadu3.jpg" class="img-responsive imgshadow" style="    margin-top: -90px;">
          </div> -->
         <div class="col-sm-12 ">
            <div class="product_caption">
            <h2 class="top40 bottom10 text-uppercase" style="color: #000;text-align: center;">Product</h2>
            <span class="product_divider">
                <img src="{{ url('images/rsz_divider.png')}}">
              </span>
             </div>
            <div id="sres">

             <form method="POST" action="{{url('add_to_cart_submit')}}">
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="product text-center">
                <div class="col-sm-4 col-md-offset-4">
                 <div class="productdiv">
                  <div class="product-img-box">
                    <img src="{{ url('images/products/'.$prod['prod_image'])}}" alt="menu" class="img-responsive">
                  </div>
                  <div class="detail-box">
                    <div class="food-type veg"></div>
                    <h3>{{$prod['prod_title']}}</h3>
                    <div class="title_img"><img src="{{ url('images/rsz_divider.png')}}"></div>
                    <!-- <h3>{{$prod['prod_desc']}}</h3> -->
                    <div class="price">
                      {{-- <span class="amount">&#8377; {{$data->prod_price}}</span> --}}
                      @if($prod->prod_qty == 0)
                        <span class="add_cart">Only available in shop</span>
                      @else
                        <input type="number" name="product_qty" class="product_qty" value="1">
                        @if( count($prod->weight) > 0)
                          <div class="select_kg">
                          <select class="prod_weight" name="weight">
                              @foreach ($prod->weight as $wg)
                              <option value="{{ $wg->id }}">{{ $wg->weight }} for ₹{{ $wg->price }} </option>
                              @endforeach
                          </select>
                          </div>
                        @else
                        <div class="select_kg">
                          <select class="prod_weight" name="weight">
                              <option>Default weight</option>
                          </select>
                          </div>
                        @endif
                    @endif
                        <span class="add_cart"><button type="submit" class="button add_to_cart_button">Add to Cart</button></span>
                    </div>
                  </div>
                   </div>
                   <input type="hidden" name="product_id" value="{{$prod['id']}}">
                <span class="order">
                </span>
                </div>
              </div>
              </form>

            </div>
          </div>
      </div>

      {{-- <div class="row">
        <div class="ps-pagination text-center">
              <ul class="pagination">
                  <li><a href="#"><i class="fa fa-arrow-left"></i></a></li>
                  <li class="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#"><i class="fa fa-arrow-right"></i></a></li>
              </ul>
         </div>

      </div> --}}
    </div>
  </section>
@endsection
