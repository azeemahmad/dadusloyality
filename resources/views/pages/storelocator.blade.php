@extends('layouts.internal')
@section('content')

    <div class="wrapper faq">
        <section class="section static_page">
            <div class="inner_page_banner">
                <img src="images/storeloc/banner.jpg" class="visible-lg visible-md" alt="">
				<img src="images/storeloc/banner_mob.jpg" class="visible-xs visible-sm mobile-banner" alt="">>
            </div>
            <div class="container">
                <h2 class="section-title padding-top about_title text-center">Store Locator</h2>
                <span class="product_divider">
                    <img src="{{asset('images/rsz_divider.png')}}">

                </span>
                <div class="row">
                    <div class="col-md-12">
                        <div id="map" style="height:800px"></div>
                    </div>
                    </div>
                    <div class="store-locator-inner-main-wrap">
	<h4>Hyderabad</h4>
	<div class="row row-eq-height">
		<div class="col-sm-4 store-address-main-wrap">
			<div class="store-address-inner-wrap">
				<div class="location-icon-store-name">
					<img src="images/location.png" height="30px" width="30px" alt="">
					<strong>
            Dadu's Mithai Vatika<br>

					</strong>
				</div>
				<div class="address-wrap">
					<p>
						Liberty Rd, Opp TTD Temple,<br>
            Chandra Nagar, Himayatnagar, Hyderabad,<br>
            Telangana 500029
						<a href="https://www.google.com/maps/place/Dadu's+Mithai+Vatika/@17.4056994,78.4791321,15z/data=!4m2!3m1!1s0x0:0x95ec754fd3ed92?sa=X&ved=2ahUKEwiBnJDMwYTjAhWEto8KHdLOB84Q_BIwCnoECA0QCA" target="_blank">Directions</a>

					</p>
				</div>
			</div>
		</div>
		<div class="col-sm-4 store-address-main-wrap">
			<div class="store-address-inner-wrap">
				<div class="location-icon-store-name">
										<img src="images/location.png" height="30px" width="30px" alt="">
					<strong>
						Dadus Mithai Vatika<br>

					</strong>
				</div>
				<div class="address-wrap">
					<p>
						Plot No 1090,<br> Road No 36, Jubilee Hills,<br> Hyderabad - 500033, Near Peddamaa Temple
						<a href="https://www.google.com/maps/place/Dadu's+mithai+vatika/@17.4313505,78.4076757,15z/data=!4m2!3m1!1s0x0:0xf6b91a349382b790?sa=X&ved=2ahUKEwjst8GmvoTjAhWSinAKHSs3ClsQ_BIwCnoECA8QCA" target="_blank">Directions</a>
					</p>
				</div>
			</div>
		</div>
		<div class="col-sm-4 store-address-main-wrap">
			<div class="store-address-inner-wrap">
				<div class="location-icon-store-name">
										<img src="images/location.png" height="30px" width="30px" alt="">
					<strong>
						Dadus Mithai Vatika<br>

					</strong>
				</div>
				<div class="address-wrap">
					<p>
						Ground Floor, Owners Pride Building,<br> Road No 12, Banjara Hills,<br> Hyderabad - 500034, Beside Ratnadeep Complex
						<a href="https://www.google.com/maps/place/Dadu's+Mithai+Vatika/@17.4078679,78.4402706,17z/data=!3m1!4b1!4m5!3m4!1s0x3bcb973c9c5873a1:0x2940a491f0de08bf!8m2!3d17.4078679!4d78.4424646" target="_blank">Directions</a>

					</p>
				</div>
			</div>
		</div>

		<div class="col-sm-4 store-address-main-wrap">
			<div class="store-address-inner-wrap">
				<div class="location-icon-store-name">
				<img src="images/location.png" height="30px" width="30px" alt="">
					<strong>
						Dadu's Mithai Vatika<br>

					</strong>
				</div>
				<div class="address-wrap">
					<p>
						 Plot No. 6 & 7, Jade Arcade, M.G. Road,<br> Near Pradise Circle, Sandhu Apartment, Kalasiguda,<br> Secunderabad, Telangana 500003<br>


						<a href="https://www.google.com/maps/place/Dadu's+Mithai+Vatika/@17.4414789,78.4879151,15z/data=!4m5!3m4!1s0x0:0x4fbfcbf25b9c1602!8m2!3d17.4414789!4d78.4879151" target="_blank">Directions</a>
					</p>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-xs-12 store-address-main-wrap">
			<div class="store-address-inner-wrap">
				<div class="location-icon-store-name">
				<img src="images/location.png" height="30px" width="30px" alt="">
					<strong>
						Dadu's On My Way <br>

					</strong>
				</div>
				<div class="address-wrap">
					<p>
						Ameerpet Metro Station,<br> Nagarjuna Nagar colony, Yella Reddy Guda,<br> Hyderabad, Telangana 500073
						<a href="https://www.google.com/maps/place/Dadu's+On+My+Way/@17.4352315,78.444715,15z/data=!4m2!3m1!1s0x0:0x3aad1ae13d0ceccf?sa=X&ved=2ahUKEwi7kJbPwITjAhWBr48KHeRZAq0Q_BIwDnoECAsQCA" target="_blank">Directions</a>
					</p>
				</div>
			</div>
		</div>

		<div class="col-sm-4 col-xs-12 store-address-main-wrap">
			<div class="store-address-inner-wrap">
				<div class="location-icon-store-name">
										<img src="images/location.png" height="30px" width="30px" alt="">
					<strong>
						Dadu's mithai vatika

					</strong>
				</div>
				<div class="address-wrap">
					<p class="hidden-xs">
						PNR Empire Building,<br> Pillar No. A735, Sangeet Nagar,<br> Kukatpally, Hyderabad,<br> Telangana 500072<br>

						<a href="https://goo.gl/maps/WJ8J14vSjN6g9XXt6" target="_blank">Directions</a>
					</p>
          <p class="hidden-lg hidden-md">
						PNR Empire Building,<br> Pillar No. A735, Sangeet Nagar,<br> Kukatpally, Hyderabad,<br> Telangana 500072<br>

						<a href="https://goo.gl/maps/WJ8J14vSjN6g9XXt6" target="_blank">Directions</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>


            </div> <!-- /.section-inner -->
        </div> <!-- /.container -->
    </section> <!-- /.section -->
</div> <!-- /.wrapper -->
@endsection
@section('pagescript')
<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBd-CCI_8fvsAw7EDXZwuSmw2b811D9s5E" type="text/javascript"></script>
<script type="text/javascript">
   var locations = [
     ['Himayatnagar',17.4056994,78.4791321, 1],
     ['Jubilee hill', 17.4313505,78.405487, 2],
     ['Banjara hills', 17.4078679,78.4402759, 3],
     ['Secunderabad', 17.4415094,78.485716, 4],
     ['Ameerpet metro', 17.4352718,78.4424468, 5],
     ['Kukatpally', 17.4953825,78.3985423, 6]
   ];

   var map = new google.maps.Map(document.getElementById('map'), {
     zoom: 12,
     center: new google.maps.LatLng(17.4129601,78.468328),
     mapTypeId: google.maps.MapTypeId.ROADMAP
   });

   var infowindow = new google.maps.InfoWindow();

   var marker, i;

   for (i = 0; i < locations.length; i++) {
     marker = new google.maps.Marker({
       position: new google.maps.LatLng(locations[i][1], locations[i][2]),
       map: map
     });

     google.maps.event.addListener(marker, 'click', (function(marker, i) {
       return function() {
         infowindow.setContent(locations[i][0]);
         infowindow.open(map, marker);
       }
     })(marker, i));
   }
 </script>
@endsection
