@extends('layouts.internal')
@section('content')
		<div class="wrapper">
			<section class="section static_page">
				<div class="containersection full_container">
					<div class="section-inner">
						<div class="inner_page_banner">
							<img src="images/contact/banner.jpg" class="visible-lg visible-md" alt="">
							<img src="images/contact/banner_mob.jpg" class="visible-xs visible-sm mobile-banner" alt="">
						</div>
						<div class="container">
							<h2 class="section-title padding-top about_title text-center">Contact Us</h2>

							<span class="product_divider">
				                <img src="{{asset('images/rsz_divider.png')}}">
				              </span>
				            <div class="row">
				            {{-- <div class="col-md-6 col-xs-12 col-lg-6">
				              <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d121810.95652367758!2d78.3376352!3d17.4313383!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1534863667040" width="550" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

				            </div> --}}
                            <div class="col-md-8 col-xs-12 col-lg-8 col-md-offset-2 col-lg-offset-2">
                            <div class="form-effect text-left default up-page" style="display: block;" id="contact_us_4">
                            <div class="page-container default-up-layout">
							<div class="relative-pos grid up-form">
							   <div class="form-container row">
							      <div class="form-detail">
							         <div class="header-container">
							            <div class="text-center"></div>
							            <div class="header-name">Contact Us</div>



								<!-- @if(Session::has('success'))
								    <div class="alert alert-success">
								      {{ Session::get('success') }}
								    </div>
								@endif -->
							         </div>
							         <div class="form-data-container">
							            <div class="form-footer"></div>
							            @if(session()->has('message'))
    										<div class="alert alert-success">
       										 {{ session()->get('message') }}
    										</div>
										@endif
							            <form action="{{url('contact_submit')}}" method="post" class="contact_form" enctype="multipart/form-data">
							            	{{ csrf_field() }}
							               <input name="name" type="text" placeholder="Name">
							               	@if ($errors->has('name'))
    										<div class="error">{{ $errors->first('name') }}</div>
											@endif
							               <input name="phone" type="text" placeholder="Mobile">
							               @if ($errors->has('phone'))
    										<div class="error">{{ $errors->first('phone') }}</div>
											@endif
							               <input name="email" type="email" placeholder="Email">
							                @if ($errors->has('email'))
    										<div class="error">{{ $errors->first('email') }}</div>
											@endif
							               <textarea name="message" placeholder="Write your message..."></textarea>
							               @if ($errors->has('message'))
    										<div class="error">{{ $errors->first('message') }}</div>
											@endif
							               
							               <input type="submit" value="submit">
										  	<div class="text-center">
										   		<span class="tbl-clr">Or</span><br>
										   		<span class="tbl-clr">Call: +91 9000810182</span>
									   		</div>
							            </form>

							            
							         </div>
							      </div>
							   </div>
</div>
</div></div></div></div>

						</div> <!-- /.section-body -->


						</div>

					</div> <!-- /.section-inner -->
			</section> <!-- /.section -->
		</div> <!-- /.wrapper -->

@endsection
