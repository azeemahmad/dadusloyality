@extends('layouts.internal')
@section('content')

		<div class="wrapper checkoutpage">
			<section class="section static_page">
				<div class="containersection full_container">
					<div class="section-inner">
						<div class="inner_page_banner">
							<img src="images/about/banner.jpg" alt="">
						</div>
						<div class="container">
							<h2 class="section-title padding-top about_title text-center">Privacy Policy</h2>

							<span class="product_divider">
				                <img src="{{ URL::asset('images/rsz_divider.png') }}">

				              </span>

							<div class="col-md-6 text-center">
								<img src="images/about/laddu.png" alt="">
							</div>
							<div class="col-md-6">
								<p>
									Dadu’s is a name which, over a period of decades, has become synonymous with happiness. The seeds of this revolutionary brand were sown by the visionary Mr. Rajesh Dadu, back in 1993, and its branches are still growing strong, chasing new skies. The journey which began with an 800 sq ft shop with only 2 chefs is now 400+ employees strong and has a presence across the nation supplying sweets to the who’s who of the Indian industry.
								</p>
								<p>
									Dadu’s Mithai Vatika is a leading and trusted name in the traditional Indian sweets market. With roots in Hyderabad, we have entranced Indians all over with our delectable taste. From over a million individual customers to a number of corporate giants, everyone vouches for our delicious taste, high quality and pristine purity.<strong> All our customers are united in favour of our taste and service, that’s the reason we say ‘Dadu’s ka swaad, bandhe sabka saath’. Over the time we have won several awards, certificates and have also been appreciated by the Hon. President of India.</strong>
								</p>
								<p>
									Most traditional sweet businesses have the ability to produce only about 40 varieties of sweets with very less innovation. But, with a focus on the changing demand of the customer, we have, till now, invented over 200 varieties of sweets offering new tastes to customers. This customer centric approach has played a very important part in our success. We believe that a brand that keeps innovating never grows old in the minds of the customer. This has helped in repeat footfalls from regular customers, strengthening brand loyalty, and also attracts the newer generation. Going beyond the traditional Indian sweets, we have also made a successful foray into confectioneries too. We have a no-compromise attitude when it comes to Taste, Health, Quality, Hygiene and true Value for Money.
								</p>
								<p>Adapting to the internet era, our sweets now can also be booked online and to make life easier, we also have a mobile application through which customers can place orders for their favourite Dadu’s Sweets.  You can also follow us on social media platforms like Facebook & Instagram. In the coming years, our plan is to replicate global flavours with a local twist.</p>
								<div class="text-right kaju_bottom">
									<img src="images/about/kaju.png" alt="">
								</div>
							</div>
						</div> <!-- /.section-body -->

						<div class="container-fluid red_block">
							<div class="container full-width no-padding">
								<ul class="nav nav-tabs">
						            <li class="active col-md-4">
						            	<a data-toggle="tab" href="#ourhistory">
											<div class="col-md-12 text-center">
												<img src="images/about/icon_1.png" alt="">
												<h3>Our History</h3>
												<p>
													We have been making sweets <br>since 1993 to add more <br>sweetness to your celebrations
												</p>
											</div>
										</a>
									</li>

									<li class="col-md-4">
										<a data-toggle="tab" href="#testimonials">
											<div class="col-md-12 text-center">
												<img src="images/about/icon_2.png" alt="">
												<h3>Testimonials</h3>
												<p>
													We use the best quality<br>ingredients in our products.
												</p>
											</div>
										</a>
									</li>

									<li class="col-md-4">
										<a data-toggle="tab" href="#ourworkforce">
											<div class="col-md-12 text-center">
												<img src="images/about/icon_3.png" alt="">
												<h3>Our Work Force</h3>
												<p>
													We follow 100%<br>fair trade practices.
												</p>
											</div>
										</a>
									</li>
								</ul>



								<div class="tab-content clearfix">
								  <div class="tab-pane active" id="ourhistory">
								  	<h2 class="section-title padding-top about_title text-center">History</h2>
							          <div class="col-md-12 text-center">
											<!--<div class="top_text">
												<img src="images/about/text.png" alt="">
											</div>-->
											<p>
												Mr. Rajesh Dadu spent his initial sweet making days in Bengaluru. After a considerable amount of time he moved to Hyderabad to set up the first Dadu’s store at Himayatnagar in 1993.
												<strong>With a mere capital of Rs 30,000, expertise in the business, a dream to win the heart of nation, and with the blessings of his mother, Dadu began this journey of sweetness.</strong>
											</p>
										</div>
										<div class="history_block">
											<div class="col-md-6">
												<img src="images/about/history.png" alt="">
											</div>
											<div class="col-md-6">
												<p>
													The path, however, wasn’t without hardships. Lack of funds was a major barrier which did not allow Mr. Dadu to invest in more than two employees or equip his kitchen with top-class machinery. He has also spent several months sleeping on the pavement outside his shop just to make sure his customer gets fresh, hot samosas in the morning. Obstacles couldn’t stop Mr. Rajesh Dadu from chasing his vision. In fact, he considered each one of them as a lesson and reinvented himself with it. Most of the profits during the initial years were invested in modernisation and innovation. A decade later, in 2003, our second outlet was established at Secundarabad, followed by the third in 2013 at Jubilee Hills and fourth in 2015 at Banjara Hills. We are all set to open our fifth and sixth outlet at Kukatpally and Kondapur respectively.  Through  ‘Dadu’s Mithai Vatika’s’ successful business model, Mr. Rajesh Dadu has showed the world that one does not need degrees from top business schools to run a successful business, but common sense to listen to what the customer wants and deliver it better than expected. Dadu’s evolution still continues.
												</p>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="testimonials">
										<h2 class="section-title padding-top about_title text-center">Testimonials</h2>

										<div class="owl-carousel owl-theme">
										    <div class="img-wrap">
										        <div class="col-md-12 testimonial-block ">
									                <h3>Anshika Anshu</h3>
									                <p>Amazing chocolates. Really delicious and cute shaped. Chocolates are so yum & fresh too. They are really soft. They have good options for gifting as well.</p>
									                <img src="images/dummy-pic.png">
									              </div>
										    </div>
										     <!-- /.img-wrap -->
										    <div class="img-wrap">
										        <div class="col-md-12 testimonial-block ">
									                <h3>Anshika Anshu</h3>
									                <p>Amazing chocolates. Really delicious and cute shaped. Chocolates are so yum & fresh too. They are really soft. They have good options for gifting as well.</p>
									                <img src="images/dummy-pic.png">
									              </div>
										    </div>
										    <div class="img-wrap">
										        <div class="col-md-12 testimonial-block ">
									                <h3>Anshika Anshu</h3>
									                <p>Amazing chocolates. Really delicious and cute shaped. Chocolates are so yum & fresh too. They are really soft. They have good options for gifting as well.</p>
									                <img src="images/dummy-pic.png">
									              </div>
										     </div>
										     <!-- /.img-wrap -->
										    <div class="img-wrap">
										        <div class="col-md-12 testimonial-block ">
									                <h3>Anshika Anshu</h3>
									                <p>Amazing chocolates. Really delicious and cute shaped. Chocolates are so yum & fresh too. They are really soft. They have good options for gifting as well.</p>
									                <img src="images/dummy-pic.png">
									              </div>
										    </div>
										    <div class="img-wrap">
										        <div class="col-md-12 testimonial-block ">
									                <h3>Anshika Anshu</h3>
									                <p>Amazing chocolates. Really delicious and cute shaped. Chocolates are so yum & fresh too. They are really soft. They have good options for gifting as well.</p>
									                <img src="images/dummy-pic.png">
									              </div>
										     </div>
										     <!-- /.img-wrap -->
										    <div class="img-wrap">
										        <div class="col-md-12 testimonial-block ">
									                <h3>Anshika Anshu</h3>
									                <p>Amazing chocolates. Really delicious and cute shaped. Chocolates are so yum & fresh too. They are really soft. They have good options for gifting as well.</p>
									                <img src="images/dummy-pic.png">
									              </div>
										    </div>
										    <div class="img-wrap">
										        <div class="col-md-12 testimonial-block ">
									                <h3>Anshika Anshu</h3>
									                <p>Amazing chocolates. Really delicious and cute shaped. Chocolates are so yum & fresh too. They are really soft. They have good options for gifting as well.</p>
									                <img src="images/dummy-pic.png">
									              </div>
										     </div>
										     <!-- /.img-wrap -->
										    <div class="img-wrap">
										        <div class="col-md-12 testimonial-block ">
									                <h3>Anshika Anshu</h3>
									                <p>Amazing chocolates. Really delicious and cute shaped. Chocolates are so yum & fresh too. They are really soft. They have good options for gifting as well.</p>
									                <img src="images/dummy-pic.png">
									              </div>
										    </div>
										    <div class="img-wrap">
										        <div class="col-md-12 testimonial-block ">
									                <h3>Anshika Anshu</h3>
									                <p>Amazing chocolates. Really delicious and cute shaped. Chocolates are so yum & fresh too. They are really soft. They have good options for gifting as well.</p>
									                <img src="images/dummy-pic.png">
									              </div>
										     </div>
										     <!-- /.img-wrap -->
										    <div class="img-wrap">
										        <div class="col-md-12 testimonial-block ">
									                <h3>Anshika Anshu</h3>
									                <p>Amazing chocolates. Really delicious and cute shaped. Chocolates are so yum & fresh too. They are really soft. They have good options for gifting as well.</p>
									                <img src="images/dummy-pic.png">
									              </div>
										    </div>
										    <div class="img-wrap">
										        <div class="col-md-12 testimonial-block ">
									                <h3>Anshika Anshu</h3>
									                <p>Amazing chocolates. Really delicious and cute shaped. Chocolates are so yum & fresh too. They are really soft. They have good options for gifting as well.</p>
									                <img src="images/dummy-pic.png">
									              </div>
										     </div>
										     <!-- /.img-wrap -->
										    <div class="img-wrap">
										        <div class="col-md-12 testimonial-block ">
									                <h3>Anshika Anshu</h3>
									                <p>Amazing chocolates. Really delicious and cute shaped. Chocolates are so yum & fresh too. They are really soft. They have good options for gifting as well.</p>
									                <img src="images/dummy-pic.png">
									              </div>
										    </div>
										</div>





									</div>
							        <div class="tab-pane" id="ourworkforce">
							        	<h2 class="section-title padding-top about_title text-center">Our Work Force</h2>
							        	<div class="col-md-12 text-center">
							          		<p>Handling a centralized kitchen spanning 50,000 Sq. Ft., 4 flagship stores, and millions of happy customers requires a bandwagon. The Dadu family forms the core of the team. Today we have more than 400 employees who are well trained in each and every department. Every employee at Dadu’s puts their heart and soul into delivering trust in the form of sweets.</p>
											<p>A customer’s safety is our concern. Hence, hygiene is one of the most important factors when it comes to the making, handling or delivery of anything manufactured by us.</p>
											<p>All the raw materials are lab tested before procurement. Periodic pest controls are carried out to ensure all our premises are rodent free. Our chef’s also take utmost precautions while making sweets.All sweets during the festive season are packed in special food grade boxes to increase the shelf life. All the guidelines of FSSAI are thoroughly followed by each and every person who works for us.
											</p>
							          	</div>
									</div>
								</div>
					        </div>


							</div>
						</div>

						<!--<div class="container">
							<h2 class="section-title padding-top about_title text-center">History</h2>
							<div class="col-md-12 text-center">

								<p>
									Mr. Rajesh Dadu spent his initial sweet making days in Bengaluru. After a considerable amount of time he moved to Hyderabad to set up the first Dadu’s store at Himayatnagar in 1993.
									<strong>With a mere capital of Rs 30,000, expertise in the business, a dream to win the heart of nation, and with the blessings of his mother, Dadu began this journey of sweetness.</strong>
								</p>
							</div>
							<div class="history_block">
								<div class="col-md-6">
									<img src="images/about/history.png" alt="">
								</div>
								<div class="col-md-6">
									<p>
										The path, however, wasn’t without hardships. Lack of funds was a major barrier which did not allow Mr. Dadu to invest in more than two employees or equip his kitchen with top-class machinery. He has also spent several months sleeping on the pavement outside his shop just to make sure his customer gets fresh, hot samosas in the morning. Obstacles couldn’t stop Mr. Rajesh Dadu from chasing his vision. In fact, he considered each one of them as a lesson and reinvented himself with it. Most of the profits during the initial years were invested in modernisation and innovation. A decade later, in 2003, our second outlet was established at Secundarabad, followed by the third in 2013 at Jubilee Hills and fourth in 2015 at Banjara Hills. We are all set to open our fifth and sixth outlet at Kukatpally and Kondapur respectively.  Through  ‘Dadu’s Mithai Vatika’s’ successful business model, Mr. Rajesh Dadu has showed the world that one does not need degrees from top business schools to run a successful business, but common sense to listen to what the customer wants and deliver it better than expected. Dadu’s evolution still continues.
									</p>
								</div>
							</div>
						</div>-->








					</div> <!-- /.section-inner -->
				</div> <!-- /.container -->
			</section> <!-- /.section -->
		</div> <!-- /.wrapper -->

		<span class="chop">
			<img src="images/about/chop.png" alt="">
		</span>
		<span class="cap">
			<img src="images/about/cap.png" alt="">
		</span>



@endsection
