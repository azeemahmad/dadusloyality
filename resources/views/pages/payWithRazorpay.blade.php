@extends('layouts.internal')

@section('content')
<style media="screen">
.razorpay_block {
/* min-height: 500px; */
height: 64vh;
position: relative;
}
.razorpay_inner{
    position: absolute;
    top: 50%;
    left: 50%;
    transform:translate(-50%, -50%);
}
</style>
<div class="razorpay_block">
    <div class="razorpay_inner">
        <button id="rzp-button1" class="btn btn-success pull-right">Please Clicke here to Proceed the Payment by Razorpay</button>
        <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
        <form name='razorpayform' action="{{url('/razorpaypayment')}}" method="POST">
          {{csrf_field()}}
            <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id">
            <input type="hidden" name="razorpay_signature"  id="razorpay_signature" >
            <input type="hidden" name="payment" value='razorpay'>
            <input type="hidden" name="order_id" value='{{ $order_id }}'>
            <input type="hidden" name="address_id" value='{{ $customer_address_id }}'>
        </form>
    </div>
</div>
<script>
var options = <?php echo $json;?>;
// console.log(options);
options.handler = function (response){
    document.getElementById('razorpay_payment_id').value = response.razorpay_payment_id;
    document.getElementById('razorpay_signature').value = response.razorpay_signature;
    document.razorpayform.submit();
};
options.theme.image_padding = false;
options.modal = {
    ondismiss: function() {
        console.log("This code runs when the popup is closed");
    },
    escape: true,
    backdropclose: false
};
var rzp = new Razorpay(options);
document.getElementById('rzp-button1').onclick = function(e){
    rzp.open();
    e.preventDefault();
}
</script>

   <script type = "text/javascript">  
    window.onload = function () {  
        document.onkeydown = function (e) {  
            return (e.which || e.keyCode) != 116;  
        };  
    }  
</script>  

@endsection
