@extends('layouts.internal')
@section('content')

	<div class="navbar navbar-secondary">
		<div class="steps">
			<div class="steps-inner">
				<div class="step-item">
					<div class="step-count">1</div>
					<div class="step-label">Your Items</div>
				</div> <!-- /.item-step-item -->
				<div class="step-item">
					<div class="step-count">2</div>
					<div class="step-label">Information</div>
				</div> <!-- /.step-item -->
				<div class="step-item active">
					<div class="step-count">3</div>
					<div class="step-label">Shipping</div>
				</div> <!-- /.step-item -->
				<div class="step-item">
					<div class="step-count">4</div>
					<div class="step-label">Payment</div>
				</div> <!-- /.step-item -->
			</div>
		</div> <!-- /.steps -->
	</div>

	<section class="section checkoutpage">
		<div class="container section">
			<div class="section-inner">
				<div class="">
					<div class="">
						<div class="section-nav back" data-href="cart">
							<i class="ion-android-arrow-back"></i>
							<div>Back</div>
						</div> <!-- /.section-nav.back -->
					</div> <!-- /.section-nav-group -->
				</div>
			</div>
			@if (session('del_msg'))
	   			<div class="alert alert-danger" role="alert">
	   		   		{{ session('del_msg') }}
	      		</div>
	   	   @endif
			<h2 class="prod-title">Your Addresses</h2>
			<div class="title_img"><img src="{{ URL::asset('images/rsz_divider.png') }}"></div>

			<div class="row">
				@if(!empty($user_data->customeraddresses))
					@foreach ($user_data->customeraddresses as $address)

						<div class="col-md-4 col-xs6">
							<div class="card">
								<div class="card-image">
									<h4 class="card-title text-center">{{ title_case($address->name) }}</h4>
								</div>

								<div class="card-content">
									<p>{{ strtoupper($address->address) }} <br>{{ strtoupper($address->state) }} <br>{{ strtoupper($address->city) }} <br>{{ strtoupper($address->pincode) }} <br>{{ strtoupper($address->mobile_number)  }} </p>
								</div>

								<div class="card-action">
									<a href="{{ url('checkoutstep4') }}/{{ base64_encode( $address->id) }}">Select Address & Proceed</a>
								</div>
							</div>
						</div>
					@endforeach
				@else
					<div> No Address </div>
				@endif
			</div>
		</div>
	</section>

	<section class="section checkoutpage step-3">
		<div class="container section">
			<div class="section-inner">
				<form action="javascript:void(0);" role="form" method="POST" id="save_address" novalidate="novalidate">
					<div class="section-body">
						<h2 class="prod-title padding-top">Add New Address</h2>
						<div class="title_img"><img src="images/rsz_divider.png"></div>
						<div class="account-form">

							<div class="divider"></div>
							<!-- <div class="section-subtitle" id="shipping-address">Add New Address</div> -->
							<div class="row">
								<div class="col-md-12">

									<div class="row">
										<div class="col-md-12 col-sm-12">
											<div class="form-group">
												<label>Contact Person Name<span class="required"></span></label>
												<input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
											</div> <!-- /.form-group -->
										</div> <!-- /.col-md-6 -->
									</div> <!-- /.row -->
									<div class="form-group">
										<label>Address<span class="required"></span></label>
										<input type="text" name="address" id="address" class="form-control" value="{{ old('address') }}">
									</div> <!-- /.form-group -->
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<div class="form-group">
												<label>State <span class="required"></span></label>
												<input type="text" name="state" id="state" class="form-control" value="{{ old('state') }}">
											</div> <!-- /.form-group -->
										</div> <!-- /.col-md-6 -->
										<div class="col-md-6 col-sm-6">
											<div class="form-group">
												<label>City <span class="required"></span></label>
												<input type="text" name="city" id="city" class="form-control" value="{{ old('city') }}">
											</div> <!-- /.form-group -->
										</div> <!-- /.col-md-6 -->
									</div> <!-- /.row -->
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<div class="form-group">
												<label>Pin Code <span class="required"></span></label>
												<input type="text" name="pincode" id="pincode" class="form-control number" value="{{ old('pincode') }}">
											</div> <!-- /.form-group -->
										</div> <!-- /.col-md-6 -->
										<div class="col-md-6 col-sm-6">
											<div class="form-group">
												<label>Contact Number <span class="required"></span></label>
												<input type="text" name="mobile_number" id="mobile_number" class="form-control phone" value="{{ old('mobile_number') }}">
											</div> <!-- /.form-group -->
										</div> <!-- /.col-md-6 -->
									</div> <!-- /.row -->
									<span class="label label-danger shippingerror"></span>
								</div> <!-- /.col-md-12 -->
							</div> <!-- /.row -->

						</div> <!-- /.account-form -->
					</div> <!-- /.section-body -->

					<div class="section-cta">
						<p>After this step you will fill in the payment information</p>
						<input type="submit" class="btn btn-primary" value="Add Address & Proceed">
					</div> <!-- /.section-cta -->
					</form>

			</div> <!-- /.section-inner -->
		</div> <!-- /.container -->
	</section> <!-- /.section -->

@endsection
@section('pagestyle')
	<style>
	.card {
		margin-top: 10px;
	    box-sizing: border-box;
	    border-radius: 2px;
	    background-clip: padding-box;
	    background-color: #c1c1c1;
	    border: dashed 1px #000;
	}
	.card span.card-title {
		color: #000;
		font-size: 24px;
		font-weight: 300;
		text-transform: uppercase;
	}

	.card .card-image {
		position: relative;
		overflow: hidden;
	}
	.card .card-image img {
		border-radius: 2px 2px 0 0;
		background-clip: padding-box;
		position: relative;
		z-index: 1;
	}
	.card .card-image span.card-title {
		bottom: 0;
		left: 0;
		padding: 16px;
		color: #000;
		text-align: center;
	}
	.card .card-content {
		padding: 16px;
		border-radius: 0 0 2px 2px;
		background-clip: padding-box;
		box-sizing: border-box;
		min-height: 160px;
	}
	.card .card-content p {
		margin: 0;
		color: inherit;
		color: #000;
	}
	.card .card-content span.card-title {
		line-height: 48px;
	}
	.card .card-action {
		border-top: 1px solid rgba(160, 160, 160, 0.2);
		padding: 16px;
	}
	.card .card-action a {
		color: #000;
		margin-right: 16px;
		transition: color 0.3s ease;
		text-transform: uppercase;
	}
	.card .card-action a:hover {
		color: #ffd8a6;
		text-decoration: none;
	}
	</style>
@endsection
