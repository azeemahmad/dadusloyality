 {{-- {{ dd( ($category)) }}  --}}
@extends('layouts.internal')
@section('title', isset($category->metatitle) && !empty($category->metatitle) ? $category->metatitle : '')
@section('keywords', isset($category->keywords) && !empty($category->keywords) ? $category->keywords : '')
@section('description', isset($category->description) && !empty($category->description) ? $category->description : '')

@section('content')
<section class="productsection">
    <div class="container-fluid no-padding">
      @if(!empty($category))
        <img class="bannerimage img-responsive hidden-xs hidden-sm" src={{ $category->category_banner != '' ?  URL('images/banner/'.$category->category_banner) : URL('images/bannerdadus.jpg') }} />
        <img class="bannerimage img-responsive hidden-md hidden-lg" src={{ $category->category_mobile_banner != '' ?  URL('images/banner/'.$category->category_mobile_banner) : URL('images/bannerdadus.jpg') }} />
      @else
        <img class="bannerimage img-responsive" src="{{ URL('images/bannerdadus.jpg') }}" />
      @endif
    </div>
    <div class="container mgtop50 ">
        <!--   <div class="col-md-6 equal_innerdiv" >
            <img src="images/newproductimages/dadu3.jpg" class="img-responsive imgshadow" style="    margin-top: -90px;">
          </div> -->

         <div class="col-sm-12 ">
            <div class="product_caption">
            <h2 class="top40 bottom10 text-uppercase" style="color: #000;text-align: center;">Products</h2>
            <span class="product_divider">
                <img src="{{ URL::asset('images/rsz_divider.png') }}">
              </span>
             </div>
            <div id="sres">

            @if( isset($products) && !empty($products) && $products->first() != null )
            @foreach($products as $key => $data)
             <form method="POST" action="{{url('add_to_cart_submit')}}">
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
              @if($data->prod_qty != 0)<a href="{{url('product/details/'.$data->product_slug)}}"><div class="col-sm-4 product">
                 <div class="productdiv">
                  <div class="product-img-box">
                    <img src="{{URL::asset('images/products/'.$data->prod_image)}}"
                     alt="menu" class="img-responsive">
                  </div>
                  <div class="detail-box">
                    <div class="food-type veg"></div>
                    <a href="{{url('product/details/'.$data->product_slug)}}"><h3>{{$data->prod_title}}</h3></a>
                    <div class="title_img"><img src="{{ url('images/rsz_divider.png') }}"></div>
                    <div class="price">
                      @if($data->prod_qty == 0)
                        <span class="out_of_stock"><button>Only available in shop</button></span>
                      @else
                        <input type="number" name="product_qty" class="product_qty" value="1">
                        @if( $data->weight->first()!= null )
                        <div class="select_kg">
                          <select class="prod_weight" name="weight">
                              @foreach ($data->weight as $wg)
                              <option value="{{ $wg->id }}">{{ $wg->weight }} for ₹{{ $wg->price }} </option>
                              @endforeach
                          </select>
                        </div>
                        @else
                          <select class="prod_weight" name="weight">
                              <option>Default weight</option>
                          </select>
                        @endif
                        <span class="add_cart"><button type="submit" class="button add_to_cart_button">Add to Cart</button></span>
                        @endif
                    </div>
                  </div>
                   </div>
                   <input type="hidden" name="product_id" value="{{$data->id}}">
                <span class="order">
                </span>
              </div></a>
            @else
              <div class="col-sm-4 product">
                 <div class="productdiv">
                  <div class="product-img-box">
                    <img src="{{URL::asset('images/products/'.$data->prod_image)}}"
                     alt="menu" class="img-responsive">
                  </div>
                  <div class="detail-box">
                    <div class="food-type veg"></div>
                    <a href="{{url('product/details/'.$data->product_slug)}}"><h3>{{$data->prod_title}}</h3></a>
                    <div class="title_img"><img src="{{ url('images/rsz_divider.png') }}"></div>

                    <div class="price">
                      @if($data->prod_qty == 0)
                        <span class="out_of_stock"><button>Only available in shop</button></span>
                      @else
                        <input type="number" name="product_qty" class="product_qty" value="1">
                        @if( $data->weight->first()!= null )
                        <div class="select_kg">
                          <select class="prod_weight" name="weight">
                              @foreach ($data->weight as $wg)
                              <option value="{{ $wg->id }}">{{ $wg->weight }} for ₹{{ $wg->price }} </option>
                              @endforeach
                          </select>
                        </div>
                        @else
                          <select class="prod_weight" name="weight">
                              <option>Default weight</option>
                          </select>
                        @endif
                        <span class="add_cart"><button type="submit" class="button add_to_cart_button">Add to Cart</button></span>
                        @endif
                    </div>
                  </div>
                   </div>
                   <input type="hidden" name="product_id" value="{{$data->id}}">
                <span class="order">
                </span>
              </div>
            @endif
              </form>
            @endforeach
            @else
                <h5 class="no-prod">No Product</h5>
            @endif
            </div>
          </div>
    </div>
  </section>
@endsection
