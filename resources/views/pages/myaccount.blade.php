@extends('layouts.internal')
@section('content')
    <style>
        .reeadmepoints-text{
            font-size: 18px;
            color:#dc343c;
        }
    </style>

    <div class="wrapper myaccount">
        <section class="section static_page">
            <div class="inner_page_banner">
                <img src="images/myaccount/banner.jpg" class="visible-lg visible-md" alt="">
                <img src="images/myaccount/banner_mob.jpg" class="visible-xs visible-sm" alt="">>
            </div>
            <div class="container">
                <h2 class="section-title padding-top about_title text-center">MY Account</h2>
				<span class="product_divider">
					<img src="{{ URL::asset('images/rsz_divider.png') }}">

				</span>
                @if (session('msg'))
                {{ session('msg') }}
                @endif
                        <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab"
                                                              data-toggle="tab">My Profile</a></li>
                    <li role="presentation"><a href="#orders" aria-controls="orders" role="tab" data-toggle="tab">My
                            Orders</a></li>
                    <li role="presentation"><a href="#readme" aria-controls="orders" role="tab" data-toggle="tab">My
                            Readme Points</a></li>
                    {{-- <li role="presentation"><a href="#address" aria-controls="address" role="tab" data-toggle="tab">Address Book</a></li> --}}
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="profile">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{ url('update_profile') }}" role="form" method="POST"
                                      novalidate="novalidate">
                                    {{ csrf_field() }}
                                    <div class="form-group tbl-clr">
                                        <label>Your Name</label>
                                        <input type="text" name="name" id="registername"
                                               value="{{ $user_order_details->name }}" class="form-control">
                                        <span class="alert-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                    <div class="form-group tbl-clr">
                                        <label>Mobile Number</label>
                                        <input type="number" name="mobile" id="registermobile"
                                               value="{{ $user_order_details->mobile }}" class="form-control" readonly>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group tbl-clr">
                                        <label>Email Id</label>
                                        <input type="emailid" name="email" id="registeremail"
                                               value="{{ $user_order_details->email }}" class="form-control">
                                        <span class="alert-danger">{{ $errors->first('email') }}</span>
                                    </div>
                                    <!-- /.form-group -->

                                    <div class="form-group tbl-clr">
                                        <label>Date Of Birth</label>
                                        <input type="dateofbirth" name="dob" id="dob"
                                               value="{{ date( "Y-m-d", strtotime($user_order_details->dob)) }}"
                                               class="form-control datepicker">
                                        <span class="alert-danger">{{ $errors->first('dob') }}</span>
                                    </div>
                                    <!-- /.form-group -->

                                    <div class="form-group text-right">
                                        <input type="submit" class="btn btn-primary registeruser" value="Update">
                                    </div>
                                    <!-- /.form-group.text-right -->
                                    <span class="label label-danger error"></span>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="orders">
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                {{-- dd($user_order_details->orders) --}}
                                @if(!empty($user_order_details->orders->first()))

                                    <table class="table tbl-clr">
                                        <thead>
                                        <tr>
                                            <th>Order#</th>
                                            <th>Order Date/Time</th>
                                            <th>Order Total</th>
                                            <th>Order Status</th>
                                            <th>Cancel Request</th>
                                            <th>View Invoice</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($user_order_details->orders as $order )
                                            <tr>
                                                <th>{{ $order->id }}</th>
                                                <th>{{ date( 'Y-m-d h:i:s A', strtotime($order->created_at)) }}</th>
                                                <th>{{ $order->grand_total }}</th>
                                                <th>{{ $order->status->title }}</th>
                                                <th>
                                                    <button type="button" @if( $order->status->id != 2) disabled
                                                            @endif data-oid="{{ $order->id }}"
                                                            class="btn btn-link btn-cle">Cancel This Order
                                                    </button>
                                                </th>
                                                <th><a target="_blank"
                                                       href="{{ url('invoice').'/'.base64_encode($order->id) }}">View
                                                        Invoice</a></th>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <span> No Orders </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="readme">
                        <?php
                        $endpoint = 'http://mqst.mloyalpos.com/Service.svc/GET_CUSTOMER_TRANS_INFO';
                        $data = [
                                "objClass" => [
                                        "customer_mobile" => $user_order_details->mobile
                                ]
                        ];
                        $result = app('App\Http\Controllers\Controller')->curl_post($endpoint, $data);
                        $customerdata = json_decode($result, true);
                        if ($customerdata['GET_CUSTOMER_TRANS_INFOResult']['Success'] == true) {
                            $customerdetail = $customerdata['GET_CUSTOMER_TRANS_INFOResult']['output']['response'];
                            $customerdetails = json_decode($customerdetail, true);
                            $personaldetails = $customerdetails['CUSTOMER_DETAILS'][0];
                            $pointhistory = $customerdetails['EARN_BURN_HISTORY'];
                            $coupoHistory = $customerdetails['COUPON_HISTORY'];
                        }
                        ?>
                        <div class="row">
                            @if(isset($personaldetails) && $personaldetails != null)
                            <div class="tbl-clr">
                                <div class="col-sm-4">
                                <p> <span class="reeadmepoints-text">Name  :  </span>{{$personaldetails['Name']}}</p>
                                <p><span class="reeadmepoints-text">Email  :  </span>{{$personaldetails['EmailId']}}</p>
                                <p><span class="reeadmepoints-text">Mobile  :  </span>{{$user_order_details->mobile}}</p>
                                <p><span class="reeadmepoints-text">LastVisit  :  </span>{{$personaldetails['LastVisit']}}</p>
                                </div>
                               <div class="col-sm-4">
                                   <p><span class="reeadmepoints-text">Remaining Loyality Points  :  </span>{{$personaldetails['LoyalityPoints']}}</p>
                                   <p><span class="reeadmepoints-text">Loyality Points Value  :  </span>{{$personaldetails['LoyalityPointsValue']}}</p>
                                   <p><span class="reeadmepoints-text">Total Bonus points  :  </span>{{$personaldetails['BonusPoints']}}</p>
                                   <p><span class="reeadmepoints-text">Point Per Value  :  </span>{{$personaldetails['Point_Per_Value']}}</p>
                                </div>
                                <div class="col-sm-4">
                                    <p><span class="reeadmepoints-text">Point Per Value  :  </span>{{$personaldetails['Point_Per_Value']}}</p>
                                    <p><span class="reeadmepoints-text">Total Earning Points  :  </span> {{$personaldetails['EarningPoints']}}</p>
                                    <p><span class="reeadmepoints-text">Total Burning Points  :  </span>{{$personaldetails['BurningPoints']}}</p>
                                    <p><span class="reeadmepoints-text">Total PurchaseAmount  :  </span>{{$personaldetails['PurchaseAmount']}}</p>
                                </div>
                            </div>
                            @endif
                             @if(isset($pointhistory) && !empty($pointhistory) && $pointhistory != null)
                            <div class="col-md-12 table-responsive">
                                <br>
                                <h3 class="tbl-clr">Loyality Points History</h3>
                                <table class="table tbl-clr">
                                    <thead>
                                    <tr>
                                        <th>Sr.no</th>
                                        <th>Store Code</th>
                                        <th>Store Id</th>
                                        <th>Store Name</th>
                                        <th>Detail</th>
                                        <th>Order ID</th>
                                        <th>Bill Date</th>
                                        <th>Points</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                     @foreach($pointhistory as $k => $v)
                                    <tr>
                                        <td>{{++$k}}</td>
                                        <td>{{$v['StoreCode']}}</td>
                                        <td>{{$v['StoreId']}}</td>
                                        <td>{{$v['StoreName']}}</td>
                                        <td>{{$v['Detail']}}</td>
                                        <td>{{$v['BillNo']}}</td>
                                        <td>{{date('d F Y',strtotime($v['BillDate']))}}</td>
                                        @if($v['Detail']=='Burning' &&  $v['Points'] > 0)
                                        <td style="color: red">{{$v['Points']}}</td>
                                        @else
                                        <td style="color: green">{{$v['Points']}}</td>
                                        @endif
                                    </tr>
                                     @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
