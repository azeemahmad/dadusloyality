@extends('layouts.internal')
@section('content')

	<div class="wrapper faq">
		<section class="section static_page">
			<div class="inner_page_banner">
				<img src="images/faq/banner.jpg" class="visible-lg visible-md" alt="">
				<img src="images/faq/banner_mob.jpg" class="visible-xs visible-sm mobile-banner" alt="">
			</div>
			<div class="container">
				<h2 class="section-title padding-top about_title text-center">Faq</h2>
             <div class="panel-group" id="accordion">
							<span class="product_divider">
				                <img src="{{asset('images/rsz_divider.png')}}">
				              </span>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Q : What are the benefits of Dadus Mithai Vatika mobile app ?</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in">
							<div class="panel-body">
								This mobile app allows you to easily make payment and place order online.
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Q : How can I recharge my card (app) ?</a>
							</h4>
						</div>
						<div id="collapseTen" class="panel-collapse collapse">
							<div class="panel-body">
								Online - Go to "My Account" and click on 'Reload' button and select 'Online' to recharge online with amount of your choice. .
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">Q : What is the minimum amount to recharge the card ?</a>
							</h4>
						</div>
						<div id="collapseEleven" class="panel-collapse collapse">
							<div class="panel-body">
								The minimum amount to recharge or activate your card is Rs.30.</strong>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Q : How do I make a payment using this app ?</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse">
							<div class="panel-body">
								Online - Go to "Order Now" and select the items to be ordered. In the payment page select the 'Payment options' as 'Wallet/Online-PG' and pay the order amount.
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Q : What happens if there are issues with my phone and app gets deleted? Will I lose all my data?</a>
							</h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse">
							<div class="panel-body">
								Your data is completely safe. You can simply download the app again from app store. Login with your credentials and your data will reappear again.
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Q: In case I have more doubts, complaints or suggestions, how can I reach to resolve the issue?</a>
							</h4>
						</div>
						<div id="collapseFive" class="panel-collapse collapse">
							<div class="panel-body">
								You can fill the form in the "Feedback" section. You can also write an email to info@dadus.co.in. The feedback reaches us directly and we will revert at the earliest to initiate a quick action. You can also call us at 09849107254 to speak directly.
								<br />
							</div>
						</div>
					</div>
				</div>
			</div> <!-- /.section-inner -->
		</div> <!-- /.container -->
	</section> <!-- /.section -->
</div> <!-- /.wrapper -->




@endsection
