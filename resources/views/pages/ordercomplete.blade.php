{{-- {{ dd($order) }} --}}
@extends('layouts.internal')
@if(!empty($recent) && $recent == true )
	@section('conversioncode')
		<script>
  			fbq('track', 'Purchase');
		</script>
	@endsection
@endif
@section('content')
				<div class="navbar navbar-secondary">
					<div class="steps">
						<div class="steps-inner">
							<div class="step-item">
								<div class="step-count">1</div>
								<div class="step-label">Your Items</div>
							</div> <!-- /.item-step-item -->
							<div class="step-item">
								<div class="step-count">2</div>
								<div class="step-label">Information</div>
							</div> <!-- /.step-item -->
							<div class="step-item">
								<div class="step-count">3</div>
								<div class="step-label">Shipping</div>
							</div> <!-- /.step-item -->
							<div class="step-item active">
								<div class="step-count">4</div>
								<div class="step-label">Payment</div>
							</div> <!-- /.step-item -->
						</div>
					</div> <!-- /.steps -->
				</div>

			<section class="section checkoutpage">
				<div class="container section">
					<div class="section-inner">
						<div class="center">
							<div class="thankyou">
								<div class="thankyou-icon">
									<img src="{{ url('images/checkoutpage/mail-icon.png') }}">
								</div> <!-- /.thankyou-icon -->
								<div class="section-title">Thank you for shopping</div>
							</div> <!-- /.thankyou -->
						</div>  <!-- /.section-body -->
					</div> <!-- /.section-inner -->
				</div> <!-- /.container -->
			</section> <!-- /.section -->
			<section class="section checkoutpage">
				<div class="container section">
					<div class="section-inner">
						<div class="section-body center cl-black">
							<h2 class="prot-title">Invoice</h2>
							<div class="title_img"><img src="{{ URL::asset('images/rsz_divider.png') }}"></div>
							<div class="row invo">								

							<div class="col-md-6 text-left">
								<div class="invoice_info">
									Delivery to:<br>
									<strong>{{ title_case($order->name) }}</strong><br>
									{{ $order->mobile }}<br>
									{{ $order->address }}<br>
									{{ $order->state }}<br>
									{{ $order->city }}<br>
									{{ $order->pincode }}<br>
								</div>
							</div>
							<div class="col-md-6 text-right">
								<div class="deliever_info">
									Order Date: {{ date('d-m-Y H:i:s', strtotime($order->created_at)) }}<br>
									Order Id: {{ $order->id }} <br>
									Payment Method: {{ $order->payment_method }}
								</div>
							</div>
							<div class="col-md-12 table-responsive">
							<table class="table invoice_table">
								<tr>
									<th>Product Id</th>
									<th>Product Name</th>
									<th>Pack Weight/Units</th>
									<th>Product Quantity</th>
									<th>Product Rate</th>
									<th>Product Amount</th>
								</tr>
								@foreach ($order['cart'] as $pwid => $pval)
								<tr>
									<td>{{ $pval['data']['id'] }}</td>
									<td>{{ $pval['data']['prod_title'] }}</td>
									<td>{{ $pval['weight']['weight'] }}</td>
									<td>{{ $pval['qty'] }}</td>
									<td>{{ $pval['weight']['price'] }}</td>
									<td><strong> {{ $pval['qty'] * $pval['weight']['price'] }}</strong></td>
								</tr>
								@endforeach
								<tr>
									<td class="noborder" colspan="6"></td>
								</tr>
								<tr>
									<td colspan="5" class="text-right">Cart Total</td>
									<td><strong>{{ $order->cart_total }}</strong></td>
								</tr>
								{{-- <tr>
									<td colspan="5" class="text-right" >Tax</td>
									<td><strong> {{ $order->tax }}</strong></td>
								</tr> --}}
								<tr>
									<td colspan="5" class="text-right" >Discount</td>
									<td><strong> {{ $order->discount }}</strong></td>
								</tr>
								<tr>
									<td colspan="5" class="text-right" >Delivery Charges</td>
									<td><strong> {{  $order->delivery_charge  }}</strong></td>
								</tr>
								<tr>
									<td colspan="5" class="text-right">Total</td>
									<td><strong> {{  $order->grand_total  }}</strong></td>
								</tr>
							</table>
						</div>

						</div>
					</div>
					<div class="thankyou-inner text-center">

									<div class="thankyou-cta">
										<a data-href="/" class="btn btn-primary btn-outline"><i class="ion-android-arrow-back"></i> Back to Shop</a>
									</div> <!-- /.thankyou-cta -->
								</div> <!-- /.thankyou-inner -->
				</div>
			</section>
@endsection
