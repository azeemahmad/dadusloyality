@extends('layouts.external')
@section('title', 'Best Sweet Shop in Hyderabad  - Dadus Mithai Vatika')

@section('keywords')
   {{'Dadus, Best Sweet Shop in Hyderabad, Angeer Sweet, Badam Sweets, Mixed Sweet, Kaju Sweets, Pista Sweets, Ghee Sweets, Bengali Sweets.'}}
@endsection

@section('description')
   {{'Dadus Mithai Vatika in Hyderabad offers delicious sweets. It is the best sweet shop with a variety of desserts made with Kaju, Anjeer, Badam, Ghee, Pista etc.'}}
@endsection
@section('content')
<div class="vs-slider mobileslider">
    <div class="vs-section active">
        <div class="container-fluid no_padding bannerscene" style=" ">
          <!-- <img src="images/first_banner.jpg" style="max-width: 100%;"> -->
            <ul id="scene" class="scene unselectable">
                <li class="layer" data-depth="0.10"><div class="tukadaimg depth-30" id="tukada1">
                        <img src="img/rsz_almonds.png" alt="Rope" class="sceneimg">
                    </div>
                </li>
                <li class="layer" data-depth="0.10"><div class="backgroundbanner"></div></li>

                <li class="layer" data-depth="0.15">
                    <div class="tukadaimg depth-30" id="tukada2">
                        <img src="images/bannerelements/1.png" alt="Rope" class="sceneimg"></div>
                </li>

                <li class="layer" data-depth="0.20"><div class="tukadaimg depth-30" id="tukada3">
                        <img src="images/bannerelements/2.png" alt="Rope" class="sceneimg"></div></li>

                <li class="layer" data-depth="0.25"><div class="tukadaimg depth-30" id="tukada4">
                        <img src="images/bannerelements/3.png" alt="Rope" class="sceneimg"></div></li>

                <li class="layer" data-depth="0.20"><div class="tukadaimg depth-30" id="tukada5">
                        <img src="images/bannerelements/4.png" alt="Rope" class="sceneimg"></div></li>

                <li class="layer" data-depth="0.35"><div class="tukadaimg depth-30" id="tukada6">
                        <img src="images/bannerelements/5.png" alt="Rope" class="sceneimg"></div></li>

                <li class="layer" data-depth="0.40"><div class="tukadaimg depth-30" id="tukada7">
                        <img src="images/bannerelements/6.png" alt="Rope" class="sceneimg"></div></li>

                <li class="layer" data-depth="0.15"><div class="tukadaimg depth-30" id="tukada8">
                        <img src="images/bannerelements/2.png" alt="Rope" class="sceneimg"></div></li>

                <li class="layer" data-depth="0.10"><div class="tukadaimg depth-30" id="tukada9">
                        <img src="images/bannerelements/5.png" alt="Rope" class="sceneimg"></div></li>

                <li class="layer" data-depth="0.15"><div class="tukadaimg depth-30" id="tukada10">
                        <img src="images/bannerelements/6.png" alt="Rope" class="sceneimg"></div></li>

                <li class="layer" data-depth="0.25"><div class="tukadaimg depth-30" id="tukada11">
                        <img src="images/bannerelements/3.png" alt="Rope" class="sceneimg"></div></li>

                <li class="layer" data-depth="0.40"><div class="tukadaimg depth-30" id="tukada12">
                        <img src="images/bannerelements/2.png" alt="Rope" class="sceneimg"></div></li>

                <li class="layer" data-depth="0.15"><div class="tukadaimg depth-30" id="tukada13">
                        <img src="images/bannerelements/1.png" alt="Rope" class="sceneimg"></div></li>

                <li class="layer" data-depth="0.15"><div class="tukadaimg depth-30" id="tukada14">
                        <img src="images/bannerelements/3.png" alt="Rope" class="sceneimg"></div></li>

                <li class="layer" data-depth="0.15"><div class="tukadaimg depth-30" id="tukada15">
                        <img src="images/bannerelements/6.png" alt="Rope" class="sceneimg"></div></li>
            </ul>
        </div>
    </div>

    <div class="vs-section movable_bg1">
        <div class="container-fluid no_padding">
            <div class="coloredsect jumbotron sweets">
                <div class="container-fluid no_padding">
                    <div class="col-md-12 text-center">
                        <h1 class="jumbo_title">Tukada Mithai ka</h1>
                        <span class="sweet_divider">
                            <img src="images/divider.png">
                        </span>
                        <h3>Sweets</h3>
                        <a href="{!! url('category/mixed-sweets') !!}" class="slider_btn">Shop Now</a>
                    </div>
                </div>
                <div class="container text-center">
                    <div class="biscuit_block">
                        <img src="images/sweets.png">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="vs-section movable_bg2">
        <div class="container-fluid no_padding">
            <div class="coloredsect jumbotron dryfruit">
                <div class="container-fluid no_padding">
                    <div class="col-md-12 text-center">
                        <h1 class="jumbo_title">Pitara Khushiyo ka</h1>
                        <span class="sweet_divider">
                            <img src="images/divider.png">
                        </span>
                        <h3>Dry fruit gift box</h3>
                        <a href="{!! url('category/dry-fruits-gift-boxes') !!}" class="slider_btn">Shop Now</a>
                    </div>
                </div>
                <div class="container text-center">
                    <div class="biscuit_block">
                        <img src="images/dryfruit.png">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="vs-section movable_bg3">
        <div class="container-fluid no_padding">
            <div class="coloredsect jumbotron festival">
                <div class="container-fluid no_padding">
                    <div class="col-md-12 text-center">
                        <h1 class="jumbo_title">Har pal ko banade ek Meetha utsav</h1>
                        <span class="sweet_divider">
                            <img src="images/divider.png">
                        </span>
                        <h3>Festival Attraction</h3>
                        <a href="{!! url('category/festival-attractions') !!}" class="slider_btn">Shop Now</a>
                    </div>
                </div>
                <div class="container text-center">
                    <div class="biscuit_block">
                        <img src="images/festival.png">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="vs-section movable_bgdokala">
        <div class="container-fluid no_padding">
            <div class="coloredsect jumbotron namkeens">
                <div class="container-fluid no_padding">
                    <div class="col-md-12 text-center">
                        <h1 class="jumbo_title">Hai humare paas saadhan anek</h1>
                        <span class="sweet_divider">
                            <img src="images/divider.png">
                        </span>
                        <h3>Namkeens</h3>
                        <a href="{!! url('category/regular-namkeen') !!}" class="slider_btn">Shop Now</a>
                    </div>
                </div>
                <div class="container text-center">
                    <div class="biscuit_block">
                        <img src="images/namkeens.png">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="vs-section movable_bg4">
        <div class="container-fluid no_padding">
            <div class="coloredsect jumbotron bites_chikki">
                <div class="container-fluid no_padding">
                    <div class="col-md-12 text-center">
                        <h1 class="jumbo_title">Jab Ek Mitha Sapna Bana Khushiyo Ka Karan</h1>
                        <span class="sweet_divider">
                            <img src="images/divider.png">
                        </span>
                        <h3>Bites & Chikkies</h3>
                        <a href="{!! url('category/bites-and-chikky') !!}" class="slider_btn">Shop Now</a>
                    </div>
                </div>
                <div class="container text-center">
                    <div class="biscuit_block">
                        <img src="images/chikkies.png">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="vs-section movable_bg3">
        <div class="container-fluid no_padding">
            <div class="coloredsect jumbotron cookies">
                <div class="container-fluid no_padding">
                    <div class="col-md-12 text-center">
                        <h1 class="jumbo_title">Aisi Dadu's ki parampara mithaas ki</h1>
                        <span class="sweet_divider">
                            <img src="images/divider.png">
                        </span>
                        <h3>Cookies</h3>
                        <a href="{!! url('category/cookies') !!}" class="slider_btn">Shop Now</a>
                    </div>
                </div>
                <div class="container text-center">
                    <div class="biscuit_block">
                        <img src="img/biscuit.png">
                    </div>
                </div>
            </div>
        </div>
        <footer class="homefooter">
            <div class="container">
                <div class="col-md-4">
                    copyright &copy; 2018 | All Rights Reserved
                </div>
                <div class="col-md-4">
                    Powered by <a href="http://firsteconomy.com/" target="_blank">First Economy</a>
                </div>
                <div class="foot-icon-li col-md-4">
                    <div class="foot-icon">
                        <a href="https://twitter.com/dadusMV" target="_blank">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                    <div class="foot-icon">
                        <a href="https://www.facebook.com/dadusgroup/" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </div>
                    <div class="foot-icon">
                        <a href="https://www.instagram.com/dadusgroup/" target="_blank">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <nav>
        <ul class="vs-vertical-nav">
            <li><a href="#" class="vs-prev">Prev </a></li>
            <li><a href="#" class="vs-next">Next</a></li>
        </ul>
    </nav>
</div>


@endsection
