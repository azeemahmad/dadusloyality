@extends('layouts.internal')
@section('content')
		<div class="wrapper">
			<section class="section static_page">
				<div class="containersection full_container">
					<div class="section-inner">
						{{-- <div class="inner_page_banner">
							<img src="images/about/banner.jpg" alt="">
						</div> --}}
						<div class="container">
							<h2 class="section-title padding-top about_title text-center">Career</h2>
							<span class="product_divider">
				                <img src="{{ URL::asset('images/rsz_divider.png') }}">
				              </span>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <span class="tbl-clr">
                                        No openings
                                    </span>
                                </div>
                            </div>
						</div> <!-- /.section-body -->

						</div>

					</div> <!-- /.section-inner -->
			</section> <!-- /.section -->
		</div> <!-- /.wrapper -->
@endsection
