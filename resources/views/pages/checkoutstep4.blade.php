@extends('layouts.internal')
@section('content')
				<div class="navbar navbar-secondary">
					<div class="steps">
						<div class="steps-inner">
							<div class="step-item">
								<div class="step-count">1</div>
								<div class="step-label">Your Items</div>
							</div> <!-- /.item-step-item -->
							<div class="step-item">
								<div class="step-count">2</div>
								<div class="step-label">Information</div>
							</div> <!-- /.step-item -->
							<div class="step-item">
								<div class="step-count">3</div>
								<div class="step-label">Shipping</div>
							</div> <!-- /.step-item -->
							<div class="step-item active">
								<div class="step-count">4</div>
								<div class="step-label">Payment</div>
							</div> <!-- /.step-item -->
						</div>
					</div> <!-- /.steps -->
				</div>
				{{-- <form action="{{ url('/confirm_order') }}" method="POST"> --}}

				{{-- <form action="javascript:void(0)" method="POST"> --}}
				<section class="section checkoutpage">
				<div class="container section">
					<div class="section-inner">
						<div class="section-body">
							<div class="section-nav-group">
							<a href="{{ url('checkoutstep3')}}">
								<div class="section-nav back">
									<i class="ion-android-arrow-back"></i>
									<div>Back</div>
								</div> <!-- /.section-nav.back -->
							</a>
							</div> <!-- /.section-nav-group -->
							<h2 class="section-title padding-top">Order Preview</h2>
								{{ csrf_field() }}
							<div class="section-subtitle">Cart Product</div>
							@if( count($cart) > 0)
							<div class="items">
								@foreach ($cart as $pid => $pdata)
									{{-- {{ dd($pid,$pdata) }} --}}
								<div class="item">
									<div class="item-inner">
										<figure>
											<a href="javascript:void(0)">
												<img src="{{ url('images/products/'.$pdata['data']['prod_image'] )}}" alt="product photo">
											</a>
										</figure>
										<div class="item-details">
											<div class="item-title"><div class="food-type veg"></div>{{ title_case($pdata['data']['prod_title']) }} - {{ $pdata['weight']['weight'] }} </div>
											<div class="item-title">{{ title_case($pdata['data']['prod_desc']) }}</div>
										</div> <!-- /.item-details -->
										<div class="item-price-qty">
											<div class="itm-prc">₹ {{ $pdata['weight']['price'] }}</div>
											<div class="itm-qty">{{ $pdata['qty'] }}</div>
											<div class="itm-total">₹ {{ $pdata['weight']['price']*$pdata['qty'] }}</div>
										</div>

										{{-- <div class="item-price">
											<div class="value" data-price="">₹ {{ title_case($pdata['data']['prod_price']) }}</div>
											<span class="quantity">
												<input type="text" name="quantity" value="{{ $pdata['qty'] }}" class="qty" readonly="">
											</span>
										</div> <!-- /.item-price --> --}}
										<div class="close"><i class="ion-close"></i></div>
									</div> <!-- /.item-inner -->
								</div>
								@endforeach
							</div>
						@else
							<div class="items">
								<div class="item">
									<h4>No Product</h4>
								</div>
							</div>
						@endif


							<div class="section-subtitle">Cost Breakup</div>
							<div class="section-subtitle">Loyalty Point Remaining <span class="pull-right" style="color:green">₹ {{$totalLoyalitypointvale}}</span></div>
							<div class="grand-total">
								<div class="grand-total-inner" data-slide=".grand-total-details," data-active=".grand-total-inner">
									<div class="grand-total-toggle">
										<div class="grand-total-icon">
											<!-- <i class="fa fa-shopping-bag" aria-hidden="true" style="color: #000;"></i> -->
											<i class="fa fa-angle-down" aria-hidden="true"></i>
										</div> <!-- /.grand-total-icon -->
										<div class="grand-total-label">
											Grand Total <span>{{count($cart)}} Items</span>
										</div> <!-- /.grand-total-label -->
										<div class="grand-total-price">
											₹ {{ $grand_total }}
										</div> <!-- /.grand-total-price -->
									</div>
									<div class="grand-total-details">
										<div class="row">
											<div class="col-md-12">
												<div class="total-info" data-calculate-me>
													<div class="total-item">
														<div class="total-name">Cart Total</div>
														<div class="total-value" id="total-order">&#8377; {{ $cart_total }}</div>
													</div> <!-- /.total-item -->
													<div class="total-item">
														<div class="total-name">Delivery Charge</div>
														<div class="total-value" id="total-shipping">&#8377; {{ isset($delivery_charges) ? $delivery_charges : 0 }}</div>
													</div> <!-- /.total-item -->
													<div class="total-item" data-calculate-min="true">
														<div class="total-name">Discount</div>
														<div class="total-value" id="total-discount">&#8377; {{  isset($discount) ? $discount: 0 }}</div>
													</div> <!-- /.total-item -->
													{{-- <div class="total-item">
														<div class="total-name">Tax</div>
														<div class="total-value" id="total-tax">&#8377; {{ isset($tax) ? $tax : 0 }}</div>
													</div> <!-- /.total-item --> --}}
													<div class="total-item total">
														<div class="total-name">Grand Total</div>
														<div class="total-value" id="total-all">&#8377; {{ $grand_total }}</div>
													</div> <!-- /.total-item -->
												</div> <!-- /.total-info -->
											</div> <!-- /.col-md-12 -->
										</div> <!-- /.row -->
									</div> <!-- /.grand-total-details -->
								</div> <!-- /.grand-total-inner -->
							</div> <!-- /.grand-total -->

							<div class="section-subtitle">Payment</div>
							{{-- {{ dd($customer_address,Auth::user()->name)}} --}}

						</div> <!-- /.section-body -->
						<div class="section-cta">
						<!-- <p style="color:#000"><b>Note:</b><br> Delivered within Hyderabad on the same day (If order placed before 1 PM),<br>
						Next day delivery within Hyderabad, if order placed after 1 PM<br>
					We do not deliver on Sundays.<br>
				3-4 working days for deliveries in all other cities.<br>
			You will receive a refund for a cancelled order, in minimum 7 working days.<br>
				If you have confirmed your order then place your order and we'll process it.</p> -->
							{{-- <button class="btn btn-primary placeorder" >Place Order<i class="ion-ios-arrow-thin-right"></i></button> --}}
							{{-- <button disabled class="btn btn-primary placeorder" >Coming Soon<i class="ion-ios-arrow-thin-right"></i></button> --}}

							<form class="form-horizontal" role="form" action="{{url('/payment')}}" id="razor"  method="post">

							{{--<form class="form-horizontal" role="form" action="javascript:void(0)" id="razor"  method="post">--}}
							{{csrf_field()}}
                                @if($totalLoyalitypointvale > 0)
								<div class="section-subtitle readmetab">Readme loyalty point &nbsp;&nbsp;<input type="checkbox" value="1" name="loyalitypoint" id="loyalitypointcheckbox"></div>
								@endif
								<div class="section-subtitle ajaxloader" style="display: none"><img src="{{asset('ajax-loader.gif')}}"></div>

								<div class="section-subtitle passcodetab" style="display: none">Enter OTP &nbsp;&nbsp;<input type="number" value="" name="passcode" id="passcode">
								<button id="verifypasscode">Verify OTP</button>
								</div>

							<input type="hidden" name="address_id" value="{{ $address_id }}">
							<input type="hidden" name="payment_method" value="razor_pay">
							{{-- <input type="hidden" name="item_name" value="Dadus Sweet">
							<input type="hidden" name="item_description" value="Dadus ka swaad">
							<input type="hidden" name="item_number" value="1234"> --}}
							<input type="hidden" name="amount" value="{{str_replace(',','', $cart_total)}}">
							<input type="hidden" name="address" value="{{ str_replace(',','', $customer_address['address'])}} {{ str_replace(',','', $customer_address['state'])}} {{ str_replace(',','', $customer_address['city'])}}">
							<input type="hidden" name="currency" value="INR">
							<input type="hidden" name="cust_name" value="{{ Auth::user()->name }}">
							<input type="hidden" name="email" value="{{ Auth::user()->email }}">
							<input type="hidden" name="contact" value="{{ Auth::user()->mobile_number }}">
							<input type="submit"  class="razor_pay" id="razorsubmit" value="Make Payment">
							</form>
							<br>
							{{-- <span class="label label-danger paymenterror" >Note: This website is under development, orders cannot be processed</span> --}}
							<span class="label label-danger paymenterror"></span>
						</div> <!-- /.section-cta -->
					</div> <!-- /.section-inner -->
				</div> <!-- /.container -->
				</section> <!-- /.section -->

				<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <div class="section-cta">
						<p style="color:#000"><b>Note:</b><br> Delivered within Hyderabad on the same day (If order placed before 1 PM),<br>
						Next day delivery within Hyderabad, if order placed after 1 PM<br>
					We do not deliver on Sundays.<br>
				3-4 working days for deliveries in all other cities.<br>
			You will receive a refund for a cancelled order, in minimum 7 working days.<br>
				If you have confirmed your order then place your order and we'll process it.</p>
							<p><input type="checkbox" id="checkedbox" name="terms"> <span style="color:#000;">I accept the <u>Terms and Conditions</u></span></p>
						</div>
      </div>

    </div>
  </div>
</div>
			{{-- </form> --}}

				<div class="modal" id="myUpdatedModal">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body" id="dispalysuccessmessage">
								<div style="color:red">There is something going wrong! please try again.</div>
							</div>
							<div class="pull-right">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>

						</div>
					</div>
				</div>
@endsection
@section('pagestyle')
	<style>
	.item-price-qty{
		display: table-cell !important;
		vertical-align: top;
		padding: 20px 40px 0 20px;
		width: 300px;
	}
	.itm-prc,.itm-qty,.itm-total
	{
		display: inline-block;
		color: #db0044;
		padding: 0 5px;
	}
	</style>
@endsection
@section('pagescript')
<script>
	$('#loyalitypointcheckbox').click(function() {
		if ($(this).is(':checked')) {
			if(confirm("Are you sure want to use loyalty point?")){
				var totalamount= '{{$grand_total}}';
				var readme= '{{$totalLoyalitypointvale}}';
				var mobile='{{Auth::user()->mobile}}';
				$.ajax({
					type: "POST",
					url: "/getreadmepasscode",
					data: {totalamount: totalamount,readme:readme,mobile:mobile},
					beforeSend: function() {
						$('.readmetab').hide();
						$('.ajaxloader').show();
					},
					success: function (response) {
                             if(response==1){
								 $('.ajaxloader').hide();
                                 $('.readmetab').hide();
								 $('#razorsubmit').hide();
								 $('.passcodetab').show();
							 }
						     else{
								 $('.ajaxloader').hide();
								 $('.readmetab').show();
								 $('.passcodetab').hide();
								 $('#razorsubmit').show();
								 $("#myUpdatedModal").modal("toggle");

							 }
					}
				});
			}
			else{
				return false;
			}
		}
	});

	$('#verifypasscode').click(function(event){
		  event.preventDefault();
          var passcode=$('#passcode').val();
		  if(passcode.length != 6){
			  alert('Enter 6 digits passcode.');
			  return false;
		  }
		  else{
			   $('.ajaxloader').show();
               $('#razor').submit();
		  }

	});
</script>
@endsection
