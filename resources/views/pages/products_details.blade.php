
@extends('layouts.internal')

@section('title', isset($prod) && !empty($prod['metatitle']) ? $prod['metatitle']  : $prod['prod_title'])
@section('keywords', isset($categories) && !empty($categories) ? $prod['prod_title'] .' - '. $categories : '')
@section('description', isset($prod) && !empty($prod['description']) ? $prod['description'] : $prod['prod_title'])


@section('content')
<!-- 

{{ var_dump($prod) }}
-->

<section class="product_detail_section">
  <div class="container">
    @if(!empty($prod))
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-6 col-sm-6 col-xs-12">
           
                <!-- <img src='{{asset('images/products/'.$prod->prod_image)}}'> -->
          
           <div class="exzoom" id="exzoom">
                          

                          <div class="exzoom_img_box">
                            <ul class='exzoom_img_ul'>
                              <li> <img src='{{asset('images/products/'.$prod->prod_image)}}'></li>
                           
                            </ul>
                          </div>
                       
                        
                          <!-- Nav Buttons -->
                         
                        </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <form method="POST" action="{{url('add_to_cart_submit')}}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="product-detail">
              <div class="">
                  <h1 class="product_name">@if(!empty($prod->prod_title)){{$prod->prod_title}}@endif</h1>                  
              </div><br>
              <p class="product_desc">@if(!empty($prod->prod_desc)){{$prod->prod_desc}}@endif</p>

              {{-- <div class="add-qty">
                <div class="">
                 <div class="input-group">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="product_qty[1]">
                        <span class="glyphicon glyphicon-minus"></span>
                      </button>
                    </span>
                        <input type="text" name="product_qty[1]" class="form-control input-number pd_num" value="1" min="1" max="10">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default btn-number " data-type="plus" data-field="product_qty[1]">
                         <span class="glyphicon glyphicon-plus"></span>
                        </button>
                    </span>
                  </div>
                </div>
              </div><br> --}}
              @if($prod->prod_qty == 0)
                <span class="add_cart">Only available in shop</span>
              @else
                <input type="number" name="product_qty" class="product_qty new_qua" style="margin-left:0;" value="1"><br><br><br>
              @endif
              @if(!empty($prod->weight))
              <div class="select_kg new_kg">
                          <select class="prod_weight" name="weight">
                                  @foreach($prod->weight as $k=>$v)
                                                            <option value="{{$v['id']}}">{{$v['weight']}} for ₹{{$v['price']}} </option>
                                  @endforeach
                                                        </select>
                          </div><br><br>
               @endif
                <span class="add_cart"><button type="submit" class="button add_to_cart_button">Add to Cart</button></span>
                <input type="hidden" name="product_id" value="{{$prod->id}}">
              </div>
            </form>
          </div>
    </div>
  @endif
  </div>
</section>



@endsection

@section('pagescript')
 <script>

//plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function(e){
    e.preventDefault();

    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {

            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            }
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
  $('.input-number').focusin(function(){
     $(this).data('oldValue', $(this).val());
  });
  $('.input-number').change(function() {

      minValue =  parseInt($(this).attr('min'));
      maxValue =  parseInt($(this).attr('max'));
      valueCurrent = parseInt($(this).val());

      name = $(this).attr('name');
      if(valueCurrent >= minValue) {
          $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
      } else {
          alert('Sorry, the minimum value was reached');
          $(this).val($(this).data('oldValue'));
      }
      if(valueCurrent <= maxValue) {
          $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
      } else {
          alert('Sorry, the maximum value was reached');
          $(this).val($(this).data('oldValue'));
      }


  });
  $(".input-number").keydown(function (e) {
          // Allow: backspace, delete, tab, escape, enter and .
          if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
               // Allow: Ctrl+A
              (e.keyCode == 65 && e.ctrlKey === true) ||
               // Allow: home, end, left, right
              (e.keyCode >= 35 && e.keyCode <= 39)) {
                   // let it happen, don't do anything
                   return;
          }
          // Ensure that it is a number and stop the keypress
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
              e.preventDefault();
          }
    });

// quantity plus/minus js ends here

		// image zoom effect
		$("#exzoom").exzoom({
		        autoPlay: false,
		});

    </script>

@endsection
