@extends('layouts.internal')
@section('content')
	<section class="section checkoutpage login-page">
		<div class="containersection">
			<div class="section-inner">
				<div class="section-body">
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li><span class="label label-danger">{{ $error }}</span></li>
								@endforeach
							</ul>
						</div>
					@endif
					@if (session('message'))
						<div class="notification closeable">
							{{ session('message') }}
						</div>
					@endif
					<h2 class="section-title padding-top">Your Information</h2>

					<div class="account-form">

						<div class="row">

							<div class="col-md-8 col-sm-8">
								<div class="section-subtitle">Account</div>
								<div id="login-form">
									<form  action="javascript:void(0)" role="form" method="POST" id="signin" novalidate="novalidate">
										<div class="form-group">
											<label>Mobile Number</label>
											<input type="number" name="signinnumber" id="signinnumber" class="form-control" required="">
										</div> <!-- /.form-group -->

										<div class="form-group text-right">
											<input type="submit" class="btn btn-primary signin" value="Sign In">
										</div>
										<span class="label label-danger error"></span>
										<!-- /.form-group.text-right -->
									</form>
								</div> <!-- /.login-form -->

								<!--otp form start here-->
								<div id="otp-form" class="login-form">
									<form action="javascript:void(0);">
										<div class="form-group">
											<label>Please Enter OTP</label>
											<input type="number" name="otp" id="otpnumber" class="form-control">
											<span id="otp_error"></span>
										</div> <!-- /.form-group -->
										<input type="hidden" id="otp_mobile">
										<div class="form-group text-right">
											<input type="button" class="btn btn-primary validateotp" value="Submit">
										</div> <!-- /.form-group.text-right -->
									</form>
								</div>
								<!--otp form end here-->
								<div class="login-form" id="register-form">
									<form action="javascript:void(0);" role="form" method="POST" id="register" novalidate="novalidate">
										<div class="form-group">
											<label>Your Name</label>
											<input type="text" name="registername" id="registername" class="form-control">
										</div>
										<div class="form-group">
											<label>Mobile Number</label>
											<input type="number" name="registermobile" id="registermobile" class="form-control">
										</div> <!-- /.form-group -->
										<div class="form-group">
											<label>Email Id</label>
											<input type="emailid" name="registeremail" id="registeremail" class="form-control">
										</div> <!-- /.form-group -->

										<div class="form-group">
											<label>Date Of Birth</label>
											<input type="dateofbirth" name="dob" id="dob" class="form-control datepicker">
										</div> <!-- /.form-group -->

										<div class="form-group text-right">
											<input type="submit" class="btn btn-primary registeruser" value="Register">
										</div> <!-- /.form-group.text-right -->

										<span class="label label-danger registererror"></span>
									</form>
								</div> <!-- /.login-form -->

								<div class="form-group">
									<p class="help-block">If you do not have an account? <a href="#" data-fade="#register-form,#login-form">Register</a> or <a href="#" data-fade="#login-form,#register-form">Sign In</a></p>
								</div> <!-- /.form-group -->
							</div> <!-- /.col-md-6 -->


							{{-- <div class="col-md-6 col-sm-6">
								<div class="section-subtitle ">Or Login With</div>
								<div class="le-social-button-login form-group">
									<a href="redirect"><button class="ico-fb le-icon" title="Connect with Facebook"></button></a>
									<a href="googleredirect"><button class="ico-go le-icon" title="Connect with Facebook"></button></a>
								</div>

								<!-- <div class="form-group">
								<p class="help-block">Checkout as guest? <a href="#" data-fade=".email-field,#login-form,#register-form">Back</a></p>
							</div> --> <!-- /.form-group -->

						</div> <!-- /.col-md-6 --> --}}

					</div> <!-- /.row -->
					<div class="divider"></div>


				</div> <!-- /.account-form -->
			</div> <!-- /.section-body -->

		</div> <!-- /.section-inner -->
	</div> <!-- /.container -->
</section> <!-- /.section -->



</div> <!-- /.wrapper -->
@endsection
