<!-- header -->
<header class="homeheader">
<nav class="navbar">
  <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="{{ url('/') }}">
          <img src="img/logo.png">
        </a>
      </div>

      <div class="iconsmobile hidden-md hidden-lg">
        <a href="{{ 'my_account' }}"><i class="fa fa-user mobicons"></i> </a>
        <a href="{{ 'cart' }}"><i class="fa fa-shopping-bag mobicons" aria-hidden="true"></i></a>
      </div>

     <a href="javascript:void(0);" style="" class="icon menuicon" onclick="openNav()">&#9776;</a>
      <ul class="nav navbar-nav navbar-right sidenav" id="mySidenav">
        <a href="javascript:void(0)" class="closebtn hidden-md hidden-lg" onclick="closeNav()">&times;</a>
        <li class="active"><a href="/">Home</a></li>
        <li><a href="{{ url('aboutus') }}">About Us</a></li>
        <li><a href="{{ url('category') }}">Buy Products Online</a></li>
        <li><a href="{{ url('career') }}">Careers</a></li>
        <li><a href="{{ url('contactus') }}">Contact Us</a></li>
        <li><a href="tel:919000810182"><i class="fa fa-phone" aria-hidden="true"></i> (+91)-9000810182</a></li>
        <li class="icon_links hidden-sm hidden-xs"><a href="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
        <!-- <li class="icon_links hidden-sm hidden-xs">
          <a href="#" id="search"><i class="fa fa-search" aria-hidden="true"></i></a>
        </li> -->
      </ul>
  </div>
</nav>
</header>
<!-- end of header -->
