{{-- {{ dd($category_tree) }} --}}
<!-- header -->
<header class="innerheader">
    <div class="stickygold sticky-top-bar ">
        <div class="offerbar text-center">
            <span class="tagline" style="color: #fff">Celebrating 25 years of legacy</span>
        </div>
    </div>
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{url('/')}}">
                    <img src="{{ URL::asset('img/logo.png') }}">
                </a>
            </div>

                <div class="iconsmobile hidden-md hidden-lg">
                    <a href="{{ url('my_account') }}"><i class="fa fa-user mobicons"></i> </a>
                    <a href="{{ url('cart') }}"><i class="fa fa-shopping-bag mobicons" aria-hidden="true"></i></a>
                </div>

                <div class="search-bar col-md-5 col-sm-5">

                    <ul class="nav navbar-nav navbar-right small-nav sidenav" id="mySidenav">
                        <a href="javascript:void(0)" class="closebtn hidden-md hidden-lg" onclick="closeNav()">×</a>
                        <li class="desktop-hide"><a href="/">Home</a></li>
                        <li><a href="{{ url('aboutus') }}">About Us</a></li>

                        <!-- <li><a href="{{ url('aboutus') }}">About Us</a></li> -->
                        <li class="desktop-hide"><a href="{{ url('category') }}">Buy Products Online</a></li>
                        <li class="desktop-hide"><a href="{{ url('career') }}">Careers</a></li>
                        <!-- <li><a href="{{ url('contactus') }}">Contact Us</a></li> -->        
                        <!-- <li class="icon_links hidden-sm hidden-xs"><a href="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a> -->


                        <li><a href="{{ url('contactus') }}">Contact Us</a></li>
                        @if(Auth::check())
                        <li> <a href="{{ url('my_account') }}">My Account</a></li>

                        <li><a href="{{ url('logout') }}">Logout</a></li>
                        @else
                        <li><a href="{{ url('login') }}">Login</a></li>
                        @endif

                        <li class="desktop-hide"><a href="javascript:void(0)"><i class="fa fa-phone" aria-hidden="true"></i> (+91)-9000810182</a></li>

                      </ul>
                </div>

                <a href="javascript:void(0);" style="" class="icon menuicon" onclick="openNav()">&#9776;</a>
                <ul class="nav navbar-nav navbar-right sidenav hidden-md hidden-lg" id="mySidenav">
                    <a href="javascript:void(0)" class="closebtn hidden-md hidden-lg" onclick="closeNav()">&times;</a>
                    <li class="active"><a href="/">Home</a></li>
                    <li><a href="{{ url('aboutus') }}">About Us</a></li>
                    <li><a href={{ url('product') }}>Buy Products Online</a></li>
                    <li><a href="{{'careers'}}">Careers</a></li>
                    <li><a href="{{'contactus'}}">Contact Us</a></li>
                    <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i> (+91)-9000810182</a></li>
                    <li class="icon_links hidden-sm hidden-xs"><a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
                    <li class="icon_links hidden-sm hidden-xs">
                        <a href="#" id="search"><i class="fa fa-search" aria-hidden="true"></i></a>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right inheaderlinks hidden-sm hidden-xs" id="">
                    <li class="search-li">
                        <form>
                            <div name="search-form1" id="search-form">
                                <input type="search" name="search" id="search_con" class="form-control searchbox" placeholder="Search Entire Store Here" autocomplete="off"
                                required="required" title="" data-res-id="desk_result">
                                <button class="btn search__btn"><i class="fa fa-search search-icon" id="search_btnsub"></i></button>
                            </div>
                            <div class="searchresult" id="desk_result">
                            </div>
                        </form>
                    </li>
                </ul>

            </div>
        </nav>
        <!-- second product category header -->
        <!-- <form class="search-container-mobile ">
                <input type="text" id="search-bar" placeholder="Search">
                <a href="#"><img class="search-icon" src="http://www.endlessicons.com/wp-content/uploads/2012/12/search-icon.png"></a>
            </form> -->

            <ul class="nav navbar-nav navbar-right inheaderlinks mobile-custom-search" id="">
                    <li class="search-li">
                        <form>
                            <div name="search-form1" id="search-form">
                                <input type="search" name="search" id="search_con" class="form-control searchbox" placeholder="Search Entire Store Here" autocomplete="off" required="required" title="" data-res-id="mob_result">
                                <button class="btn search__btn"><i class="fa fa-search search-icon" id="search_btnsub"></i></button>
                            </div>
                            <div class="searchresult" id="mob_result">
                            </div>
                        </form>
                    </li>
                </ul>

        <div class="productsheader clearfix">

            <div class="selectcategoriesdiv">
                <!-- <form id="mobile-search">
                    <input type="search" placeholder="Search">
                </form> -->
                
                <div class="selectproduct text-center">                    
                    Select Categories<span class="caret"></span>
                </div>
                <ul class="subheaderproductpage" data-hover="dropdown" data-animations="fadeInLeft fadeIn fadeIn fadeIn"  >
                    {{-- {{ dd($category_tree) }} --}}
                    @foreach($category_tree as $cat)
                        @if($cat->children->isEmpty())
                            <li class="option"><a href="{{ URL('category/'.$cat->category_slug) }}">{{ title_case($cat->title) }}</a></li>
                        @else
                            <li class="dropdown option">
                                <a href="{{ URL('category/'.$cat->category_slug) }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ title_case($cat->title) }}<span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdownhover-bottom" role="menu">
                                        @foreach($cat->children as $subcat)
                                            <li><a href="{{ URL('category/'.$subcat->category_slug) }}">{{ $subcat->title }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
            <!-- second product category header -->
        </header>
        <!-- end of header -->

        <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }
        </script>
