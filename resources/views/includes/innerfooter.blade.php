<footer class="innerfooter">
<div class="container " >
    <div class="row">
        <div class="col-md-12" style="">
            <div class="col-md-2  col-sm-12 col-xs-12">
                <h4 class="footer-title">Our Services</h4>

              <div class="footer-links">
                <ul>
                <li><a href="{{ url('aboutus') }}">About Us</a></li>
                {{-- <li><a href="#">Blog</a></li> --}}
                <li><a href="{{ url('category') }}">Products</a></li>
                <li class="last"><a href="{{url('contactus')}}">Contact Us</a></li>
                </ul>
                </div>

            </div>

            <div class="col-md-3 col-sm-12 col-xs-12">
            <h4 class="footer-title">our policies</h4>

              <div class="footer-links">
                <ul>
                {{-- <li class="privacy"><a href="#">Cancellations and Returns</a></li> --}}
                {{-- <li class="privacy"><a href="#">Terms and conditions</a></li> --}}
                <li class="privacy"><a href="{{url('privacypolicy')}}">Privacy Policy</a></li>
                <li><a href="{{url('faq')}}">FAQ</a></li>
                <li><a href="{{ url('storelocator') }}">Store Locator</a></li>
              </ul>
              </div>

            </div>

            <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                <h4 class="footer-title">Quick Links</h4>
                <div class="footer-links">
                    <ul>
                        <li class=""><a class="greyshade_head" href="{{ url('my_account') }}">my account</a>
                        </li>
                        <li class=""><a class="greyshade_head" href="{{ url('cart') }}">my cart</a>
                        </li>
                        <li class=""><a class="greyshade_head" href="{{ url('login') }}">log in</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ">
                <h4 class="footer-title">Connect With Us</h4>
                <div class="footer-links">
                    <ul>
                        <li class="">
                            <a class="greyshade_head" href="tel:919000810182">(91)-9000810182</a>
                        </li>
                        <li class="">
                            <a class="greyshade_head nocaps" href="mailto:info@dadus.co.in">info@dadus.co.in</a>
                        </li>
                        <li class="foot-icon-li">
                            <div class="foot-icon">
                                <a href="https://twitter.com/dadusMV" target="_blank">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </div>
                            <div class="foot-icon">
                                <a href="https://www.facebook.com/dadusgroup/" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </div>
                            <div class="foot-icon">
                                <a href="https://www.instagram.com/dadusgroup/" target="_blank">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12">
                <h4 class="footer-title sgn">Sign up for our newsletter:</h4>
                <div class="div-brd divbrds"></div>
                <div class="pad-gap"></div>
                <div class="footer-contacts">
                    <div class="block block-subscribe">
                        <div id="newsletter-validate-detail">
                            <div class="block-content">
                                <div class="input-box">
                                    <input type="text" name="email" id="newsletter" title="Email" class="input-text required-entry validate-email" placeholder="Email" style="">
                                    <button type="submit" title="Subscribe" class="button footer_submit newsletter_submit" style=""><span class="join newsletter_submit" id="news">JOIN</span>
                                    </button>
                                    <div class="newslettermsg" style="color:red;font-size:12px;"></div>
                                    {{-- <span class="showLoadernews" style="display:none;font-size: 15px;margin:0 auto;text-align:center;"><img style="width:30px;height:30px;" src="{{url('images/img_load.gif')}}"> &nbsp;&nbsp;&nbsp;please wait...</span> --}}
                                </div>
                                <!-- <div class="actions"> <button type="submit" title="Subscribe" class="button footer_submit"><span><span class="">Submit</span></span></button> </div> --></div>
                        </div>
                    </div>
                    <div class="contact-info"> </div>
                </div>
            </div>
         </div>
         </div>
    </div>
</footer>
<div class="container-fluid bottomfooter">
        <div class="row">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="col-md-6">
                      Copyright © {{date('Y')}} | All Rights Reserved.
                    </div>
                    <div class="col-md-6">
                      Powered by <a href="http://firsteconomy.com/" target="_blank">First Economy</a>
                    </div>
                </div>
            </div>
    </div>
</div>
