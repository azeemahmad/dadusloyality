$(function(){
    // Initialize iCheck
    if($.fn.iCheck) {
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-purple',
            radioClass: 'iradio_flat-purple'
        });
    }
    // Initialize Bootstrap Popover
    if($.fn.popover){
        $('[data-toggle="popover"]').popover({
            placement: 'auto',
            html: true,
            trigger: 'click'
        });
    }
    // Initialize Bootstrap Tooltip
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body',
        zIndex: 2002
    });

    var host_url = location.protocol + "//" + location.host;
    // alert(host_url);

    $('form#signin').validate({

        rules: {
            signinnumber:
            {
                required: true,
                 minlength: 10,
                 maxlength:10,
            }
        },
        messages : {
            signinnumber : {
                required : '<span class="alert-danger">Mobile Number is a required field.</span>',
                minlength : '<span class="alert-danger">Mobile Number should be of 10 digits.</span>',
                maxlength : '<span class="alert-danger">Mobile Number should be of 10 digits.</span>',
            }
        },
        submitHandler: function (form) {
            var mobilenumber = $("#signinnumber").val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var ajax_data = {
                mobilenumber : mobilenumber
            }
            $.ajax({
                type: "post",
                url: host_url + '/login',
                data: ajax_data,//data,
                beforeSend: function(result) {
                    $('.signin').val('Please wait..');
                },
                success: function(response) {
                    if(response.status == true)
                    {
                        $('#otp_mobile').val(mobilenumber);
                        $("#login-form").css("display", "none");
                        $("#otp-form").css("display", "block");
                    }
                    else
                    {
                        $(".error").html(response.message);
                    }
                }
            });
        }

    });

    $( ".validateotp" ).click(function() {

        var otpnumner = $("#otpnumber").val();
        var mobile = $("#otp_mobile").val();
        var redirect = $("#otp_mobile").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var ajax_data = {
            otpnumner : otpnumner,
            mobile:mobile
        }

        $.ajax({
            type: "post",
            url: host_url + '/validateotp',
            data: ajax_data,//data,
            beforeSend: function(result)
            {
                $('.validateotp').val('Please wait..');
            },
            success: function(response)
            {
                $('.validateotp').val('Submit');
                if(response.status == false)
                {
                    $('#otp_error').html('<span class="alert-danger">Invalid OTP</span>');
                    // window.location.href = "checkoutstep2";
                }
                else
                {
                    // window.location.href = "checkoutstep3";
                    location.reload();
                }
            }
        });
    });

    //registration form start here

    $('form#register').validate({

        rules: {
            registername:
            {
                required: true
            },
            registermobile:
            {
                required: true,
                minlength: 10,
                maxlength:10,
            },
            registeremail:
            {
                required: true,

            },
            dob:
            {
                required: true,

            }
        },
        messages : {
            registername : {
                required : '<span class="alert-danger">Name is a required field.</span>',
            },
            registermobile : {
                required : '<span class="alert-danger">Mobile Number is a required field.</span>',
                minlength : '<span class="alert-danger">Mobile Number should be of 10 digits.</span>',
                maxlength : '<span class="alert-danger">Mobile Number should be of 10 digits.</span>',
            },
            registeremail : {
                required : '<span class="alert-danger">Email Id is a required field.</span>'
            },
            dob : {
                required : '<span class="alert-danger">Date Of Birth is a required field.</span>'
            }
        },
        submitHandler: function (form) {

            var name = $("#registername").val();
            var mobile = $("#registermobile").val();
            var email = $("#registeremail").val();
            var dob = $("#dob").val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var ajax_data = {
                name : name,
                mobile : mobile,
                email : email,
                dob : dob
            }

            $.ajax({
                type: "post",
                url: host_url + '/registeruser',
                data: ajax_data,//data,
                beforeSend: function(result) {
                    console.log('register');
                    $('.registeruser').val('Please wait..');
                },
                success: function(response) {
                    if(response.status == false){
                        $(".registererror").html(response.message);
                    }
                    else
                    {
                        $('#otp_mobile').val(mobile);
                        $("#register-form").css("display", "none");
                        $("#otp-form").css("display", "block");
                    }
                }
            });
        }
    });

    //registration form end here

    //shipping details start here
    $('form#save_address').validate({

        rules: {
            name:
            {
                required: true
            },
            address:
            {
                required: true,

            },
            state:
            {
                required: true,

            },
            city:
            {
                required: true,

            },
            pincode:
            {
                required: true,

            },
            mobile_number:
            {
                required: true,
                digits: true,
            },
        },
        messages : {
            name : {
                required : '<span class="alert-danger">Name is a required field.</span>',
            },
            address : {
                required : '<span class="alert-danger">Address is a required field.</span>'
            },
            state : {
                required : '<span class="alert-danger">State is a required field.</span>'
            },
            city : {
                required : '<span class="alert-danger">City is a required field.</span>'
            },
            pincode : {
                required : '<span class="alert-danger">pincode is a required field.</span>'
            },
            mobile_number : {
                required : '<span class="alert-danger">Mobile number is a required field.</span>',
                digits : '<span class="alert-danger">Invalid mobile number.</span>',

            }
        },
        submitHandler: function (form) {

            var name = $("#name").val();
            var address = $("#address").val();
            var state = $("#state").val();
            var city = $("#city").val();
            var pincode = $("#pincode").val();
            var mobile_number = $("#mobile_number").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var ajax_data = {
                name : name,
                address : address,
                state : state,
                city : city,
                pincode : pincode,
                mobile_number : mobile_number,
            }

            $.ajax({
                type: "post",
                url: host_url + '/saveaddress',
                data: ajax_data,//data,
                beforeSend: function(result) {

                },
                success: function(response) {
                    if(response.status == true)
                    {
                         var add_id = response.address_id;
                         add_id =  btoa(add_id);
                         window.location = 'checkoutstep4/'+add_id;
                        // location.reload();
                    }
                    else
                    {
                        $(".shippingerror").html(response.message);
                    }
                }
            });
        }
    });
    //shipping details end here

    //place order and save in database start
    $( ".placeorder" ).click(function() {
        $.ajax({
            url: host_url + '/confirmorder',
            type: 'GET',
            success: function(res) {
                if(res.status == false){
                    $(".paymenterror").html(response.message);
                }else{
                    window.location.href = "ordercomplete";
                }
            }
        });
    });
    //place order and save in database end

    //search bar done by ankita
    $( "#search_btnsub" ).click(function() {

    });

    // console.log('outtttttt');

    $("body").on("keypress",".searchbox", function(){

        // console.log('innnnnnnnnnnnnnnnnnnn');
        var search = $.trim($(this).val());
        var result_div = $(this).attr('data-res-id');
        if(search.length > 0)
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formdata = {search : search};

            $.ajax({
                url: host_url+"/search_result1",
                type: 'POST',
                data: formdata,
                success: function(res) {
                    if(res)
                    {
                        $("#" + result_div).html(res);
                    }
                    else
                    {
                        $("#" + result_div).html('Product Not Found');
                    }
                }
            });
        }
        else{
            $("#" + result_div).html('');
        }
    });

    $('#search_con').keypress(function(e){
        if(e.which == 13){//Enter key pressed
            // $('#search_btnsub').click();//Trigger search button click event
            var search = $("#search_con").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formdata = {search : search};

            $.ajax({
                url: host_url+"/search_result",
                type: 'POST',
                data: formdata,
                success: function(res) {
                    $("#sres").html(res);
                    $(".searchresult").html('');
                }
            });
        }
    });

    //end search bar


    // Cancel Order

    $('.btn-cle').on('click',function() {
        var result = confirm("Cancel Order??");
        if (result) {
    //Logic to delete the item
        var oid = $(this).data('oid');
        console.log(oid);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var formdata = {oid : oid};

        $.ajax({
            url: host_url+"/cancel_order",
            type: 'POST',
            data: formdata,
            success: function(res) {
                if(res)
                {
                    $.notify("Request sent successfully", "success");
                    setTimeout(function(){ location.reload(); }, 3000);
                }
            }
        });

    }
    });

    // Cancel Order End


    //Fetch location
    $(".user-address").on("change","#loc-select", function(){
        var loc = $.trim($('#loc-select').val());
        var loct = '';
        var a = "";
        if(loc.length > 0)
        {
            $.cookie("loct", loc,{ expires: 7 });
            var a = $.cookie("loct");

        }
        else{

        }
    });

    //End Fetch Location

    // Clickable element with attribute data-href
    $("[data-href]").each(function() {
        $(this).click(function() {
            try {
                if(eval($(this).data('href'))){
                    document.location = eval($(this).data('href'));
                    return;
                }
            } catch($e) {
                document.location = $(this).data('href');
            }

        });
    });

    // Trigger element to fadeIn or fadeOut
    $("[data-fade]").each(function() {
        $(this).click(function() {
            var $sel = $(this).data("fade"),
            $in = $sel.split(",")[0],
            $out = $sel.split(",")[1];
            $out_other = $sel.split(",")[2];

            if($in) {
                $($in).fadeIn();
            }
            if($out) {
                $($out).fadeOut();
                $($out_other).fadeOut();
            }
            return false;
        });
    });

    // Trigger element to slideDown or slideUp
    $("[data-slide]").each(function() {
        $(this).click(function() {
            var $sel = $(this).data("slide"),
            $in = $sel.split(",")[0],
            $out = $sel.split(",")[1];

            if($(this).data("active")) {
                $($(this).data("active")).toggleClass("active");
            }

            if($in) {
                $($in).slideToggle();
            }
            if($out) {
                $($out).slideToggle();
            }
            return false;
        });
    });

    $("[data-shipping-method]").each(function() {
        $(this).click(function() {
            $("[data-shipping-method]").removeClass("active");
            $(this).addClass("active");
            setShipping($(this).find("input:radio").val());
        });
    });

    $(".payment-method-item").each(function() {
        $(this).click(function() {
            $(".payment-method-item").removeClass("active");
            $(this).addClass("active");

            $(".payment-method-form").removeClass('show').hide();
            $("#" + $(this).data("target")).fadeIn();
        });
    });

    $(".items .item").each(function() {
        var $this = $(this);

        $this.find(".close").click(function() {
            $this.fadeOut(function() {
                $(this).remove();
                calculateOrder();
                calculateTotal();
            });
        });
    });

    $("#coupon-form").submit(function() {
        setDiscount(20);
        return false;
    })

    // calculateTotal();


    $(".helpme .helpme-inner").click(function() {"use strict";
    let $this = $(this);

    if(!$(".helpme").hasClass("active")) {
        $(".helpme").addClass("active");
        $(".helpme-content-scrollable").niceScroll({
            cursoropacitymin: .3,
            cursoropacitymax: .8
        });
        $(".helpme input").focus();
        $("body").append($("<div>", {
            class: 'backdrop'
        }));
    }else{
        $(".helpme").removeClass("active");
        $(".backdrop").remove();
    }
});

$("#val_pin").keyup(function (e) {
    var pin = $(this).val();
    var url_pin = host_url + '/pincodestrictcheck';
    if(pin != ''){
        if(pin.length >= 2)
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'POST', // Type of response and matches what we said in the route
                url: url_pin, // This is the url we gave in the route
                data: {'pin' : pin}, // a JSON object to send back
                success: function(response){ // What to do if we succeed

                    $("#showError").html(response.message);
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    //console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        }
    }
    else{
        $("#showError").html("");
    }
});

$('button#pincheck').on('click',function(){
    var val_pin = $('#val_pin').val();

    var url_pin = host_url + '/check_pincode';
    //alert(url_pin);return false;
    if(val_pin != ''){
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: url_pin, // This is the url we gave in the route
            data: {'val_pin' : val_pin}, // a JSON object to send back
            success: function(response){ // What to do if we succeed
                //console.log(response.message);return false;
                $("#showError").html(response.message);
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                //console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    }else{
        $("#showError").html('Please Enter correct Pincode!!');
    }
});

});